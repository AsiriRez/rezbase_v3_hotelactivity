package bookingconfirmation;

import java.util.ArrayList;

public class Quotation {
	
	private boolean isQuotationMailLoaded = true;
	private String quote_BookingRef;
	private String quote_BookingNo;
	
	private ArrayList<String> quote_HotelsHeading;
	private ArrayList<String> quote_CheckIn;
	private ArrayList<String> quote_CheckOut;
	private ArrayList<String> quote_HotelBookingStatus;	
	private ArrayList<String> quote_HotelSubTotal;
	private ArrayList<String> quote_HotelTotalTax;
	private ArrayList<String> quote_HotelBookingValue;
	
	private ArrayList<String> quote_title_Occupancy;
	private ArrayList<String> quote_firstName_Occupancy;
	private ArrayList<String> quote_lastName_Occupancy;
	
	private String quote_ActivityType;
	private String quote_BookingStatus;
	private String quote_Period;
	private String quote_RateType;
	private String quote_Rate;
	private String quote_QTY;
	private String quote_TotalValue;
	private String quote_TotalValue_Currency;
	
	private String quote_ActSubTotal;
	private String quote_ActTax;
	private String quote_ActTotal;
	
	private String quote_SubTotal;
	private String quote_TaxOther;
	
	private String quote_GrossTotalBookingValue;
	private String quote_TotalTaxOther_2;
	private String quote_TotalBookingValue_3;
	
	
	
	
	public String getQuote_ActSubTotal() {
		return quote_ActSubTotal;
	}
	public void setQuote_ActSubTotal(String quote_ActSubTotal) {
		this.quote_ActSubTotal = quote_ActSubTotal;
	}
	public String getQuote_ActTax() {
		return quote_ActTax;
	}
	public void setQuote_ActTax(String quote_ActTax) {
		this.quote_ActTax = quote_ActTax;
	}
	public String getQuote_ActTotal() {
		return quote_ActTotal;
	}
	public void setQuote_ActTotal(String quote_ActTotal) {
		this.quote_ActTotal = quote_ActTotal;
	}
	public String getQuote_TaxOther() {
		return quote_TaxOther;
	}
	public void setQuote_TaxOther(String quote_TaxOther) {
		this.quote_TaxOther = quote_TaxOther;
	}
	public String getQuote_GrossTotalBookingValue() {
		return quote_GrossTotalBookingValue;
	}
	public void setQuote_GrossTotalBookingValue(String quote_GrossTotalBookingValue) {
		this.quote_GrossTotalBookingValue = quote_GrossTotalBookingValue;
	}
	private String quote_FName;
	private String quote_LName;
	private String quote_TP;
	private String quote_Email;
	private String quote_address;
	private String quote_address_2;
	private String quote_country;
	private String quote_city;
	
	
	public boolean isQuotationMailLoaded() {
		return isQuotationMailLoaded;
	}
	public void setQuotationMailLoaded(boolean isQuotationMailLoaded) {
		this.isQuotationMailLoaded = isQuotationMailLoaded;
	}
	
	public String getQuote_BookingRef() {
		return quote_BookingRef;
	}
	public void setQuote_BookingRef(String quote_BookingRef) {
		this.quote_BookingRef = quote_BookingRef;
	}
	public String getQuote_BookingNo() {
		return quote_BookingNo;
	}
	public void setQuote_BookingNo(String quote_BookingNo) {
		this.quote_BookingNo = quote_BookingNo;
	}
	public ArrayList<String> getQuote_HotelsHeading() {
		return quote_HotelsHeading;
	}
	public void setQuote_HotelsHeading(ArrayList<String> quote_HotelsHeading) {
		this.quote_HotelsHeading = quote_HotelsHeading;
	}
	public ArrayList<String> getQuote_CheckIn() {
		return quote_CheckIn;
	}
	public void setQuote_CheckIn(ArrayList<String> quote_CheckIn) {
		this.quote_CheckIn = quote_CheckIn;
	}
	public ArrayList<String> getQuote_CheckOut() {
		return quote_CheckOut;
	}
	public void setQuote_CheckOut(ArrayList<String> quote_CheckOut) {
		this.quote_CheckOut = quote_CheckOut;
	}
	public ArrayList<String> getQuote_HotelBookingStatus() {
		return quote_HotelBookingStatus;
	}
	public void setQuote_HotelBookingStatus(ArrayList<String> quote_HotelBookingStatus) {
		this.quote_HotelBookingStatus = quote_HotelBookingStatus;
	}
	public ArrayList<String> getQuote_HotelSubTotal() {
		return quote_HotelSubTotal;
	}
	public void setQuote_HotelSubTotal(ArrayList<String> quote_HotelSubTotal) {
		this.quote_HotelSubTotal = quote_HotelSubTotal;
	}
	public ArrayList<String> getQuote_HotelTotalTax() {
		return quote_HotelTotalTax;
	}
	public void setQuote_HotelTotalTax(ArrayList<String> quote_HotelTotalTax) {
		this.quote_HotelTotalTax = quote_HotelTotalTax;
	}
	public ArrayList<String> getQuote_HotelBookingValue() {
		return quote_HotelBookingValue;
	}
	public void setQuote_HotelBookingValue(ArrayList<String> quote_HotelBookingValue) {
		this.quote_HotelBookingValue = quote_HotelBookingValue;
	}
	
	public ArrayList<String> getQuote_title_Occupancy() {
		return quote_title_Occupancy;
	}
	public void setQuote_title_Occupancy(ArrayList<String> quote_title_Occupancy) {
		this.quote_title_Occupancy = quote_title_Occupancy;
	}
	public ArrayList<String> getQuote_firstName_Occupancy() {
		return quote_firstName_Occupancy;
	}
	public void setQuote_firstName_Occupancy(ArrayList<String> quote_firstName_Occupancy) {
		this.quote_firstName_Occupancy = quote_firstName_Occupancy;
	}
	public ArrayList<String> getQuote_lastName_Occupancy() {
		return quote_lastName_Occupancy;
	}
	public void setQuote_lastName_Occupancy(ArrayList<String> quote_lastName_Occupancy) {
		this.quote_lastName_Occupancy = quote_lastName_Occupancy;
	}
	public String getQuote_ActivityType() {
		return quote_ActivityType;
	}
	public void setQuote_ActivityType(String quote_ActivityType) {
		this.quote_ActivityType = quote_ActivityType;
	}
	public String getQuote_BookingStatus() {
		return quote_BookingStatus;
	}
	public void setQuote_BookingStatus(String quote_BookingStatus) {
		this.quote_BookingStatus = quote_BookingStatus;
	}
	public String getQuote_Period() {
		return quote_Period;
	}
	public void setQuote_Period(String quote_Period) {
		this.quote_Period = quote_Period;
	}
	public String getQuote_RateType() {
		return quote_RateType;
	}
	public void setQuote_RateType(String quote_RateType) {
		this.quote_RateType = quote_RateType;
	}
	public String getQuote_Rate() {
		return quote_Rate;
	}
	public void setQuote_Rate(String quote_Rate) {
		this.quote_Rate = quote_Rate;
	}
	public String getQuote_QTY() {
		return quote_QTY;
	}
	public void setQuote_QTY(String quote_QTY) {
		this.quote_QTY = quote_QTY;
	}
	public String getQuote_TotalValue() {
		return quote_TotalValue;
	}
	public void setQuote_TotalValue(String quote_TotalValue) {
		this.quote_TotalValue = quote_TotalValue;
	}
	public String getQuote_TotalValue_Currency() {
		return quote_TotalValue_Currency;
	}
	public void setQuote_TotalValue_Currency(String quote_TotalValue_Currency) {
		this.quote_TotalValue_Currency = quote_TotalValue_Currency;
	}
	
	public String getQuote_SubTotal() {
		return quote_SubTotal;
	}
	public void setQuote_SubTotal(String quote_SubTotal) {
		this.quote_SubTotal = quote_SubTotal;
	}
	
	public String getQuote_TotalTaxOther_2() {
		return quote_TotalTaxOther_2;
	}
	public void setQuote_TotalTaxOther_2(String quote_TotalTaxOther_2) {
		this.quote_TotalTaxOther_2 = quote_TotalTaxOther_2;
	}
	public String getQuote_TotalBookingValue_3() {
		return quote_TotalBookingValue_3;
	}
	public void setQuote_TotalBookingValue_3(String quote_TotalBookingValue_3) {
		this.quote_TotalBookingValue_3 = quote_TotalBookingValue_3;
	}
	
	public String getQuote_FName() {
		return quote_FName;
	}
	public void setQuote_FName(String quote_FName) {
		this.quote_FName = quote_FName;
	}
	public String getQuote_LName() {
		return quote_LName;
	}
	public void setQuote_LName(String quote_LName) {
		this.quote_LName = quote_LName;
	}
	public String getQuote_TP() {
		return quote_TP;
	}
	public void setQuote_TP(String quote_TP) {
		this.quote_TP = quote_TP;
	}
	public String getQuote_Email() {
		return quote_Email;
	}
	public void setQuote_Email(String quote_Email) {
		this.quote_Email = quote_Email;
	}
	public String getQuote_address() {
		return quote_address;
	}
	public void setQuote_address(String quote_address) {
		this.quote_address = quote_address;
	}
	public String getQuote_address_2() {
		return quote_address_2;
	}
	public void setQuote_address_2(String quote_address_2) {
		this.quote_address_2 = quote_address_2;
	}
	public String getQuote_country() {
		return quote_country;
	}
	public void setQuote_country(String quote_country) {
		this.quote_country = quote_country;
	}
	public String getQuote_city() {
		return quote_city;
	}
	public void setQuote_city(String quote_city) {
		this.quote_city = quote_city;
	}
	
	
	

}
