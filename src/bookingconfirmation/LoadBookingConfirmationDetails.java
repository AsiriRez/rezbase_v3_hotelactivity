package bookingconfirmation;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.Search;

public class LoadBookingConfirmationDetails {

	private WebDriver driver;
	private ActivityDetails activitydetails; 
	private Search searchInfo; 
	private BookingConfirmationInfo bookingInfo;
	private BookingConfirmation confirmationDetails;
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> portal_cancelPolicy;
	private ArrayList<String> paticipanTitle;
	private ArrayList<String> paticipanFName;
	private ArrayList<String> paticipanLName;
	private ArrayList<String> Portal_paticipanTitle;
	private ArrayList<String> Portal_paticipanFName;
	private ArrayList<String> Portal_paticipanLName, adultList, childList;
	private String currentDateforImages;
	private Quotation quotationDetails;
	private String sssss;
	
	public LoadBookingConfirmationDetails(ActivityDetails activityDetails, Search searchinfo, WebDriver Driver){
		
		this.activitydetails = activityDetails;
		this.driver = Driver;
		this.searchInfo = searchinfo;
	}
	
	
//	public LoadBookingConfirmationDetails(String string, Search searchInfo2, WebDriver driver2) {
//		this.searchInfo = searchInfo2;
//		this.driver = driver2;
//		this.sssss = string;		
//	}
	

	public BookingConfirmation getConfirmationDetails(WebDriver Driver) throws InterruptedException, IOException{
		
		WebDriverWait wait = new WebDriverWait(driver, 150);	
		bookingInfo = new BookingConfirmationInfo(driver);
		//bookingInfo.viewBookingConfirmationReport(sssss, driver);
		bookingInfo.viewBookingConfirmationReport(activitydetails.getReservationNo(), driver);
		confirmationDetails = new BookingConfirmation();
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		
		
		cancelPolicy = new ArrayList<String>();
		portal_cancelPolicy = new ArrayList<String>();
		paticipanTitle = new ArrayList<String>();
		paticipanFName = new ArrayList<String>();
		paticipanLName = new ArrayList<String>();
		Portal_paticipanTitle = new ArrayList<String>();
		Portal_paticipanFName = new ArrayList<String>();
		Portal_paticipanLName = new ArrayList<String>();
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='hidResId_1']")).click();
		Thread.sleep(1000);
		
		//Customer Confirmation Mail
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer1']")).click();
		Thread.sleep(15000);
		
		driver.switchTo().frame("Myemailformat");
		
		File scrFile231 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile231, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_CustomerConfirmationMail.png"));
        		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).size() != 0) {
			
			String ActivityVaoucherRefNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).getText().substring(32, 45);
			confirmationDetails.setActivityVaoucherRefNo(ActivityVaoucherRefNo);
			
			String CCE_BookingRef = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingRef(CCE_BookingRef);
			
			String CCE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingNo(CCE_BookingNo);
			
			ArrayList<String> CCE_HotelsHeading = new ArrayList<String>();
			ArrayList<String> CCE_CheckIn = new ArrayList<String>();
			ArrayList<String> CCE_CheckOut = new ArrayList<String>();
			ArrayList<String> CCE_HotelBookingStatus = new ArrayList<String>();			
			ArrayList<String> CCE_HotelSubTotal = new ArrayList<String>();
			ArrayList<String> CCE_HotelTotalTax = new ArrayList<String>();
			ArrayList<String> CCE_HotelBookingValue = new ArrayList<String>();
			ArrayList<String> CCE_HotelAmountNow = new ArrayList<String>();
			ArrayList<String> CCE_HotelAmountUpFront = new ArrayList<String>();
			
			final short LOOP_VALUES = 300;			
			outerloop : for (int i = 0; i < LOOP_VALUES; i+=11) {
				
				try {
					
					if (driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+11)+"]/td/strong")).getText().contains("Night")) {
						
						String HotelsHeading = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+11)+"]/td/strong")).getText();
						CCE_HotelsHeading.add(HotelsHeading);
						
						String CheckIn = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+13)+"]/td[2]")).getText().split(": ")[1];
						CCE_CheckIn.add(CheckIn);
						
						String CheckOut = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+14)+"]/td[2]")).getText().split(": ")[1];
						CCE_CheckOut.add(CheckOut);
						
						String HotelBookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+16)+"]/td[2]")).getText().split(": ")[1];	
						CCE_HotelBookingStatus.add(HotelBookingStatus);
						
						String HotelSubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
						CCE_HotelSubTotal.add(HotelSubTotal);
						
						String HotelTotalTax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
						CCE_HotelTotalTax.add(HotelTotalTax);
						
						String HotelBookingValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[4]/td[2]")).getText().replace(",", "").split("\\.")[0];
						CCE_HotelBookingValue.add(HotelBookingValue);
						
						String HotelAmountNow = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[5]/td[2]")).getText().replace(",", "").split("\\.")[0];
						CCE_HotelAmountNow.add(HotelAmountNow);
						
						String HotelAmountUpFront = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[6]/td[2]")).getText().replace(",", "").split("\\.")[0];
						CCE_HotelAmountUpFront.add(HotelAmountUpFront);
												
					}
					
					
				} catch (Exception e) {
					break outerloop;
				}
				
			}
			
			confirmationDetails.setCCE_HotelsHeading(CCE_HotelsHeading);
			confirmationDetails.setCCE_CheckIn(CCE_CheckIn);
			confirmationDetails.setCCE_CheckOut(CCE_CheckOut);
			confirmationDetails.setCCE_HotelBookingStatus(CCE_HotelBookingStatus);
			confirmationDetails.setCCE_HotelSubTotal(CCE_HotelSubTotal);
			confirmationDetails.setCCE_HotelTotalTax(CCE_HotelTotalTax);
			confirmationDetails.setCCE_HotelBookingValue(CCE_HotelBookingValue);
			confirmationDetails.setCCE_HotelAmountNow(CCE_HotelAmountNow);
			confirmationDetails.setCCE_HotelAmountUpFront(CCE_HotelAmountUpFront);
			

			
			//Activity Details
			
			String CCE_ActivityType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[35]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_ActivityType(CCE_ActivityType);
			
			String CCE_BookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_BookingStatus(CCE_BookingStatus);
			
			
			String CCE_Period = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[37]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Period(CCE_Period);
			
			String CCE_RateType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[38]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_RateType(CCE_RateType);
			
			String CCE_Rate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[40]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_Rate(CCE_Rate);
			
			String CCE_QTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_QTY(CCE_QTY);
			
			String CCE_TotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[42]/td[2]/strong")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalValue(CCE_TotalValue);
			
			String CCE_TotalValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[42]/td[1]/strong")).getText().substring(7, 10);
			confirmationDetails.setCCE_TotalValue_Currency(CCE_TotalValue_Currency);
			
					 
			
			String CCE_CurrencyType_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[44]/td[7]/table/tbody/tr[1]/td[2]")).getText();
			confirmationDetails.setCCE_CurrencyType_1(CCE_CurrencyType_1);
			
			String CCE_SubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_SubTotal(CCE_SubTotal);
			
			String CCE_TotalTaxOther_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalTaxOther_1(CCE_TotalTaxOther_1);
			
			
			String CCE_TotalBookingValue_3 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[5]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_TotalBookingValue_3(CCE_TotalBookingValue_3);
			
			String CCE_TotalBookingValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[5]/td[1]")).getText().substring(21, 24);
			confirmationDetails.setCCE_TotalBookingValue_Currency(CCE_TotalBookingValue_Currency);
			
			String CCE_AmountPocessed = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[6]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setCCE_AmountPocessed(CCE_AmountPocessed);
			
			String CCE_AmountDue_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[7]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setCCE_AmountDue_2(CCE_AmountDue_2);
			
			
			
		
			//Occupancy Details
			
			
			String CCE_FName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_FName(CCE_FName);
			
			String CCE_LName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_LName(CCE_LName);
			
			String CCE_TP;
			try {
				CCE_TP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[53]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setCCE_TP(CCE_TP);
			} catch (Exception e1) {
				CCE_TP = "Not Available";
				confirmationDetails.setCCE_TP(CCE_TP);
			}
			
			String CCE_Email = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[54]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_Email(CCE_Email);
			
			String CCE_address = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[53]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_address(CCE_address);
					
			String CCE_country = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCCE_country(CCE_country);
			
			String CCE_city = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[55]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCCE_city(CCE_city);
				
			///
			
			ArrayList<String> title_Occupancy = new ArrayList<String>();
			ArrayList<String> firstName_Occupancy = new ArrayList<String>();
			ArrayList<String> lastName_Occupancy = new ArrayList<String>();
			
			WebElement roomOccuTable = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table"));
			List<WebElement> roomOccuTableTr = roomOccuTable.findElements(By.tagName("tr"));
		
			for (int i = 3; i <= roomOccuTableTr.size(); i++) {
				
				try {
					
					String occutitle = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[2]")).getText();
					
					if (occutitle.contains("M")) {
						
						title_Occupancy.add(occutitle);
						String occuFName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						firstName_Occupancy.add(occuFName);
						String occuLName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						lastName_Occupancy.add(occuLName);
						
					}	
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
			}
			
			WebElement roomOccuTable_Hotel2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[63]/td/table"));
			List<WebElement> roomOccuTableTr_Hotel2 = roomOccuTable_Hotel2.findElements(By.tagName("tr"));
			
			for (int i = 3; i <= roomOccuTableTr_Hotel2.size(); i++) {
				
				try {
					
					String occutitle = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[2]")).getText();
					
					if (occutitle.contains("M")) {
						
						title_Occupancy.add(occutitle);
						String occuFName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						firstName_Occupancy.add(occuFName);
						String occuLName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						lastName_Occupancy.add(occuLName);
						
					}
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
			}
			
			confirmationDetails.setCCE_title_Occupancy(title_Occupancy);
			confirmationDetails.setCCE_firstName_Occupancy(firstName_Occupancy);
			confirmationDetails.setCCE_lastName_Occupancy(lastName_Occupancy);
			
			///
			
			
			WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table"));
			List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
			
			for (int i = 3; i <= rowListForNames.size(); i++) {
				
				String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[3]")).getText();
				paticipanTitle.add(title);	
				
				String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[4]")).getText();
				paticipanFName.add(fName);	
				
				String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[5]")).getText();
				paticipanLName.add(lName);
				
			}
			
			confirmationDetails.setCCE_CusTitle(paticipanTitle);
			confirmationDetails.setCCE_CusFName(paticipanFName);
			confirmationDetails.setCCE_CusLName(paticipanLName);
			
			
			
			String CCE_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[81]/td")).getText();
			confirmationDetails.setCCE_CustomerNote(CCE_CustomerNotes);
			
						
			//HotelOne cancelPolicy
			
			ArrayList<String> hotelOne_CancelPolicy = new ArrayList<String>();
			
			WebElement cancelElementHotelOne = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[91]/td/ul"));
			List<WebElement> liTypes_HotelOne = cancelElementHotelOne.findElements(By.tagName("li"));
			
			for (int i = 1; i <= liTypes_HotelOne.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[91]/td/ul/li["+i+"]")).getText();
				hotelOne_CancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setHotelOne_CanellationPolicy(hotelOne_CancelPolicy);
			
			
			//HotelTwo cancelPolicy
			
			ArrayList<String> hotelTwo_CancelPolicy = new ArrayList<String>();
			
			WebElement cancelElementHotelTwo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[93]/td/ul"));
			List<WebElement> liTypes_HotelTwo = cancelElementHotelTwo.findElements(By.tagName("li"));
			
			for (int i = 1; i <= liTypes_HotelTwo.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[93]/td/ul/li["+i+"]")).getText();
				hotelTwo_CancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setHotelTwo_CanellationPolicy(hotelTwo_CancelPolicy);
			
			
			
			
			WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[95]/td/ul"));
			List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
			
			
			for (int i = 1; i <= liTypes.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[95]/td/ul/li["+i+"]")).getText();
				cancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setCanellationPolicy(cancelPolicy);
			
			
			String CCE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setCCE_Tel_1(CCE_Tel_1);
			
			String CCE_Tel_2;			
			try {
				CCE_Tel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[102]/td")).getText().split(": ")[1];
				confirmationDetails.setCCE_Tel_2(CCE_Tel_2);				
			} catch (Exception e) {
				CCE_Tel_2 = "Not Available";
				confirmationDetails.setCCE_Tel_2(CCE_Tel_2);				
			}
						
			String CCE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setCCE_Email_1(CCE_Email_1);
			String CCE_Email_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[103]/td")).getText().split(": ")[1];
			confirmationDetails.setCCE_Email_2(CCE_Email_2);
			String CCE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setCCE_Website(CCE_Website);
			String CCE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setCCE_Fax(CCE_Fax);
			String CCE_CompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[104]/td/div")).getText();
			confirmationDetails.setCCE_CompanyName(CCE_CompanyName);
			
			
			
		}else{
			
			confirmationDetails.setCustomerConfirmationMailLoaded(false);	
		}
		
		/////////
		
		
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
				
		//Portal Mail
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_portal1']")).click();
		Thread.sleep(15000);
		
		driver.switchTo().frame("Myemailformat");
		
		File scrFile232 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile232, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_PortalMail.png"));
        
        
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).size() != 0) {
					
			String ActivityVaoucherRefNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td")).getText().substring(32, 45);
			confirmationDetails.setActivityVaoucherRefPortalNo(ActivityVaoucherRefNo);
			
			String PortalBookingRef = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).getText().split(": ")[1];
			confirmationDetails.setPortalBookingRef(PortalBookingRef);
			
			String PortalBookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td")).getText().split(": ")[1];
			confirmationDetails.setPortalBookingNo(PortalBookingNo);
			
			ArrayList<String> PortalHotelsHeading = new ArrayList<String>();
			ArrayList<String> PortalCheckIn = new ArrayList<String>();
			ArrayList<String> PortalCheckOut = new ArrayList<String>();
			ArrayList<String> PortalHotelBookingStatus = new ArrayList<String>();			
			ArrayList<String> PortalHotelSubTotal = new ArrayList<String>();
			ArrayList<String> PortalHotelTotalTax = new ArrayList<String>();
			ArrayList<String> PortalHotelBookingValue = new ArrayList<String>();
			ArrayList<String> PortalHotelAmountNow = new ArrayList<String>();
			ArrayList<String> PortalHotelAmountUpFront = new ArrayList<String>();
			
			final short LOOP_VALUES = 300;			
			outerloop : for (int i = 0; i < LOOP_VALUES; i+=11) {
				
				try {
					
					if (driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+11)+"]/td/strong")).getText().contains("Night")) {
						
						String HotelsHeading = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+11)+"]/td/strong")).getText();
						PortalHotelsHeading.add(HotelsHeading);
						
						String CheckIn = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+13)+"]/td[2]")).getText().split(": ")[1];
						PortalCheckIn.add(CheckIn);
						
						String CheckOut = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+14)+"]/td[2]")).getText().split(": ")[1];
						PortalCheckOut.add(CheckOut);
						
						String HotelBookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+16)+"]/td[2]")).getText().split(": ")[1];	
						PortalHotelBookingStatus.add(HotelBookingStatus);
						
						String HotelSubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
						PortalHotelSubTotal.add(HotelSubTotal);
						
						String HotelTotalTax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
						PortalHotelTotalTax.add(HotelTotalTax);
						
						String HotelBookingValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[4]/td[2]")).getText().replace(",", "").split("\\.")[0];
						PortalHotelBookingValue.add(HotelBookingValue);
						
						String HotelAmountNow = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[5]/td[2]")).getText().replace(",", "").split("\\.")[0];
						PortalHotelAmountNow.add(HotelAmountNow);
						
						String HotelAmountUpFront = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+(i+20)+"]/td[7]/table/tbody/tr[6]/td[2]")).getText().replace(",", "").split("\\.")[0];
						PortalHotelAmountUpFront.add(HotelAmountUpFront);
												
					}
					
					
				} catch (Exception e) {
					break outerloop;
				}
				
			}
			
			confirmationDetails.setPortalHotelsHeading(PortalHotelsHeading);
			confirmationDetails.setPortalCheckIn(PortalCheckIn);
			confirmationDetails.setPortalCheckOut(PortalCheckOut);
			confirmationDetails.setPortalHotelBookingStatus(PortalHotelBookingStatus);
			confirmationDetails.setPortalHotelSubTotal(PortalHotelSubTotal);
			confirmationDetails.setPortalHotelTotalTax(PortalHotelTotalTax);
			confirmationDetails.setPortalHotelBookingValue(PortalHotelBookingValue);
			confirmationDetails.setPortalHotelAmountNow(PortalHotelAmountNow);
			confirmationDetails.setPortalHotelAmountUpFront(PortalHotelAmountUpFront);
			

			
			//Activity Details
			
			String PortalActivityType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[35]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalActivityType(PortalActivityType);
			
			String PortalBookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalBookingStatus(PortalBookingStatus);
			
			
			String PortalPeriod = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[37]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalPeriod(PortalPeriod);
			
			String PortalRateType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[38]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalRateType(PortalRateType);
			
			String PortalRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[40]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalRate(PortalRate);
			
			String PortalQTY = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[41]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalQTY(PortalQTY);
			
			String PortalTotalValue = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[42]/td[2]/strong")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalTotalValue(PortalTotalValue);
			
			String PortalTotalValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[42]/td[1]/strong")).getText().substring(7, 10);
			confirmationDetails.setPortalTotalValue_Currency(PortalTotalValue_Currency);
			
					 
			
			String PortalCurrencyType_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[44]/td[7]/table/tbody/tr[1]/td[2]")).getText();
			confirmationDetails.setPortalCurrencyType_1(PortalCurrencyType_1);
			
			String PortalSubTotal = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[2]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalSubTotal(PortalSubTotal);
			
			String PortalTotalTaxOther_1Do = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[3]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalTotalTaxOther_1(PortalTotalTaxOther_1Do);
			
			
			String PortalTotalBookingValue_3 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[5]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalTotalBookingValue_3(PortalTotalBookingValue_3);
			
			String PortalTotalBookingValue_Currency = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[5]/td[1]")).getText().substring(21, 24);
			confirmationDetails.setPortalTotalBookingValue_Currency(PortalTotalBookingValue_Currency);
			
			String PortalAmountPocessed = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[6]/td[2]")).getText().replace(",", "").split("\\.")[0];
			confirmationDetails.setPortalAmountPocessed(PortalAmountPocessed);
			
			String PortalAmountDue_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[47]/td[7]/table/tbody/tr[7]/td[2]")).getText().split("\\.")[0];
			confirmationDetails.setPortalAmountDue_2(PortalAmountDue_2);
			
			
			
		
			//Occupancy Details
			
			
			String PortalFName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalFName(PortalFName);
			
			String PortalLName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[52]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalLName(PortalLName);
			
			String PortalTP;
			try {
				PortalTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[53]/td[8]")).getText().split(": ")[1];
				confirmationDetails.setPortalTP(PortalTP);
			} catch (Exception e1) {
				PortalTP = "Not Available";
				confirmationDetails.setPortalTP(PortalTP);
			}
			
			String PortalEmail = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[54]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setPortalEmail(PortalEmail);
			
			String Portaladdress = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[53]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortaladdress(Portaladdress);
					
			String Portalcountry = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[51]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setPortalcountry(Portalcountry);
			
			String Portalcity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[55]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setPortalcity(Portalcity);
					
			////
			
			ArrayList<String> title_Occupancy = new ArrayList<String>();
			ArrayList<String> firstName_Occupancy = new ArrayList<String>();
			ArrayList<String> lastName_Occupancy = new ArrayList<String>();
			
			WebElement roomOccuTable = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table"));
			List<WebElement> roomOccuTableTr = roomOccuTable.findElements(By.tagName("tr"));
		
			for (int i = 3; i <= roomOccuTableTr.size(); i++) {
				
				try {
					
					String occutitle = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[2]")).getText();
					
					if (occutitle.contains("M")) {
						
						title_Occupancy.add(occutitle);
						String occuFName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						firstName_Occupancy.add(occuFName);
						String occuLName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						lastName_Occupancy.add(occuLName);
						
					}	
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
			}
			
			WebElement roomOccuTable_Hotel2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[63]/td/table"));
			List<WebElement> roomOccuTableTr_Hotel2 = roomOccuTable_Hotel2.findElements(By.tagName("tr"));
			
			for (int i = 3; i <= roomOccuTableTr_Hotel2.size(); i++) {
				
				try {
					
					String occutitle = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[2]")).getText();
					
					if (occutitle.contains("M")) {
						
						title_Occupancy.add(occutitle);
						String occuFName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[3]")).getText();
						firstName_Occupancy.add(occuFName);
						String occuLName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[60]/td/table/tbody/tr["+i+"]/td[4]")).getText();
						lastName_Occupancy.add(occuLName);
						
					}
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
			}
			
			confirmationDetails.setPortal_title_Occupancy(title_Occupancy);
			confirmationDetails.setPortal_firstName_Occupancy(firstName_Occupancy);
			confirmationDetails.setPortal_lastName_Occupancy(lastName_Occupancy);
			
			////
			
			
			WebElement tableId = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table"));
			List<WebElement> rowListForNames = tableId.findElements(By.tagName("tr"));
			
			for (int i = 3; i <= rowListForNames.size(); i++) {
				
				String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[3]")).getText();
				Portal_paticipanTitle.add(title);	
				
				String fName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[4]")).getText();
				Portal_paticipanFName.add(fName);	
				
				String lName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[67]/td/table/tbody/tr["+i+"]/td[5]")).getText();
				Portal_paticipanLName.add(lName);
				
			}
			
			confirmationDetails.setPortalCusTitle(Portal_paticipanTitle);
			confirmationDetails.setPortalCusFName(Portal_paticipanFName);
			confirmationDetails.setPortalCusLName(Portal_paticipanLName);
			
			
			
			String PortalCustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[81]/td")).getText();
			confirmationDetails.setPortalCustomerNote(PortalCustomerNotes);
			
						
			//HotelOne cancelPolicy
			
			ArrayList<String> hotelOne_CancelPolicy = new ArrayList<String>();
			
			WebElement cancelElementHotelOne = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[91]/td/ul"));
			List<WebElement> liTypes_HotelOne = cancelElementHotelOne.findElements(By.tagName("li"));
			
			for (int i = 1; i <= liTypes_HotelOne.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[91]/td/ul/li["+i+"]")).getText();
				hotelOne_CancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setPortal_hotelOne_CanellationPolicy(hotelOne_CancelPolicy);
			
			
			//HotelTwo cancelPolicy
			
			ArrayList<String> hotelTwo_CancelPolicy = new ArrayList<String>();
			
			WebElement cancelElementHotelTwo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[93]/td/ul"));
			List<WebElement> liTypes_HotelTwo = cancelElementHotelTwo.findElements(By.tagName("li"));
			
			for (int i = 1; i <= liTypes_HotelTwo.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[93]/td/ul/li["+i+"]")).getText();
				hotelTwo_CancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setPortal_hotelTwo_CanellationPolicy(hotelTwo_CancelPolicy);
			
			
			
			
			WebElement cancelElement = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[95]/td/ul"));
			List<WebElement> liTypes = cancelElement.findElements(By.tagName("li"));
			
			
			for (int i = 1; i <= liTypes.size(); i++) {
				
				String policyString = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[95]/td/ul/li["+i+"]")).getText();
				portal_cancelPolicy.add(policyString);	
			}
			
			confirmationDetails.setPortal_CanellationPolicy(portal_cancelPolicy);
			
			
			String PortalTel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setPortalTel_1(PortalTel_1);
			
			String PortalTel_2;			
			try {
				PortalTel_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[102]/td")).getText().split(": ")[1];
				confirmationDetails.setPortalTel_2(PortalTel_2);				
			} catch (Exception e) {
				PortalTel_2 = "Not Available";
				confirmationDetails.setPortalTel_2(PortalTel_2);				
			}
			
			String PortalEmail_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setPortalEmail_1(PortalEmail_1);
			String PortalEmail_2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[103]/td")).getText().split(": ")[1];
			confirmationDetails.setPortalEmail_2(PortalEmail_2);
			String PortalWebsite = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setPortalWebsite(PortalWebsite);
			String PortalFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setPortalFax(PortalFax);
			String PortalCompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[104]/td/div")).getText();
			confirmationDetails.setPortalCompanyName(PortalCompanyName);
			
		
			
		}else{
			
			confirmationDetails.setCustomerPortalMailLoaded(false);	
		}
		
		/////////
		
		Thread.sleep(500);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
				
		//Portal Mail
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer2']")).click();
		Thread.sleep(15000);
		
		driver.switchTo().frame("Myemailformat");
		
		File scrFile238 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile238, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_CVMail_Activity.png"));
        
        
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[1]")).size() != 0) {
				
			
			String CVE_LeadPassenger = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
			confirmationDetails.setCVE_LeadPassenger(CVE_LeadPassenger);
			
			String CVE_IssueDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_IssueDate(CVE_IssueDate);
			
//			String CVE_Address1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[2]")).getText().split(": ")[1];
//			confirmationDetails.setCVE_Address1(CVE_Address1);
			
			String CVE_City = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[6]/td[2]")).getText();
			confirmationDetails.setCVE_City(CVE_City);
			
			String CVE_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[5]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_BookingNo(CVE_BookingNo);
			
			String CVE_ActivityName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_ActivityName(CVE_ActivityName);
			
			String CVE_bookingStatus = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setCVE_bookingStatus(CVE_bookingStatus);
			
			
			String CVE_SupplierAdd = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_SupplierAdd(CVE_SupplierAdd);
			
			String CVE_SupplierTP = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[12]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setCVE_SupplierTP(CVE_SupplierTP);
			
			String CVE_ProgramCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[4]")).getText().split(": ")[1];
			confirmationDetails.setCVE_ProgramCity(CVE_ProgramCity);
			
			
			
			String CVE_ActivityTransferDes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[1]")).getText();
			confirmationDetails.setCVE_ActivityTransferDes(CVE_ActivityTransferDes);
			
			String CVE_Duration = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText();
			confirmationDetails.setCVE_Duration(CVE_Duration);
			
			String CVE_RatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[3]")).getText();
			confirmationDetails.setCVE_RatePlan(CVE_RatePlan);
			
			String CVE_Qty = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[4]")).getText();
			confirmationDetails.setCVE_Qty(CVE_Qty);
			
			String CVE_ServiceDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[5]")).getText();
			confirmationDetails.setCVE_ServiceDate(CVE_ServiceDate);
			
//			String CVE_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[24]/td[1]")).getText();
//			confirmationDetails.setCVE_CustomerNotes(CVE_CustomerNotes);
			
			String CvE_Tel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setCVE_Tel_1(CvE_Tel_1);		
			String CvE_Email_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setCVE_Email_1(CvE_Email_1);	
			String CvE_Website = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setCVE_Website(CvE_Website);
			String CvE_Fax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setCVE_Fax(CvE_Fax);
		
		
		}else{
			
			confirmationDetails.setCVEMail_Activity(false);
		}
		
		
		///Hotel 1 Voucher
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		driver.findElement(By.xpath(".//*[@id='hidResId_2']")).click();
		Thread.sleep(1500);
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer2']")).click();
		Thread.sleep(15000);
		
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).size() != 0) {
			
			
			String h1_LeadPassenger = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
			confirmationDetails.setH1_LeadPassenger(h1_LeadPassenger);
			
			String h1_HotelName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[8]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_HotelName(h1_HotelName);
			
			String h1_Address = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[8]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_Address(h1_Address);
			
			String h1_City = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_City(h1_City);
			
			String h1_Status = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_Status(h1_Status);
			
			String h1_Tel = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_Tel(h1_Tel);
			
			String h1_CheckIn = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_CheckIn(h1_CheckIn);
			
			String h1_CheckOut = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_CheckOut(h1_CheckOut);
			
			String h1_RoomType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_RoomType(h1_RoomType);
			
			String h1_BedType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[13]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_BedType(h1_BedType);
			
			String h1_RatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_RatePlan(h1_RatePlan);
			
			String h1_Nights = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[4]")).getText().split(": ")[1];
			confirmationDetails.setH1_Nights(h1_Nights);
			
			String h1_Adult = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_Adult(h1_Adult);
			
			String h1_Children = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_Children(h1_Children);
			
			String h1_Rooms = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH1_Rooms(h1_Rooms);
			
			/*String h1_HotelCon_Tel = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[22]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_HotelCon_Tel(h1_HotelCon_Tel);
			
			String h1_EmailAdd = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[23]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_EmailAdd(h1_EmailAdd);*/
			
			String h1_Starcar = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH1_StarCat(h1_Starcar);
			

			
			ArrayList<String> h1_PassengerNames = new ArrayList<String>();
			ArrayList<String> h1_TypeAge = new ArrayList<String>();
			
			for (int i = 24; i < 29 ; i++) {
				
				String name = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+i+"]/td[2]")).getText();
				h1_PassengerNames.add(name);
				
				String type = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+i+"]/td[3]")).getText();
				h1_TypeAge.add(type);			
			}
			
			confirmationDetails.setH1_PassengerNames(h1_PassengerNames);
			confirmationDetails.setH1_TypeAge(h1_TypeAge);
			
			
			String h1_CusNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[32]/td")).getText();
			confirmationDetails.setH1_CusNotes(h1_CusNotes);
				
			String h1_PortalTel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setH1_PortalTel_1(h1_PortalTel_1);		
			String h1_PortalEmail_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setH1_PortalEmail_1(h1_PortalEmail_1);		
			String h1_PortalWebsite = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setH1_PortalWebsite(h1_PortalWebsite);		
			String h1_PortalFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setH1_PortalFax(h1_PortalFax);		
			String h1_PortalCompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td/div")).getText();
			confirmationDetails.setH1_PortalCompanyName(h1_PortalCompanyName);
			
			
		}else{
			confirmationDetails.setHotel1_Mail(false);
		}
		
		
		/// Hotel 2 mail
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		
		Thread.sleep(2500);
		driver.findElement(By.xpath(".//*[@id='hidResId_2']")).click();
		Thread.sleep(1500);
		
		driver.findElement(By.xpath(".//*[@id='hidResId_3']")).click();
		Thread.sleep(1500);
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer2']")).click();
		Thread.sleep(15000);
		
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).size() != 0) {
			
			
			String h1_LeadPassenger = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
			confirmationDetails.setH2_LeadPassenger(h1_LeadPassenger);
			
			String h1_HotelName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[8]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_HotelName(h1_HotelName);
			
			String h1_Address = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[8]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_Address(h1_Address);
			
			String h1_City = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_City(h1_City);
			
			String h1_Status = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[9]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_Status(h1_Status);
			
			String h1_Tel = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_Tel(h1_Tel);
			
			String h1_CheckIn = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[11]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_CheckIn(h1_CheckIn);
			
			String h1_CheckOut = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[10]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_CheckOut(h1_CheckOut);
			
			String h1_RoomType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_RoomType(h1_RoomType);
			
			String h1_BedType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[13]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_BedType(h1_BedType);
			
			String h1_RatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_RatePlan(h1_RatePlan);
			
			String h1_Nights = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[14]/td[4]")).getText().split(": ")[1];
			confirmationDetails.setH2_Nights(h1_Nights);
			
			String h1_Adult = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_Adult(h1_Adult);
			
			String h1_Children = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[16]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_Children(h1_Children);
			
			String h1_Rooms = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[15]/td[8]")).getText().split(": ")[1];
			confirmationDetails.setH2_Rooms(h1_Rooms);
			
			/*String h1_HotelCon_Tel = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[22]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_HotelCon_Tel(h1_HotelCon_Tel);
			
			String h1_EmailAdd = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[23]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_EmailAdd(h1_EmailAdd);*/
			
			String h1_Starcar = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[17]/td[2]")).getText().split(": ")[1];
			confirmationDetails.setH2_StarCat(h1_Starcar);

			
			ArrayList<String> h1_PassengerNames = new ArrayList<String>();
			ArrayList<String> h1_TypeAge = new ArrayList<String>();
			
			for (int i = 24; i < 29 ; i++) {
				
				String name = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+i+"]/td[2]")).getText();
				h1_PassengerNames.add(name);
				
				String type = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+i+"]/td[3]")).getText();
				h1_TypeAge.add(type);			
			}
			
			confirmationDetails.setH2_PassengerNames(h1_PassengerNames);
			confirmationDetails.setH2_TypeAge(h1_TypeAge);
			
			
			String h1_CusNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[32]/td")).getText();
			confirmationDetails.setH2_CusNotes(h1_CusNotes);
				
			String h1_PortalTel_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[1].split(": ")[1];
			confirmationDetails.setH2_PortalTel_1(h1_PortalTel_1);		
			String h1_PortalEmail_1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[3].split(": ")[1];
			confirmationDetails.setH2_PortalEmail_1(h1_PortalEmail_1);		
			String h1_PortalWebsite = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[4].split(": ")[1];
			confirmationDetails.setH2_PortalWebsite(h1_PortalWebsite);		
			String h1_PortalFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]/div")).getText().split("\n")[2].split(": ")[1];
			confirmationDetails.setH2_PortalFax(h1_PortalFax);		
			String h1_PortalCompanyName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[39]/td/div")).getText();
			confirmationDetails.setH2_PortalCompanyName(h1_PortalCompanyName);
			
			
		}else{
			confirmationDetails.setHotel2_Mail(false);
		}
		
		
		
		return confirmationDetails;
		
	}
	
	public Quotation getQuotationMail(WebDriver Driver){
		
		quotationDetails = new Quotation();
		
		WebDriverWait wait = new WebDriverWait(driver, 200);	
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDate = dateFormatcurrentDate.format(cal.getTime());
		String parentHandle = null;
						
		try {
			
			driver.get(""+PG_Properties.getProperty("Quotation")+""+currentDate+"");
						
			WebElement mailTable = driver.findElement(By.xpath(".//*[@id='filetable']"));
			List<WebElement> trElement = mailTable.findElements(By.tagName("tr"));
			
			for (int j = 4; j < trElement.size(); j++) {
		
				String quotationNumber = driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+j+"]/td[2]/a")).getText();
				//activitydetails.getQuote_QuotationNo()
				if (quotationNumber.contains(activitydetails.getQuote_QuotationNo())) {
				
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr["+j+"]/td[2]/a")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='filetable']/tbody/tr[4]/td[2]/a")).click();					
					Thread.sleep(2000);
					
					parentHandle = driver.getWindowHandle();
					for (String winHandle : driver.getWindowHandles()) {
				        driver.switchTo().window(winHandle); 
				    }
					
				    Thread.sleep(3000);
				    
					/////
				    
				    String quote_BookingNo = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/div/strong")).getText().split("\\.")[1].replaceAll(" ", "");
					quotationDetails.setQuote_BookingNo(quote_BookingNo);
					
					String quotation_BookingRef = driver.findElement(By.xpath("html/body/table/tbody/tr[4]/td/strong")).getText().replaceAll(" ", "");
					quotationDetails.setQuote_BookingRef(quotation_BookingRef);
					
					
					ArrayList<String> quotation_HotelsHeading = new ArrayList<String>();
					ArrayList<String> quotation_CheckIn = new ArrayList<String>();
					ArrayList<String> quotation_CheckOut = new ArrayList<String>();
					ArrayList<String> quotation_HotelBookingStatus = new ArrayList<String>();			
					ArrayList<String> quotation_HotelSubTotal = new ArrayList<String>();
					ArrayList<String> quotation_HotelTotalTax = new ArrayList<String>();
					ArrayList<String> quotation_HotelBookingValue = new ArrayList<String>();
					
					
					
					final short LOOP_VALUES = 300;			
					outerloop : for (int i = 0; i < LOOP_VALUES; i+=14) {
						
						try {
							
							if (driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+13)+"]/td/strong")).getText().contains("Night")) {
								
								String HotelsHeading = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+13)+"]/td/strong")).getText();
								quotation_HotelsHeading.add(HotelsHeading);
								
								String CheckIn = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+15)+"]/td[2]")).getText().split(": ")[1];
								quotation_CheckIn.add(CheckIn);
								
								String CheckOut = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+16)+"]/td[2]")).getText().split(": ")[1];
								quotation_CheckOut.add(CheckOut);
								
								String HotelBookingStatus = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+18)+"]/td[2]")).getText().split(": ")[1];	
								quotation_HotelBookingStatus.add(HotelBookingStatus);
								
								String HotelSubTotal = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+23)+"]/td[8]")).getText().replace(",", "").split("\\.")[0];
								quotation_HotelSubTotal.add(HotelSubTotal);
								
								String HotelTotalTax = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+24)+"]/td[8]")).getText().replace(",", "").split("\\.")[0];
								quotation_HotelTotalTax.add(HotelTotalTax);
								
								String HotelBookingValue = driver.findElement(By.xpath("html/body/table/tbody/tr["+(i+25)+"]/td[8]")).getText().replace(",", "").split("\\.")[0];
								quotation_HotelBookingValue.add(HotelBookingValue);
								
								
							}
							
							
						} catch (Exception e) {
							break outerloop;
						}
						
					}
					
					quotationDetails.setQuote_HotelsHeading(quotation_HotelsHeading);
					quotationDetails.setQuote_CheckIn(quotation_CheckIn);
					quotationDetails.setQuote_CheckOut(quotation_CheckOut);
					quotationDetails.setQuote_HotelBookingStatus(quotation_HotelBookingStatus);
					quotationDetails.setQuote_HotelSubTotal(quotation_HotelSubTotal);
					quotationDetails.setQuote_HotelTotalTax(quotation_HotelTotalTax);
					quotationDetails.setQuote_HotelBookingValue(quotation_HotelBookingValue);
					

					
					//Activity Details
					
					String quotation_ActivityType = driver.findElement(By.xpath("html/body/table/tbody/tr[43]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_ActivityType(quotation_ActivityType);
					
					String quotation_BookingStatus;
					
					try {
						quotation_BookingStatus = driver.findElement(By.xpath("html/body/table/tbody/tr[47]/td[2]")).getText().split(": ")[1];
						quotationDetails.setQuote_BookingStatus(quotation_BookingStatus);
					} catch (Exception e) {
						quotation_BookingStatus = "Not Available";
						quotationDetails.setQuote_BookingStatus(quotation_BookingStatus);
					}
					
										
					String quotation_Period = driver.findElement(By.xpath("html/body/table/tbody/tr[45]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_Period(quotation_Period);
					
					String quotation_RateType = driver.findElement(By.xpath("html/body/table/tbody/tr[46]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_RateType(quotation_RateType);
					
					String quotation_Rate = driver.findElement(By.xpath("html/body/table/tbody/tr[48]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_Rate(quotation_Rate);
					
					String quotation_QTY = driver.findElement(By.xpath("html/body/table/tbody/tr[49]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_QTY(quotation_QTY);
					
					String quotation_TotalValue = driver.findElement(By.xpath("html/body/table/tbody/tr[50]/td[2]")).getText().split(": ")[1].replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_TotalValue(quotation_TotalValue);
					
					String quotation_TotalValue_Currency = driver.findElement(By.xpath("html/body/table/tbody/tr[50]/td[1]/strong")).getText().substring(7, 10);
					quotationDetails.setQuote_TotalValue_Currency(quotation_TotalValue_Currency);
					
					
					
					
					
					
					String quotation_ActSubTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[53]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_ActSubTotal(quotation_ActSubTotal);		 
					
					String quotation_ActTax = driver.findElement(By.xpath("html/body/table/tbody/tr[54]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_ActTax(quotation_ActTax);
										
					String quotation_ActTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[55]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_ActTotal(quotation_ActTotal);
					
					
					/*String quotation_SubTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[59]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_SubTotal(quotation_SubTotal);		 
					
					String quotation_Tax = driver.findElement(By.xpath("html/body/table/tbody/tr[60]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_TaxOther(quotation_Tax);*/
										
					String quotation_TotalGross = driver.findElement(By.xpath("html/body/table/tbody/tr[58]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_GrossTotalBookingValue(quotation_TotalGross);
					
					String quotation_ActSubTotalTax = driver.findElement(By.xpath("html/body/table/tbody/tr[59]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_TotalTaxOther_2(quotation_ActSubTotalTax);		 
					
					String quotation_ActTotalTotal = driver.findElement(By.xpath("html/body/table/tbody/tr[60]/td[8]")).getText().replace(",", "").split("\\.")[0];
					quotationDetails.setQuote_TotalBookingValue_3(quotation_ActTotalTotal);
										
					
				
					//Occupancy Details
					
					
					String quotation_FName = driver.findElement(By.xpath("html/body/table/tbody/tr[64]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_FName(quotation_FName);
					
					String quotation_LName = driver.findElement(By.xpath("html/body/table/tbody/tr[65]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_LName(quotation_LName);
					
					String quotation_TP;
					try {
						quotation_TP = driver.findElement(By.xpath("html/body/table/tbody/tr[65]/td[8]")).getText().split(": ")[1];
						quotationDetails.setQuote_TP(quotation_TP);
					} catch (Exception e1) {
						quotation_TP = "Not Available";
						quotationDetails.setQuote_TP(quotation_TP);
					}
					
					String quotation_Email = driver.findElement(By.xpath("html/body/table/tbody/tr[67]/td[8]")).getText().split(": ")[1];
					quotationDetails.setQuote_Email(quotation_Email);
					
					String quotation_address = driver.findElement(By.xpath("html/body/table/tbody/tr[66]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_address(quotation_address);
							
					String quotation_country = driver.findElement(By.xpath("html/body/table/tbody/tr[64]/td[8]")).getText().split(": ")[1];
					quotationDetails.setQuote_country(quotation_country);
					
					String quotation_city = driver.findElement(By.xpath("html/body/table/tbody/tr[68]/td[2]")).getText().split(": ")[1];
					quotationDetails.setQuote_city(quotation_city);
				    
				    
					
					break;
				}
		
			}
			
			driver.close(); // close newly opened window when done with it
		    driver.switchTo().window(parentHandle); // switch back to the original window
			
		    Thread.sleep(6000);
			
		}catch(Exception e){
			
			quotationDetails.setQuotationMailLoaded(false);
			
		}
		
		
		
		return quotationDetails;
	}
	
	
	
	
	
	
}
