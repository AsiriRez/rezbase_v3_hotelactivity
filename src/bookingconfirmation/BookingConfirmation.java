package bookingconfirmation;

import java.util.ArrayList;

public class BookingConfirmation {

	private boolean isCustomerConfirmationMailLoaded  = true;
	private boolean isCustomerPortalMailLoaded  = true;
	private boolean isSupplier  = true;
	private boolean isCVEMail_Activity  = true;
	
	private String ActivityVaoucherRefNo;
	private String CCE_BookingRef;
	private String CCE_BookingNo;
	
	private ArrayList<String> CCE_HotelsHeading;
	private ArrayList<String> CCE_CheckIn;
	private ArrayList<String> CCE_CheckOut;
	private ArrayList<String> CCE_HotelBookingStatus;	
	private ArrayList<String> CCE_HotelSubTotal;
	private ArrayList<String> CCE_HotelTotalTax;
	private ArrayList<String> CCE_HotelBookingValue;
	private ArrayList<String> CCE_HotelAmountNow;
	private ArrayList<String> CCE_HotelAmountUpFront;
	
	private ArrayList<String> CCE_title_Occupancy;
	private ArrayList<String> CCE_firstName_Occupancy;
	private ArrayList<String> CCE_lastName_Occupancy;
	
	private String CCE_ActivityType;
	private String CCE_BookingStatus;
	private String CCE_Period;
	private String CCE_RateType;
	private String CCE_Rate;
	private String CCE_QTY;
	private String CCE_TotalValue;
	private String CCE_TotalValue_Currency;
	
	private String CCE_CurrencyType_1;
	private String CCE_SubTotal;
	private String CCE_TotalTaxOther_1;
	private String CCE_TotalBookingValue;
	private String CCE_AmountPayableNow;
	private String CCE_AmountDue;
	
	private String CCE_CurrencyType_2;
	private String CCE_SubTotal_2;
	private String CCE_TotalTaxOther_2;
	
	private String CCE_TotalBookingValue_3;
	private String CCE_TotalBookingValue_Currency;
	private String CCE_AmountPocessed;
	private String CCE_AmountDue_2;
	
	private String CCE_FName;
	private String CCE_LName;
	private String CCE_TP;
	private String CCE_Email;
	private String CCE_address;
	private String CCE_address_2;
	private String CCE_country;
	private String CCE_city;
	
	private ArrayList<String> CCE_CusTitle;
	private ArrayList<String> CCE_CusFName;
	private ArrayList<String> CCE_CusLName;
	
	private String CCE_Occupancy_ActivityName;
	private String CCE_Occupancy_FName;
	private String CCE_Occupancy_LName;
	
	private String CCE_PaymentTotalValue;
	
	private ArrayList<String> CanellationPolicy;
	private ArrayList<String> hotelOne_CanellationPolicy;
	private ArrayList<String> hotelTwo_CanellationPolicy;
	
	private int cancelPolicyCount;
	
	private String CCE_ActivityNote;
	private String CCE_CustomerNote;
	
	private String CCE_Tel_1;
	private String CCE_Tel_2;
	private String CCE_Email_1;
	private String CCE_Email_2;
	private String CCE_Website;
	private String CCE_Fax;
	private String CCE_CompanyName;
	
	//////////////////////////////////////////////////////
	
	
	private String ActivityVaoucherRefPortalNo;
	private String PortalBookingRef;
	private String PortalBookingNo;
	
	private ArrayList<String> PortalHotelsHeading;
	private ArrayList<String> PortalCheckIn;
	private ArrayList<String> PortalCheckOut;
	private ArrayList<String> PortalHotelBookingStatus;	
	private ArrayList<String> PortalHotelSubTotal;
	private ArrayList<String> PortalHotelTotalTax;
	private ArrayList<String> PortalHotelBookingValue;
	private ArrayList<String> PortalHotelAmountNow;
	private ArrayList<String> PortalHotelAmountUpFront;
	
	private ArrayList<String> Portal_title_Occupancy;
	private ArrayList<String> Portal_firstName_Occupancy;
	private ArrayList<String> Portal_lastName_Occupancy;
	
	private String PortalActivityType;
	private String PortalBookingStatus;
	private String PortalPeriod;
	private String PortalRateType;
	private String PortalRate;
	private String PortalQTY;
	private String PortalTotalValue;
	private String PortalTotalValue_Currency;
	
	private String PortalCurrencyType_1;
	private String PortalSubTotal;
	private String PortalTotalTaxOther_1;
	private String PortalTotalBookingValue;
	private String PortalAmountPayableNow;
	private String PortalAmountDue;
	
	private String PortalCurrencyType_2;
	private String PortalSubTotal_2;
	private String PortalTotalTaxOther_2;
	
	private String PortalTotalBookingValue_3;
	private String PortalTotalBookingValue_Currency;
	private String PortalAmountPocessed;
	private String PortalAmountDue_2;
	
	private String PortalFName;
	private String PortalLName;
	private String PortalTP;
	private String PortalEmail;
	private String Portaladdress;
	private String Portaladdress_2;
	private String Portalcountry;
	private String Portalcity;
	
	private ArrayList<String> PortalCusTitle;
	private ArrayList<String> PortalCusFName;
	private ArrayList<String> PortalCusLName;
	
	private String PortalOccupancy_ActivityName;
	private String PortalOccupancy_FName;
	private String PortalOccupancy_LName;
	
	private String PortalPaymentTotalValue;
	
	private ArrayList<String> Portal_CanellationPolicy;
	private ArrayList<String> Portal_hotelOne_CanellationPolicy;
	private ArrayList<String> Portal_hotelTwo_CanellationPolicy;
	
	private int Portal_cancelPolicyCount;
	
	private String PortalActivityNote;
	private String PortalCustomerNote;
	
	private String PortalTel_1;
	private String PortalTel_2;
	private String PortalEmail_1;
	private String PortalEmail_2;
	private String PortalWebsite;
	private String PortalFax;
	private String PortalCompanyName;
	
	
	////
	
	
	public String CVE_LeadPassenger;
	public String CVE_IssueDate;
	public String CVE_Address1;
	public String CVE_City;
	public String CVE_BookingNo;
	
	public String CVE_ActivityName;
	public String CVE_bookingStatus;
	public String CVE_SupplierName;
	public String CVE_SupplierAdd;
	public String CVE_SupplierTP;
	public String CVE_ProgramCity;
	
	public String CVE_ActivityTransferDes;
	public String CVE_Duration;
	public String CVE_RatePlan;
	public String CVE_Qty;
	public String CVE_ServiceDate;
	
	public String CVE_CustomerNotes;
	
	public String CVE_Tel_1;
	public String CVE_Tel_2;
	public String CVE_Email_1;
	public String CVE_Email_2;
	public String CVE_Website;
	public String CVE_Fax;
	public String CVE_CompanyName;
	
	///
	
	private boolean isHotel1_Mail  = true;
	private String h1_LeadPassenger;
	private String h1_HotelName;
	private String h1_Address;
	private String h1_City;
	private String h1_Status;
	private String h1_Tel;
	private String h1_CheckIn;
	private String h1_CheckOut;
	private String h1_RoomType;
	private String h1_BedType;
	private String h1_RatePlan;
	private String h1_Nights;
	private String h1_Adult;
	private String h1_Children;
	private String h1_Rooms;
	private String h1_StarCat;
	
	private String h1_HotelCon_Tel;
	private String h1_EmailAdd;
	private ArrayList<String> h1_PassengerNames;
	private ArrayList<String> h1_TypeAge;
	private String h1_CusNotes;
	private String h1_PortalTel_1;
	private String h1_PortalTel_2;
	private String h1_PortalEmail_1;
	private String h1_PortalEmail_2;
	private String h1_PortalWebsite;
	private String h1_PortalFax;
	private String h1_PortalCompanyName;
	
	private boolean isHotel2_Mail  = true;
	private String h2_LeadPassenger;
	private String h2_HotelName;
	private String h2_Address;
	private String h2_City;
	private String h2_Status;
	private String h2_Tel;
	private String h2_CheckIn;
	private String h2_CheckOut;
	private String h2_RoomType;
	private String h2_BedType;
	private String h2_RatePlan;
	private String h2_Nights;
	private String h2_Adult;
	private String h2_Children;
	private String h2_Rooms;
	private String h2_StarCat;
	
	private String h2_HotelCon_Tel;
	private String h2_EmailAdd;
	private ArrayList<String> h2_PassengerNames;
	private ArrayList<String> h2_TypeAge;
	private String h2_CusNotes;
	private String h2_PortalTel_1;
	private String h2_PortalTel_2;
	private String h2_PortalEmail_1;
	private String h2_PortalEmail_2;
	private String h2_PortalWebsite;
	private String h2_PortalFax;
	private String h2_PortalCompanyName;
	
	
	
	
	
	public String getH1_StarCat() {
		return h1_StarCat;
	}
	public void setH1_StarCat(String h1_StarCat) {
		this.h1_StarCat = h1_StarCat;
	}
	public String getH2_StarCat() {
		return h2_StarCat;
	}
	public void setH2_StarCat(String h2_StarCat) {
		this.h2_StarCat = h2_StarCat;
	}
	public boolean isHotel2_Mail() {
		return isHotel2_Mail;
	}
	public void setHotel2_Mail(boolean isHotel2_Mail) {
		this.isHotel2_Mail = isHotel2_Mail;
	}
	public String getH2_LeadPassenger() {
		return h2_LeadPassenger;
	}
	public void setH2_LeadPassenger(String h2_LeadPassenger) {
		this.h2_LeadPassenger = h2_LeadPassenger;
	}
	public String getH2_HotelName() {
		return h2_HotelName;
	}
	public void setH2_HotelName(String h2_HotelName) {
		this.h2_HotelName = h2_HotelName;
	}
	public String getH2_Address() {
		return h2_Address;
	}
	public void setH2_Address(String h2_Address) {
		this.h2_Address = h2_Address;
	}
	public String getH2_City() {
		return h2_City;
	}
	public void setH2_City(String h2_City) {
		this.h2_City = h2_City;
	}
	public String getH2_Status() {
		return h2_Status;
	}
	public void setH2_Status(String h2_Status) {
		this.h2_Status = h2_Status;
	}
	public String getH2_Tel() {
		return h2_Tel;
	}
	public void setH2_Tel(String h2_Tel) {
		this.h2_Tel = h2_Tel;
	}
	public String getH2_CheckIn() {
		return h2_CheckIn;
	}
	public void setH2_CheckIn(String h2_CheckIn) {
		this.h2_CheckIn = h2_CheckIn;
	}
	public String getH2_CheckOut() {
		return h2_CheckOut;
	}
	public void setH2_CheckOut(String h2_CheckOut) {
		this.h2_CheckOut = h2_CheckOut;
	}
	public String getH2_RoomType() {
		return h2_RoomType;
	}
	public void setH2_RoomType(String h2_RoomType) {
		this.h2_RoomType = h2_RoomType;
	}
	public String getH2_BedType() {
		return h2_BedType;
	}
	public void setH2_BedType(String h2_BedType) {
		this.h2_BedType = h2_BedType;
	}
	public String getH2_RatePlan() {
		return h2_RatePlan;
	}
	public void setH2_RatePlan(String h2_RatePlan) {
		this.h2_RatePlan = h2_RatePlan;
	}
	public String getH2_Nights() {
		return h2_Nights;
	}
	public void setH2_Nights(String h2_Nights) {
		this.h2_Nights = h2_Nights;
	}
	public String getH2_Adult() {
		return h2_Adult;
	}
	public void setH2_Adult(String h2_Adult) {
		this.h2_Adult = h2_Adult;
	}
	public String getH2_Children() {
		return h2_Children;
	}
	public void setH2_Children(String h2_Children) {
		this.h2_Children = h2_Children;
	}
	public String getH2_Rooms() {
		return h2_Rooms;
	}
	public void setH2_Rooms(String h2_Rooms) {
		this.h2_Rooms = h2_Rooms;
	}
	public String getH2_HotelCon_Tel() {
		return h2_HotelCon_Tel;
	}
	public void setH2_HotelCon_Tel(String h2_HotelCon_Tel) {
		this.h2_HotelCon_Tel = h2_HotelCon_Tel;
	}
	public String getH2_EmailAdd() {
		return h2_EmailAdd;
	}
	public void setH2_EmailAdd(String h2_EmailAdd) {
		this.h2_EmailAdd = h2_EmailAdd;
	}
	public ArrayList<String> getH2_PassengerNames() {
		return h2_PassengerNames;
	}
	public void setH2_PassengerNames(ArrayList<String> h2_PassengerNames) {
		this.h2_PassengerNames = h2_PassengerNames;
	}
	public ArrayList<String> getH2_TypeAge() {
		return h2_TypeAge;
	}
	public void setH2_TypeAge(ArrayList<String> h2_TypeAge) {
		this.h2_TypeAge = h2_TypeAge;
	}
	public String getH2_CusNotes() {
		return h2_CusNotes;
	}
	public void setH2_CusNotes(String h2_CusNotes) {
		this.h2_CusNotes = h2_CusNotes;
	}
	public String getH2_PortalTel_1() {
		return h2_PortalTel_1;
	}
	public void setH2_PortalTel_1(String h2_PortalTel_1) {
		this.h2_PortalTel_1 = h2_PortalTel_1;
	}
	public String getH2_PortalTel_2() {
		return h2_PortalTel_2;
	}
	public void setH2_PortalTel_2(String h2_PortalTel_2) {
		this.h2_PortalTel_2 = h2_PortalTel_2;
	}
	public String getH2_PortalEmail_1() {
		return h2_PortalEmail_1;
	}
	public void setH2_PortalEmail_1(String h2_PortalEmail_1) {
		this.h2_PortalEmail_1 = h2_PortalEmail_1;
	}
	public String getH2_PortalEmail_2() {
		return h2_PortalEmail_2;
	}
	public void setH2_PortalEmail_2(String h2_PortalEmail_2) {
		this.h2_PortalEmail_2 = h2_PortalEmail_2;
	}
	public String getH2_PortalWebsite() {
		return h2_PortalWebsite;
	}
	public void setH2_PortalWebsite(String h2_PortalWebsite) {
		this.h2_PortalWebsite = h2_PortalWebsite;
	}
	public String getH2_PortalFax() {
		return h2_PortalFax;
	}
	public void setH2_PortalFax(String h2_PortalFax) {
		this.h2_PortalFax = h2_PortalFax;
	}
	public String getH2_PortalCompanyName() {
		return h2_PortalCompanyName;
	}
	public void setH2_PortalCompanyName(String h2_PortalCompanyName) {
		this.h2_PortalCompanyName = h2_PortalCompanyName;
	}
	public boolean isHotel1_Mail() {
		return isHotel1_Mail;
	}
	public void setHotel1_Mail(boolean isHotel1_Mail) {
		this.isHotel1_Mail = isHotel1_Mail;
	}
	public String getH1_LeadPassenger() {
		return h1_LeadPassenger;
	}
	public void setH1_LeadPassenger(String h1_LeadPassenger) {
		this.h1_LeadPassenger = h1_LeadPassenger;
	}
	public String getH1_HotelName() {
		return h1_HotelName;
	}
	public void setH1_HotelName(String h1_HotelName) {
		this.h1_HotelName = h1_HotelName;
	}
	public String getH1_Address() {
		return h1_Address;
	}
	public void setH1_Address(String h1_Address) {
		this.h1_Address = h1_Address;
	}
	public String getH1_City() {
		return h1_City;
	}
	public void setH1_City(String h1_City) {
		this.h1_City = h1_City;
	}
	public String getH1_Status() {
		return h1_Status;
	}
	public void setH1_Status(String h1_Status) {
		this.h1_Status = h1_Status;
	}
	public String getH1_Tel() {
		return h1_Tel;
	}
	public void setH1_Tel(String h1_Tel) {
		this.h1_Tel = h1_Tel;
	}
	public String getH1_CheckIn() {
		return h1_CheckIn;
	}
	public void setH1_CheckIn(String h1_CheckIn) {
		this.h1_CheckIn = h1_CheckIn;
	}
	public String getH1_CheckOut() {
		return h1_CheckOut;
	}
	public void setH1_CheckOut(String h1_CheckOut) {
		this.h1_CheckOut = h1_CheckOut;
	}
	public String getH1_RoomType() {
		return h1_RoomType;
	}
	public void setH1_RoomType(String h1_RoomType) {
		this.h1_RoomType = h1_RoomType;
	}
	public String getH1_BedType() {
		return h1_BedType;
	}
	public void setH1_BedType(String h1_BedType) {
		this.h1_BedType = h1_BedType;
	}
	public String getH1_RatePlan() {
		return h1_RatePlan;
	}
	public void setH1_RatePlan(String h1_RatePlan) {
		this.h1_RatePlan = h1_RatePlan;
	}
	public String getH1_Nights() {
		return h1_Nights;
	}
	public void setH1_Nights(String h1_Nights) {
		this.h1_Nights = h1_Nights;
	}
	public String getH1_Adult() {
		return h1_Adult;
	}
	public void setH1_Adult(String h1_Adult) {
		this.h1_Adult = h1_Adult;
	}
	public String getH1_Children() {
		return h1_Children;
	}
	public void setH1_Children(String h1_Children) {
		this.h1_Children = h1_Children;
	}
	public String getH1_Rooms() {
		return h1_Rooms;
	}
	public void setH1_Rooms(String h1_Rooms) {
		this.h1_Rooms = h1_Rooms;
	}
	public String getH1_HotelCon_Tel() {
		return h1_HotelCon_Tel;
	}
	public void setH1_HotelCon_Tel(String h1_HotelCon_Tel) {
		this.h1_HotelCon_Tel = h1_HotelCon_Tel;
	}
	public String getH1_EmailAdd() {
		return h1_EmailAdd;
	}
	public void setH1_EmailAdd(String h1_EmailAdd) {
		this.h1_EmailAdd = h1_EmailAdd;
	}
	public ArrayList<String> getH1_PassengerNames() {
		return h1_PassengerNames;
	}
	public void setH1_PassengerNames(ArrayList<String> h1_PassengerNames) {
		this.h1_PassengerNames = h1_PassengerNames;
	}
	public ArrayList<String> getH1_TypeAge() {
		return h1_TypeAge;
	}
	public void setH1_TypeAge(ArrayList<String> h1_TypeAge) {
		this.h1_TypeAge = h1_TypeAge;
	}
	public String getH1_CusNotes() {
		return h1_CusNotes;
	}
	public void setH1_CusNotes(String h1_CusNotes) {
		this.h1_CusNotes = h1_CusNotes;
	}
	public String getH1_PortalTel_1() {
		return h1_PortalTel_1;
	}
	public void setH1_PortalTel_1(String h1_PortalTel_1) {
		this.h1_PortalTel_1 = h1_PortalTel_1;
	}
	public String getH1_PortalTel_2() {
		return h1_PortalTel_2;
	}
	public void setH1_PortalTel_2(String h1_PortalTel_2) {
		this.h1_PortalTel_2 = h1_PortalTel_2;
	}
	public String getH1_PortalEmail_1() {
		return h1_PortalEmail_1;
	}
	public void setH1_PortalEmail_1(String h1_PortalEmail_1) {
		this.h1_PortalEmail_1 = h1_PortalEmail_1;
	}
	public String getH1_PortalEmail_2() {
		return h1_PortalEmail_2;
	}
	public void setH1_PortalEmail_2(String h1_PortalEmail_2) {
		this.h1_PortalEmail_2 = h1_PortalEmail_2;
	}
	public String getH1_PortalWebsite() {
		return h1_PortalWebsite;
	}
	public void setH1_PortalWebsite(String h1_PortalWebsite) {
		this.h1_PortalWebsite = h1_PortalWebsite;
	}
	public String getH1_PortalFax() {
		return h1_PortalFax;
	}
	public void setH1_PortalFax(String h1_PortalFax) {
		this.h1_PortalFax = h1_PortalFax;
	}
	public String getH1_PortalCompanyName() {
		return h1_PortalCompanyName;
	}
	public void setH1_PortalCompanyName(String h1_PortalCompanyName) {
		this.h1_PortalCompanyName = h1_PortalCompanyName;
	}
	public boolean isCVEMail_Activity() {
		return isCVEMail_Activity;
	}
	public void setCVEMail_Activity(boolean isCVEMail_Activity) {
		this.isCVEMail_Activity = isCVEMail_Activity;
	}
	public String getCVE_LeadPassenger() {
		return CVE_LeadPassenger;
	}
	public void setCVE_LeadPassenger(String cVE_LeadPassenger) {
		CVE_LeadPassenger = cVE_LeadPassenger;
	}
	public String getCVE_IssueDate() {
		return CVE_IssueDate;
	}
	public void setCVE_IssueDate(String cVE_IssueDate) {
		CVE_IssueDate = cVE_IssueDate;
	}
	public String getCVE_Address1() {
		return CVE_Address1;
	}
	public void setCVE_Address1(String cVE_Address1) {
		CVE_Address1 = cVE_Address1;
	}
	public String getCVE_City() {
		return CVE_City;
	}
	public void setCVE_City(String cVE_City) {
		CVE_City = cVE_City;
	}
	public String getCVE_BookingNo() {
		return CVE_BookingNo;
	}
	public void setCVE_BookingNo(String cVE_BookingNo) {
		CVE_BookingNo = cVE_BookingNo;
	}
	public String getCVE_ActivityName() {
		return CVE_ActivityName;
	}
	public void setCVE_ActivityName(String cVE_ActivityName) {
		CVE_ActivityName = cVE_ActivityName;
	}
	public String getCVE_bookingStatus() {
		return CVE_bookingStatus;
	}
	public void setCVE_bookingStatus(String cVE_bookingStatus) {
		CVE_bookingStatus = cVE_bookingStatus;
	}
	public String getCVE_SupplierName() {
		return CVE_SupplierName;
	}
	public void setCVE_SupplierName(String cVE_SupplierName) {
		CVE_SupplierName = cVE_SupplierName;
	}
	public String getCVE_SupplierAdd() {
		return CVE_SupplierAdd;
	}
	public void setCVE_SupplierAdd(String cVE_SupplierAdd) {
		CVE_SupplierAdd = cVE_SupplierAdd;
	}
	public String getCVE_SupplierTP() {
		return CVE_SupplierTP;
	}
	public void setCVE_SupplierTP(String cVE_SupplierTP) {
		CVE_SupplierTP = cVE_SupplierTP;
	}
	public String getCVE_ProgramCity() {
		return CVE_ProgramCity;
	}
	public void setCVE_ProgramCity(String cVE_ProgramCity) {
		CVE_ProgramCity = cVE_ProgramCity;
	}
	public String getCVE_ActivityTransferDes() {
		return CVE_ActivityTransferDes;
	}
	public void setCVE_ActivityTransferDes(String cVE_ActivityTransferDes) {
		CVE_ActivityTransferDes = cVE_ActivityTransferDes;
	}
	public String getCVE_Duration() {
		return CVE_Duration;
	}
	public void setCVE_Duration(String cVE_Duration) {
		CVE_Duration = cVE_Duration;
	}
	public String getCVE_RatePlan() {
		return CVE_RatePlan;
	}
	public void setCVE_RatePlan(String cVE_RatePlan) {
		CVE_RatePlan = cVE_RatePlan;
	}
	public String getCVE_Qty() {
		return CVE_Qty;
	}
	public void setCVE_Qty(String cVE_Qty) {
		CVE_Qty = cVE_Qty;
	}
	public String getCVE_ServiceDate() {
		return CVE_ServiceDate;
	}
	public void setCVE_ServiceDate(String cVE_ServiceDate) {
		CVE_ServiceDate = cVE_ServiceDate;
	}
	public String getCVE_CustomerNotes() {
		return CVE_CustomerNotes;
	}
	public void setCVE_CustomerNotes(String cVE_CustomerNotes) {
		CVE_CustomerNotes = cVE_CustomerNotes;
	}
	public String getCVE_Tel_1() {
		return CVE_Tel_1;
	}
	public void setCVE_Tel_1(String cVE_Tel_1) {
		CVE_Tel_1 = cVE_Tel_1;
	}
	public String getCVE_Tel_2() {
		return CVE_Tel_2;
	}
	public void setCVE_Tel_2(String cVE_Tel_2) {
		CVE_Tel_2 = cVE_Tel_2;
	}
	public String getCVE_Email_1() {
		return CVE_Email_1;
	}
	public void setCVE_Email_1(String cVE_Email_1) {
		CVE_Email_1 = cVE_Email_1;
	}
	public String getCVE_Email_2() {
		return CVE_Email_2;
	}
	public void setCVE_Email_2(String cVE_Email_2) {
		CVE_Email_2 = cVE_Email_2;
	}
	public String getCVE_Website() {
		return CVE_Website;
	}
	public void setCVE_Website(String cVE_Website) {
		CVE_Website = cVE_Website;
	}
	public String getCVE_Fax() {
		return CVE_Fax;
	}
	public void setCVE_Fax(String cVE_Fax) {
		CVE_Fax = cVE_Fax;
	}
	public String getCVE_CompanyName() {
		return CVE_CompanyName;
	}
	public void setCVE_CompanyName(String cVE_CompanyName) {
		CVE_CompanyName = cVE_CompanyName;
	}
	public ArrayList<String> getCCE_title_Occupancy() {
		return CCE_title_Occupancy;
	}
	public void setCCE_title_Occupancy(ArrayList<String> cCE_title_Occupancy) {
		CCE_title_Occupancy = cCE_title_Occupancy;
	}
	public ArrayList<String> getCCE_firstName_Occupancy() {
		return CCE_firstName_Occupancy;
	}
	public void setCCE_firstName_Occupancy(ArrayList<String> cCE_firstName_Occupancy) {
		CCE_firstName_Occupancy = cCE_firstName_Occupancy;
	}
	public ArrayList<String> getCCE_lastName_Occupancy() {
		return CCE_lastName_Occupancy;
	}
	public void setCCE_lastName_Occupancy(ArrayList<String> cCE_lastName_Occupancy) {
		CCE_lastName_Occupancy = cCE_lastName_Occupancy;
	}
	public ArrayList<String> getPortal_title_Occupancy() {
		return Portal_title_Occupancy;
	}
	public void setPortal_title_Occupancy(ArrayList<String> portal_title_Occupancy) {
		Portal_title_Occupancy = portal_title_Occupancy;
	}
	public ArrayList<String> getPortal_firstName_Occupancy() {
		return Portal_firstName_Occupancy;
	}
	public void setPortal_firstName_Occupancy(
			ArrayList<String> portal_firstName_Occupancy) {
		Portal_firstName_Occupancy = portal_firstName_Occupancy;
	}
	public ArrayList<String> getPortal_lastName_Occupancy() {
		return Portal_lastName_Occupancy;
	}
	public void setPortal_lastName_Occupancy(
			ArrayList<String> portal_lastName_Occupancy) {
		Portal_lastName_Occupancy = portal_lastName_Occupancy;
	}
	public String getActivityVaoucherRefPortalNo() {
		return ActivityVaoucherRefPortalNo;
	}
	public void setActivityVaoucherRefPortalNo(String activityVaoucherRefPortalNo) {
		ActivityVaoucherRefPortalNo = activityVaoucherRefPortalNo;
	}
	public String getPortalBookingRef() {
		return PortalBookingRef;
	}
	public void setPortalBookingRef(String portalBookingRef) {
		PortalBookingRef = portalBookingRef;
	}
	public String getPortalBookingNo() {
		return PortalBookingNo;
	}
	public void setPortalBookingNo(String portalBookingNo) {
		PortalBookingNo = portalBookingNo;
	}
	public ArrayList<String> getPortalHotelsHeading() {
		return PortalHotelsHeading;
	}
	public void setPortalHotelsHeading(ArrayList<String> portalHotelsHeading) {
		PortalHotelsHeading = portalHotelsHeading;
	}
	public ArrayList<String> getPortalCheckIn() {
		return PortalCheckIn;
	}
	public void setPortalCheckIn(ArrayList<String> portalCheckIn) {
		PortalCheckIn = portalCheckIn;
	}
	public ArrayList<String> getPortalCheckOut() {
		return PortalCheckOut;
	}
	public void setPortalCheckOut(ArrayList<String> portalCheckOut) {
		PortalCheckOut = portalCheckOut;
	}
	public ArrayList<String> getPortalHotelBookingStatus() {
		return PortalHotelBookingStatus;
	}
	public void setPortalHotelBookingStatus(
			ArrayList<String> portalHotelBookingStatus) {
		PortalHotelBookingStatus = portalHotelBookingStatus;
	}
	public ArrayList<String> getPortalHotelSubTotal() {
		return PortalHotelSubTotal;
	}
	public void setPortalHotelSubTotal(ArrayList<String> portalHotelSubTotal) {
		PortalHotelSubTotal = portalHotelSubTotal;
	}
	public ArrayList<String> getPortalHotelTotalTax() {
		return PortalHotelTotalTax;
	}
	public void setPortalHotelTotalTax(ArrayList<String> portalHotelTotalTax) {
		PortalHotelTotalTax = portalHotelTotalTax;
	}
	public ArrayList<String> getPortalHotelBookingValue() {
		return PortalHotelBookingValue;
	}
	public void setPortalHotelBookingValue(ArrayList<String> portalHotelBookingValue) {
		PortalHotelBookingValue = portalHotelBookingValue;
	}
	public ArrayList<String> getPortalHotelAmountNow() {
		return PortalHotelAmountNow;
	}
	public void setPortalHotelAmountNow(ArrayList<String> portalHotelAmountNow) {
		PortalHotelAmountNow = portalHotelAmountNow;
	}
	public ArrayList<String> getPortalHotelAmountUpFront() {
		return PortalHotelAmountUpFront;
	}
	public void setPortalHotelAmountUpFront(
			ArrayList<String> portalHotelAmountUpFront) {
		PortalHotelAmountUpFront = portalHotelAmountUpFront;
	}
	public String getPortalActivityType() {
		return PortalActivityType;
	}
	public void setPortalActivityType(String portalActivityType) {
		PortalActivityType = portalActivityType;
	}
	public String getPortalBookingStatus() {
		return PortalBookingStatus;
	}
	public void setPortalBookingStatus(String portalBookingStatus) {
		PortalBookingStatus = portalBookingStatus;
	}
	public String getPortalPeriod() {
		return PortalPeriod;
	}
	public void setPortalPeriod(String portalPeriod) {
		PortalPeriod = portalPeriod;
	}
	public String getPortalRateType() {
		return PortalRateType;
	}
	public void setPortalRateType(String portalRateType) {
		PortalRateType = portalRateType;
	}
	public String getPortalRate() {
		return PortalRate;
	}
	public void setPortalRate(String portalRate) {
		PortalRate = portalRate;
	}
	public String getPortalQTY() {
		return PortalQTY;
	}
	public void setPortalQTY(String portalQTY) {
		PortalQTY = portalQTY;
	}
	public String getPortalTotalValue() {
		return PortalTotalValue;
	}
	public void setPortalTotalValue(String portalTotalValue) {
		PortalTotalValue = portalTotalValue;
	}
	public String getPortalTotalValue_Currency() {
		return PortalTotalValue_Currency;
	}
	public void setPortalTotalValue_Currency(String portalTotalValue_Currency) {
		PortalTotalValue_Currency = portalTotalValue_Currency;
	}
	public String getPortalCurrencyType_1() {
		return PortalCurrencyType_1;
	}
	public void setPortalCurrencyType_1(String portalCurrencyType_1) {
		PortalCurrencyType_1 = portalCurrencyType_1;
	}
	public String getPortalSubTotal() {
		return PortalSubTotal;
	}
	public void setPortalSubTotal(String portalSubTotal) {
		PortalSubTotal = portalSubTotal;
	}
	public String getPortalTotalTaxOther_1() {
		return PortalTotalTaxOther_1;
	}
	public void setPortalTotalTaxOther_1(String portalTotalTaxOther_1) {
		PortalTotalTaxOther_1 = portalTotalTaxOther_1;
	}
	public String getPortalTotalBookingValue() {
		return PortalTotalBookingValue;
	}
	public void setPortalTotalBookingValue(String portalTotalBookingValue) {
		PortalTotalBookingValue = portalTotalBookingValue;
	}
	public String getPortalAmountPayableNow() {
		return PortalAmountPayableNow;
	}
	public void setPortalAmountPayableNow(String portalAmountPayableNow) {
		PortalAmountPayableNow = portalAmountPayableNow;
	}
	public String getPortalAmountDue() {
		return PortalAmountDue;
	}
	public void setPortalAmountDue(String portalAmountDue) {
		PortalAmountDue = portalAmountDue;
	}
	public String getPortalCurrencyType_2() {
		return PortalCurrencyType_2;
	}
	public void setPortalCurrencyType_2(String portalCurrencyType_2) {
		PortalCurrencyType_2 = portalCurrencyType_2;
	}
	public String getPortalSubTotal_2() {
		return PortalSubTotal_2;
	}
	public void setPortalSubTotal_2(String portalSubTotal_2) {
		PortalSubTotal_2 = portalSubTotal_2;
	}
	public String getPortalTotalTaxOther_2() {
		return PortalTotalTaxOther_2;
	}
	public void setPortalTotalTaxOther_2(String portalTotalTaxOther_2) {
		PortalTotalTaxOther_2 = portalTotalTaxOther_2;
	}
	public String getPortalTotalBookingValue_3() {
		return PortalTotalBookingValue_3;
	}
	public void setPortalTotalBookingValue_3(String portalTotalBookingValue_3) {
		PortalTotalBookingValue_3 = portalTotalBookingValue_3;
	}
	public String getPortalTotalBookingValue_Currency() {
		return PortalTotalBookingValue_Currency;
	}
	public void setPortalTotalBookingValue_Currency(
			String portalTotalBookingValue_Currency) {
		PortalTotalBookingValue_Currency = portalTotalBookingValue_Currency;
	}
	public String getPortalAmountPocessed() {
		return PortalAmountPocessed;
	}
	public void setPortalAmountPocessed(String portalAmountPocessed) {
		PortalAmountPocessed = portalAmountPocessed;
	}
	public String getPortalAmountDue_2() {
		return PortalAmountDue_2;
	}
	public void setPortalAmountDue_2(String portalAmountDue_2) {
		PortalAmountDue_2 = portalAmountDue_2;
	}
	public String getPortalFName() {
		return PortalFName;
	}
	public void setPortalFName(String portalFName) {
		PortalFName = portalFName;
	}
	public String getPortalLName() {
		return PortalLName;
	}
	public void setPortalLName(String portalLName) {
		PortalLName = portalLName;
	}
	public String getPortalTP() {
		return PortalTP;
	}
	public void setPortalTP(String portalTP) {
		PortalTP = portalTP;
	}
	public String getPortalEmail() {
		return PortalEmail;
	}
	public void setPortalEmail(String portalEmail) {
		PortalEmail = portalEmail;
	}
	public String getPortaladdress() {
		return Portaladdress;
	}
	public void setPortaladdress(String portaladdress) {
		Portaladdress = portaladdress;
	}
	public String getPortaladdress_2() {
		return Portaladdress_2;
	}
	public void setPortaladdress_2(String portaladdress_2) {
		Portaladdress_2 = portaladdress_2;
	}
	public String getPortalcountry() {
		return Portalcountry;
	}
	public void setPortalcountry(String portalcountry) {
		Portalcountry = portalcountry;
	}
	public String getPortalcity() {
		return Portalcity;
	}
	public void setPortalcity(String portalcity) {
		Portalcity = portalcity;
	}
	public ArrayList<String> getPortalCusTitle() {
		return PortalCusTitle;
	}
	public void setPortalCusTitle(ArrayList<String> portalCusTitle) {
		PortalCusTitle = portalCusTitle;
	}
	public ArrayList<String> getPortalCusFName() {
		return PortalCusFName;
	}
	public void setPortalCusFName(ArrayList<String> portalCusFName) {
		PortalCusFName = portalCusFName;
	}
	public ArrayList<String> getPortalCusLName() {
		return PortalCusLName;
	}
	public void setPortalCusLName(ArrayList<String> portalCusLName) {
		PortalCusLName = portalCusLName;
	}
	public String getPortalOccupancy_ActivityName() {
		return PortalOccupancy_ActivityName;
	}
	public void setPortalOccupancy_ActivityName(String portalOccupancy_ActivityName) {
		PortalOccupancy_ActivityName = portalOccupancy_ActivityName;
	}
	public String getPortalOccupancy_FName() {
		return PortalOccupancy_FName;
	}
	public void setPortalOccupancy_FName(String portalOccupancy_FName) {
		PortalOccupancy_FName = portalOccupancy_FName;
	}
	public String getPortalOccupancy_LName() {
		return PortalOccupancy_LName;
	}
	public void setPortalOccupancy_LName(String portalOccupancy_LName) {
		PortalOccupancy_LName = portalOccupancy_LName;
	}
	public String getPortalPaymentTotalValue() {
		return PortalPaymentTotalValue;
	}
	public void setPortalPaymentTotalValue(String portalPaymentTotalValue) {
		PortalPaymentTotalValue = portalPaymentTotalValue;
	}
	public ArrayList<String> getPortal_CanellationPolicy() {
		return Portal_CanellationPolicy;
	}
	public void setPortal_CanellationPolicy(
			ArrayList<String> portal_CanellationPolicy) {
		Portal_CanellationPolicy = portal_CanellationPolicy;
	}
	public ArrayList<String> getPortal_hotelOne_CanellationPolicy() {
		return Portal_hotelOne_CanellationPolicy;
	}
	public void setPortal_hotelOne_CanellationPolicy(
			ArrayList<String> portal_hotelOne_CanellationPolicy) {
		Portal_hotelOne_CanellationPolicy = portal_hotelOne_CanellationPolicy;
	}
	public ArrayList<String> getPortal_hotelTwo_CanellationPolicy() {
		return Portal_hotelTwo_CanellationPolicy;
	}
	public void setPortal_hotelTwo_CanellationPolicy(
			ArrayList<String> portal_hotelTwo_CanellationPolicy) {
		Portal_hotelTwo_CanellationPolicy = portal_hotelTwo_CanellationPolicy;
	}
	public int getPortal_cancelPolicyCount() {
		return Portal_cancelPolicyCount;
	}
	public void setPortal_cancelPolicyCount(int portal_cancelPolicyCount) {
		Portal_cancelPolicyCount = portal_cancelPolicyCount;
	}
	public String getPortalActivityNote() {
		return PortalActivityNote;
	}
	public void setPortalActivityNote(String portalActivityNote) {
		PortalActivityNote = portalActivityNote;
	}
	public String getPortalCustomerNote() {
		return PortalCustomerNote;
	}
	public void setPortalCustomerNote(String portalCustomerNote) {
		PortalCustomerNote = portalCustomerNote;
	}
	public String getPortalTel_1() {
		return PortalTel_1;
	}
	public void setPortalTel_1(String portalTel_1) {
		PortalTel_1 = portalTel_1;
	}
	public String getPortalTel_2() {
		return PortalTel_2;
	}
	public void setPortalTel_2(String portalTel_2) {
		PortalTel_2 = portalTel_2;
	}
	public String getPortalEmail_1() {
		return PortalEmail_1;
	}
	public void setPortalEmail_1(String portalEmail_1) {
		PortalEmail_1 = portalEmail_1;
	}
	public String getPortalEmail_2() {
		return PortalEmail_2;
	}
	public void setPortalEmail_2(String portalEmail_2) {
		PortalEmail_2 = portalEmail_2;
	}
	public String getPortalWebsite() {
		return PortalWebsite;
	}
	public void setPortalWebsite(String portalWebsite) {
		PortalWebsite = portalWebsite;
	}
	public String getPortalFax() {
		return PortalFax;
	}
	public void setPortalFax(String portalFax) {
		PortalFax = portalFax;
	}
	public String getPortalCompanyName() {
		return PortalCompanyName;
	}
	public void setPortalCompanyName(String portalCompanyName) {
		PortalCompanyName = portalCompanyName;
	}
	public boolean isCustomerPortalMailLoaded() {
		return isCustomerPortalMailLoaded;
	}
	public void setCustomerPortalMailLoaded(boolean isCustomerPortalMailLoaded) {
		this.isCustomerPortalMailLoaded = isCustomerPortalMailLoaded;
	}
	public ArrayList<String> getCCE_HotelsHeading() {
		return CCE_HotelsHeading;
	}
	public void setCCE_HotelsHeading(ArrayList<String> cCE_HotelsHeading) {
		CCE_HotelsHeading = cCE_HotelsHeading;
	}
	public ArrayList<String> getCCE_CheckIn() {
		return CCE_CheckIn;
	}
	public void setCCE_CheckIn(ArrayList<String> cCE_CheckIn) {
		CCE_CheckIn = cCE_CheckIn;
	}
	public ArrayList<String> getCCE_CheckOut() {
		return CCE_CheckOut;
	}
	public void setCCE_CheckOut(ArrayList<String> cCE_CheckOut) {
		CCE_CheckOut = cCE_CheckOut;
	}
	public ArrayList<String> getCCE_HotelBookingStatus() {
		return CCE_HotelBookingStatus;
	}
	public void setCCE_HotelBookingStatus(ArrayList<String> cCE_HotelBookingStatus) {
		CCE_HotelBookingStatus = cCE_HotelBookingStatus;
	}
	public ArrayList<String> getCCE_HotelSubTotal() {
		return CCE_HotelSubTotal;
	}
	public void setCCE_HotelSubTotal(ArrayList<String> cCE_HotelSubTotal) {
		CCE_HotelSubTotal = cCE_HotelSubTotal;
	}
	public ArrayList<String> getCCE_HotelTotalTax() {
		return CCE_HotelTotalTax;
	}
	public void setCCE_HotelTotalTax(ArrayList<String> cCE_HotelTotalTax) {
		CCE_HotelTotalTax = cCE_HotelTotalTax;
	}
	public ArrayList<String> getCCE_HotelBookingValue() {
		return CCE_HotelBookingValue;
	}
	public void setCCE_HotelBookingValue(ArrayList<String> cCE_HotelBookingValue) {
		CCE_HotelBookingValue = cCE_HotelBookingValue;
	}
	public ArrayList<String> getCCE_HotelAmountNow() {
		return CCE_HotelAmountNow;
	}
	public void setCCE_HotelAmountNow(ArrayList<String> cCE_HotelAmountNow) {
		CCE_HotelAmountNow = cCE_HotelAmountNow;
	}
	public ArrayList<String> getCCE_HotelAmountUpFront() {
		return CCE_HotelAmountUpFront;
	}
	public void setCCE_HotelAmountUpFront(ArrayList<String> cCE_HotelAmountUpFront) {
		CCE_HotelAmountUpFront = cCE_HotelAmountUpFront;
	}
	
	
	
	
	
	
	
	public ArrayList<String> getHotelOne_CanellationPolicy() {
		return hotelOne_CanellationPolicy;
	}
	public void setHotelOne_CanellationPolicy(
			ArrayList<String> hotelOne_CanellationPolicy) {
		this.hotelOne_CanellationPolicy = hotelOne_CanellationPolicy;
	}
	public ArrayList<String> getHotelTwo_CanellationPolicy() {
		return hotelTwo_CanellationPolicy;
	}
	public void setHotelTwo_CanellationPolicy(
			ArrayList<String> hotelTwo_CanellationPolicy) {
		this.hotelTwo_CanellationPolicy = hotelTwo_CanellationPolicy;
	}
	public boolean isCustomerConfirmationMailLoaded() {
		return isCustomerConfirmationMailLoaded;
	}
	public void setCustomerConfirmationMailLoaded(
			boolean isCustomerConfirmationMailLoaded) {
		this.isCustomerConfirmationMailLoaded = isCustomerConfirmationMailLoaded;
	}
	
	public boolean isSupplier() {
		return isSupplier;
	}
	public void setSupplier(boolean isSupplier) {
		this.isSupplier = isSupplier;
	}
	public String getCCE_Tel_1() {
		return CCE_Tel_1;
	}
	public void setCCE_Tel_1(String cCE_Tel_1) {
		CCE_Tel_1 = cCE_Tel_1;
	}
	public String getCCE_Tel_2() {
		return CCE_Tel_2;
	}
	public void setCCE_Tel_2(String cCE_Tel_2) {
		CCE_Tel_2 = cCE_Tel_2;
	}
	public String getCCE_Email_1() {
		return CCE_Email_1;
	}
	public void setCCE_Email_1(String cCE_Email_1) {
		CCE_Email_1 = cCE_Email_1;
	}
	public String getCCE_Email_2() {
		return CCE_Email_2;
	}
	public void setCCE_Email_2(String cCE_Email_2) {
		CCE_Email_2 = cCE_Email_2;
	}
	public String getCCE_Website() {
		return CCE_Website;
	}
	public void setCCE_Website(String cCE_Website) {
		CCE_Website = cCE_Website;
	}
	public String getCCE_Fax() {
		return CCE_Fax;
	}
	public void setCCE_Fax(String cCE_Fax) {
		CCE_Fax = cCE_Fax;
	}
	public String getCCE_CompanyName() {
		return CCE_CompanyName;
	}
	public void setCCE_CompanyName(String cCE_CompanyName) {
		CCE_CompanyName = cCE_CompanyName;
	}
	
	public ArrayList<String> getCCE_CusTitle() {
		return CCE_CusTitle;
	}
	public void setCCE_CusTitle(ArrayList<String> cCE_CusTitle) {
		CCE_CusTitle = cCE_CusTitle;
	}
	public ArrayList<String> getCCE_CusFName() {
		return CCE_CusFName;
	}
	public void setCCE_CusFName(ArrayList<String> cCE_CusFName) {
		CCE_CusFName = cCE_CusFName;
	}
	public ArrayList<String> getCCE_CusLName() {
		return CCE_CusLName;
	}
	public void setCCE_CusLName(ArrayList<String> cCE_CusLName) {
		CCE_CusLName = cCE_CusLName;
	}
	
	public String getCCE_ActivityNote() {
		return CCE_ActivityNote;
	}
	public void setCCE_ActivityNote(String cCE_ActivityNote) {
		CCE_ActivityNote = cCE_ActivityNote;
	}
	public String getCCE_CustomerNote() {
		return CCE_CustomerNote;
	}
	public void setCCE_CustomerNote(String cCE_CustomerNote) {
		CCE_CustomerNote = cCE_CustomerNote;
	}
	public int getCancelPolicyCount() {
		return cancelPolicyCount;
	}
	public void setCancelPolicyCount(int cancelPolicyCount) {
		this.cancelPolicyCount = cancelPolicyCount;
	}
	public String getActivityVaoucherRefNo() {
		return ActivityVaoucherRefNo;
	}
	public void setActivityVaoucherRefNo(String activityVaoucherRefNo) {
		ActivityVaoucherRefNo = activityVaoucherRefNo;
	}
	public String getCCE_BookingRef() {
		return CCE_BookingRef;
	}
	public void setCCE_BookingRef(String cCE_BookingRef) {
		CCE_BookingRef = cCE_BookingRef;
	}
	public String getCCE_BookingNo() {
		return CCE_BookingNo;
	}
	public void setCCE_BookingNo(String cCE_BookingNo) {
		CCE_BookingNo = cCE_BookingNo;
	}
	public String getCCE_ActivityType() {
		return CCE_ActivityType;
	}
	public void setCCE_ActivityType(String cCE_ActivityType) {
		CCE_ActivityType = cCE_ActivityType;
	}
	public String getCCE_BookingStatus() {
		return CCE_BookingStatus;
	}
	public void setCCE_BookingStatus(String cCE_BookingStatus) {
		CCE_BookingStatus = cCE_BookingStatus;
	}
	public String getCCE_Period() {
		return CCE_Period;
	}
	public void setCCE_Period(String cCE_Period) {
		CCE_Period = cCE_Period;
	}
	public String getCCE_RateType() {
		return CCE_RateType;
	}
	public void setCCE_RateType(String cCE_RateType) {
		CCE_RateType = cCE_RateType;
	}
	public String getCCE_Rate() {
		return CCE_Rate;
	}
	public void setCCE_Rate(String cCE_Rate) {
		CCE_Rate = cCE_Rate;
	}
	public String getCCE_QTY() {
		return CCE_QTY;
	}
	public void setCCE_QTY(String cCE_QTY) {
		CCE_QTY = cCE_QTY;
	}
	public String getCCE_TotalValue() {
		return CCE_TotalValue;
	}
	public void setCCE_TotalValue(String cCE_TotalValue) {
		CCE_TotalValue = cCE_TotalValue;
	}
	public String getCCE_TotalValue_Currency() {
		return CCE_TotalValue_Currency;
	}
	public void setCCE_TotalValue_Currency(String cCE_TotalValue_Currency) {
		CCE_TotalValue_Currency = cCE_TotalValue_Currency;
	}
	public String getCCE_CurrencyType_1() {
		return CCE_CurrencyType_1;
	}
	public void setCCE_CurrencyType_1(String cCE_CurrencyType_1) {
		CCE_CurrencyType_1 = cCE_CurrencyType_1;
	}
	public String getCCE_SubTotal() {
		return CCE_SubTotal;
	}
	public void setCCE_SubTotal(String cCE_SubTotal) {
		CCE_SubTotal = cCE_SubTotal;
	}
	public String getCCE_TotalTaxOther_1() {
		return CCE_TotalTaxOther_1;
	}
	public void setCCE_TotalTaxOther_1(String cCE_TotalTaxOther_1) {
		CCE_TotalTaxOther_1 = cCE_TotalTaxOther_1;
	}
	public String getCCE_TotalBookingValue() {
		return CCE_TotalBookingValue;
	}
	public void setCCE_TotalBookingValue(String cCE_TotalBookingValue) {
		CCE_TotalBookingValue = cCE_TotalBookingValue;
	}
	public String getCCE_AmountPayableNow() {
		return CCE_AmountPayableNow;
	}
	public void setCCE_AmountPayableNow(String cCE_AmountPayableNow) {
		CCE_AmountPayableNow = cCE_AmountPayableNow;
	}
	public String getCCE_AmountDue() {
		return CCE_AmountDue;
	}
	public void setCCE_AmountDue(String cCE_AmountDue) {
		CCE_AmountDue = cCE_AmountDue;
	}
	public String getCCE_CurrencyType_2() {
		return CCE_CurrencyType_2;
	}
	public void setCCE_CurrencyType_2(String cCE_CurrencyType_2) {
		CCE_CurrencyType_2 = cCE_CurrencyType_2;
	}
	public String getCCE_SubTotal_2() {
		return CCE_SubTotal_2;
	}
	public void setCCE_SubTotal_2(String cCE_SubTotal_2) {
		CCE_SubTotal_2 = cCE_SubTotal_2;
	}
	public String getCCE_TotalTaxOther_2() {
		return CCE_TotalTaxOther_2;
	}
	public void setCCE_TotalTaxOther_2(String cCE_TotalTaxOther_2) {
		CCE_TotalTaxOther_2 = cCE_TotalTaxOther_2;
	}
	public String getCCE_TotalBookingValue_3() {
		return CCE_TotalBookingValue_3;
	}
	public void setCCE_TotalBookingValue_3(String cCE_TotalBookingValue_3) {
		CCE_TotalBookingValue_3 = cCE_TotalBookingValue_3;
	}
	public String getCCE_TotalBookingValue_Currency() {
		return CCE_TotalBookingValue_Currency;
	}
	public void setCCE_TotalBookingValue_Currency(
			String cCE_TotalBookingValue_Currency) {
		CCE_TotalBookingValue_Currency = cCE_TotalBookingValue_Currency;
	}
	public String getCCE_AmountPocessed() {
		return CCE_AmountPocessed;
	}
	public void setCCE_AmountPocessed(String cCE_AmountPocessed) {
		CCE_AmountPocessed = cCE_AmountPocessed;
	}
	public String getCCE_AmountDue_2() {
		return CCE_AmountDue_2;
	}
	public void setCCE_AmountDue_2(String cCE_AmountDue_2) {
		CCE_AmountDue_2 = cCE_AmountDue_2;
	}
	public String getCCE_FName() {
		return CCE_FName;
	}
	public void setCCE_FName(String cCE_FName) {
		CCE_FName = cCE_FName;
	}
	public String getCCE_LName() {
		return CCE_LName;
	}
	public void setCCE_LName(String cCE_LName) {
		CCE_LName = cCE_LName;
	}
	public String getCCE_TP() {
		return CCE_TP;
	}
	public void setCCE_TP(String cCE_TP) {
		CCE_TP = cCE_TP;
	}
	public String getCCE_Email() {
		return CCE_Email;
	}
	public void setCCE_Email(String cCE_Email) {
		CCE_Email = cCE_Email;
	}
	public String getCCE_address() {
		return CCE_address;
	}
	public void setCCE_address(String cCE_address) {
		CCE_address = cCE_address;
	}
	public String getCCE_address_2() {
		return CCE_address_2;
	}
	public void setCCE_address_2(String cCE_address_2) {
		CCE_address_2 = cCE_address_2;
	}
	public String getCCE_country() {
		return CCE_country;
	}
	public void setCCE_country(String cCE_country) {
		CCE_country = cCE_country;
	}
	public String getCCE_city() {
		return CCE_city;
	}
	public void setCCE_city(String cCE_city) {
		CCE_city = cCE_city;
	}
	public String getCCE_Occupancy_ActivityName() {
		return CCE_Occupancy_ActivityName;
	}
	public void setCCE_Occupancy_ActivityName(String cCE_Occupancy_ActivityName) {
		CCE_Occupancy_ActivityName = cCE_Occupancy_ActivityName;
	}
	public String getCCE_Occupancy_FName() {
		return CCE_Occupancy_FName;
	}
	public void setCCE_Occupancy_FName(String cCE_Occupancy_FName) {
		CCE_Occupancy_FName = cCE_Occupancy_FName;
	}
	public String getCCE_Occupancy_LName() {
		return CCE_Occupancy_LName;
	}
	public void setCCE_Occupancy_LName(String cCE_Occupancy_LName) {
		CCE_Occupancy_LName = cCE_Occupancy_LName;
	}
	public String getCCE_PaymentTotalValue() {
		return CCE_PaymentTotalValue;
	}
	public void setCCE_PaymentTotalValue(String cCE_PaymentTotalValue) {
		CCE_PaymentTotalValue = cCE_PaymentTotalValue;
	}
	public ArrayList<String> getCanellationPolicy() {
		return CanellationPolicy;
	}
	public void setCanellationPolicy(ArrayList<String> canellationPolicy) {
		CanellationPolicy = canellationPolicy;
	}
	
	
	
	
}
