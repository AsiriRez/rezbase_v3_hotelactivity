package cancellation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;

public class Cancellation {
	
	private ActivityDetails activityDetails;
	private WebDriver driver;
	
	public Cancellation(ActivityDetails activitydetails, WebDriver driver1){
		this.activityDetails = activitydetails;
		this.driver=driver1;
	}
	
	public void getCancelBookings() throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 90);	
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/operations/reservation/ModificationCancellationInfoPage.do?module=operations");
		
		driver.findElement(By.id("reservationId")).sendKeys(activityDetails.getReservationNo());
		driver.findElement(By.xpath(".//*[@id='reservationId_lkup']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("producttype_hotel")));
		
		//hotelCancel
		driver.findElement(By.xpath(".//*[@id='bkgcancel']")).click();
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
		
		Thread.sleep(8000);
		driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1000);
		
		//
		
		driver.get(PG_Properties.getProperty("Baseurl")+ "/operations/reservation/ModificationCancellationInfoPage.do?module=operations");
		
		driver.findElement(By.id("reservationId")).sendKeys(activityDetails.getReservationNo());
		driver.findElement(By.xpath(".//*[@id='reservationId_lkup']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("producttype_activity")));
		
		//Activity cancel
		
		driver.findElement(By.xpath(".//*[@id='cancellationload_activity']/a")).click();
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("label_td_chargeOnline_Y")));
		
		Thread.sleep(8000);
		driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1000);
		
		
	}

}
