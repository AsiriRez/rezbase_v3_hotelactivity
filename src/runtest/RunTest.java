package runtest;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bookingconfirmation.BookingConfirmation;
import bookingconfirmation.LoadBookingConfirmationDetails;
import bookingconfirmation.Quotation;
import cancellation.Cancellation;

import com.controller.*;
import com.model.*;
import com.opera.core.systems.OperaDriver;
import com.reader.*;

public class RunTest {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	private LoadDetails loadDetails;
	private StringBuffer  PrintWriter;
	private int TestCaseCount;
	private TreeMap<String, Search>   SearchList;
	private int i=0;	
	private String currentDateforImages;
	private ArrayList<Map<Integer, String>> sheetlist;	
	private Map<Integer, String> activitySearchInfoMap 		= null;
	private Map<Integer, String> activityPaymentsMap 		= null;
	private Map<Integer, String> activityInfoMap 		= null;
	private Map<Integer, String> hotelInfoMap 		= null;
	
	@Before
	public void setUp() throws MalformedURLException{

		if (PG_Properties.getProperty("RunMode").equalsIgnoreCase("Local")) {
			profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
			driver 			= new FirefoxDriver(profile);
			
		
		} else {
			
			DesiredCapabilities capabilities = new DesiredCapabilities();
			
			if (PG_Properties.getProperty("Browser").equalsIgnoreCase("firefox")) {
				
				profile 		= new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
				driver 			= new FirefoxDriver(profile);
			
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("chrome")){
				
				System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
				driver = new ChromeDriver();
				  				
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("ie")){
				
				try {
					System.setProperty("webdriver.ie.driver","D:\\IEDriverServer.exe");
					driver = new InternetExplorerDriver();
				
				} catch (Exception e) {					
					e.printStackTrace();
				}
					
			}else if(PG_Properties.getProperty("Browser").equalsIgnoreCase("safari")){
				
				try {
					driver = new SafariDriver();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
								
			}else{				
			
				capabilities.setCapability("opera.binary", "C://Program Files (x86)//Opera//opera.exe");
				capabilities.setCapability("opera.log.level", "CONFIG");
				driver = new OperaDriver(capabilities);
				
			}
			
						
			capabilities.setCapability("name", "Selenium Test Example");
			capabilities.setCapability("build", "1.0");
			capabilities.setCapability("browser_api_name", ""+PG_Properties.getProperty("BrowserVersion")+"");
			capabilities.setCapability("os_api_name", ""+PG_Properties.getProperty("OSVersion")+"");
			capabilities.setCapability("screen_resolution", "1024x768");
			capabilities.setCapability("record_video", "true");
			capabilities.setCapability("record_network", "true");
			capabilities.setCapability("record_snapshot", "false");
			
			driver = new RemoteWebDriver(new URL("http://surani%40rezgateway.com:u6e72a2194362230@hub.crossbrowsertesting.com:80/wd/hub"), capabilities);

		}
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		sheetlist = new ArrayList<Map<Integer, String>>();
		
		PrintWriter = new StringBuffer();
		ExcelReader reader = new ExcelReader();	
		
		sheetlist 	= reader.init(PG_Properties.getProperty("ExcelPath.ActivityReservation"));
				
		loadDetails = new LoadDetails(sheetlist);
			
		activitySearchInfoMap 	= sheetlist.get(0);
		activityPaymentsMap		= sheetlist.get(1);
		activityInfoMap         = sheetlist.get(2);
		hotelInfoMap 			= sheetlist.get(3);
		
		try {
			SearchList = loadDetails.loadActivityReservation(activitySearchInfoMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadPaymentDetails(activityPaymentsMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadActivityDetails(activityInfoMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadHotelDetails(hotelInfoMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void activityReservatiionFlow() throws InterruptedException, IOException, ParseException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>WEB Load Report - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		
		TestCaseCount = 1;
		PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
				
		Iterator<Map.Entry<String, Search>> it = (Iterator<Entry<String, Search>>) SearchList.entrySet().iterator();

		PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>System Availability</td>");
		PrintWriter.append("<td>Should Be Loaded</td>");
		
		if (isCCPageLoaded(driver) == true) {
			
			PrintWriter.append("<td>Page Loaded..!!!!</td>");
			PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
			
			while(it.hasNext()){
				
				ActivityDetails activityDetails = new ActivityDetails();
				Search searchInfo  = it.next().getValue();					
				BookingConfirmation confirmationDetails = new BookingConfirmation();
				Map<String, String> currencyMap = new HashMap<String, String>();
				RateChecker rateCheck = new RateChecker();
				Quotation quoteDetails = new Quotation();
				i++;
				
				TestCaseCount++;
				PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Reservation Flow</td>");
				PrintWriter.append("<td>Should be No Errors</td>");
			
				try {
					
					RatesCalculation ratesCal = new RatesCalculation(searchInfo);
					ActivityDetails activitydetails = ratesCal.getDailyRatesCalculation();
					ReservationFlow reservationFlw = new ReservationFlow(activitydetails, driver, searchInfo);
					driver = reservationFlw.searchEngine(driver);
					activityDetails = reservationFlw.searchResults(driver);
					
					PrintWriter.append("<td>No Errors</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
										
					TestCaseCount++;
					PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
					PrintWriter.append("<td>Should login successfully</td>");
									
					if (login(driver) == true){
						
						PrintWriter.append("<td>Success..!!!</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
						
						TestCaseCount++;
						PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>WEB Reservations</td>");
						PrintWriter.append("<td>Reservation should be made</td>");
						
						/*LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails("Q0019W070815", searchInfo, driver);					
						Quotation quotationDetails = new Quotation();
						quotationDetails = loadConfirmationDetails.getQuotationMail(driver);*/
					
						if (activityDetails.isResultsAvailable() == true && activityDetails.isActivityResultsAvailability() == true && activityDetails.isHotelResultsTwo() == true) {
							
							currencyMap = rateCheck.getExchangeRates(driver);							
							LoadBookingConfirmationDetails loadConfirmationDetails = new LoadBookingConfirmationDetails(activityDetails, searchInfo, driver);					
														
							if(searchInfo.getQuotationReq().equalsIgnoreCase("Yes")){
								
								quoteDetails = loadConfirmationDetails.getQuotationMail(driver);
								
								ReportsReader reports = new ReportsReader(activityDetails, currencyMap, driver, searchInfo, confirmationDetails, quoteDetails);								
								reports.getBasicValidationReport(driver);
								
								PrintWriter.append("<td>Quotation No  :- "+activityDetails.getQuote_QuotationNo()+" - Done</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
							}else{
								
								confirmationDetails = loadConfirmationDetails.getConfirmationDetails(driver);
								
								Cancellation cancelBooking = new Cancellation(activityDetails, driver);
								cancelBooking.getCancelBookings();
								
								
								
								
								ReportsReader reports = new ReportsReader(activityDetails, currencyMap, driver, searchInfo, confirmationDetails, quoteDetails);
								reports.getBasicValidationReport(driver);
								
								PrintWriter.append("<td>Reservation No  :- "+activityDetails.getReservationNo()+" - Done</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
																
							}
																												
						}else{			
							
							if (activityDetails.isResultsAvailable() == false) {
								PrintWriter.append("<td>HOTEL First search >> No Results Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
							
							}else if (activityDetails.isActivityResultsAvailability() == false) {
								PrintWriter.append("<td>ACTIVITY search>> No Results Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
								
							}else if (activityDetails.isHotelResultsTwo() == false) {
								PrintWriter.append("<td>HOTEL Second search >> No Results Available</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
							}
																						
						}	
						
						
					}else{
						
						PrintWriter.append("<td>Login Errorrr..!!!!</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				
				
				} catch (Exception e) {					
					PrintWriter.append("<td>Error :- [["+e.toString()+"]]</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
			        
			        ReportsReader reports = new ReportsReader(activityDetails, currencyMap, driver, searchInfo, confirmationDetails, quoteDetails);
					reports.getBasicValidationReport(driver);
					
				}
			}
			
		
		}else{
			
			PrintWriter.append("<td>Page is Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
		}
		
		
		
		
		PrintWriter.append("</table>");
	}
	
	@After
	public void tearDown() throws IOException{
		
		PrintWriter.append("</body></html>");
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("ActivityInternalReport"))));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
	    driver.quit();
		
	}
	
	public boolean isCCPageLoaded(WebDriver driver_1) throws InterruptedException{
		
		driver = driver_1;
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		Thread.sleep(1500);
		driver.switchTo().defaultContent();
		
		try {			
			WebElement idElement = driver.findElement(By.id("user_id"));
			idElement.isDisplayed();
			return true;
			
		} catch (Exception e) {
			return false;
		}		
	}
	
	public boolean login(WebDriver Driver){

		this.driver = Driver;
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}
	

	
	
}
