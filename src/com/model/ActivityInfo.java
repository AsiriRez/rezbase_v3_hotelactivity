package com.model;

public class ActivityInfo {
	
	private String profir_Markup;
	private String standardCancell_Type;
	private String standardCancell_Value;
	private String noShow_Type;
	private String noShow_Value;
	private String applyIfLessThan;
	private String cancelBuffer;
	private String activityNetRate;
	
	private String salexTax_Type;
	private String salexTax_Value;
	private String misFee_Type;
	private String misFee_Value;
	private String CC_Fee;
	
	
	
	public String getCC_Fee() {
		return CC_Fee;
	}
	public void setCC_Fee(String cC_Fee) {
		CC_Fee = cC_Fee;
	}
	public String getSalexTax_Type() {
		return salexTax_Type;
	}
	public void setSalexTax_Type(String salexTax_Type) {
		this.salexTax_Type = salexTax_Type;
	}
	public String getSalexTax_Value() {
		return salexTax_Value;
	}
	public void setSalexTax_Value(String salexTax_Value) {
		this.salexTax_Value = salexTax_Value;
	}
	public String getMisFee_Type() {
		return misFee_Type;
	}
	public void setMisFee_Type(String misFee_Type) {
		this.misFee_Type = misFee_Type;
	}
	public String getMisFee_Value() {
		return misFee_Value;
	}
	public void setMisFee_Value(String misFee_Value) {
		this.misFee_Value = misFee_Value;
	}
	public String getActivityNetRate() {
		return activityNetRate;
	}
	public void setActivityNetRate(String activityNetRate) {
		this.activityNetRate = activityNetRate;
	}
	public String getProfir_Markup() {
		return profir_Markup;
	}
	public void setProfir_Markup(String profir_Markup) {
		this.profir_Markup = profir_Markup;
	}
	public String getStandardCancell_Type() {
		return standardCancell_Type;
	}
	public void setStandardCancell_Type(String standardCancell_Type) {
		this.standardCancell_Type = standardCancell_Type;
	}
	public String getStandardCancell_Value() {
		return standardCancell_Value;
	}
	public void setStandardCancell_Value(String standardCancell_Value) {
		this.standardCancell_Value = standardCancell_Value;
	}
	public String getNoShow_Type() {
		return noShow_Type;
	}
	public void setNoShow_Type(String noShow_Type) {
		this.noShow_Type = noShow_Type;
	}
	public String getNoShow_Value() {
		return noShow_Value;
	}
	public void setNoShow_Value(String noShow_Value) {
		this.noShow_Value = noShow_Value;
	}
	public String getApplyIfLessThan() {
		return applyIfLessThan;
	}
	public void setApplyIfLessThan(String applyIfLessThan) {
		this.applyIfLessThan = applyIfLessThan;
	}
	public String getCancelBuffer() {
		return cancelBuffer;
	}
	public void setCancelBuffer(String cancelBuffer) {
		this.cancelBuffer = cancelBuffer;
	}
	
	

}
