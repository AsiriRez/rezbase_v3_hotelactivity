package com.model;

public class HotelInfo {
	
	private String scenarioId;	
	private String hotel_Name;	
	private String standardCancell_Type;	
	private String standardCancell_Value;	
	private String noShow_Type;	
	private String noShow_Value;	
	private String Cancellation_ApplyIfLessThan;	
	private String Cancellation_Buffer;	
	private String PM;	
	private String HotelNetRate;	
	private String HotelBookingFee;
	private String city;
	private String address;
	private String Tel;
	private String starCate;
	
	private String salesTax_Type;
	private String salesTax_Value;
	private String occupancy_Type;
	private String occupancy_Value;
	private String energy_Type;
	private String energy_Value;
	
	
	
	
	public String getSalesTax_Type() {
		return salesTax_Type;
	}
	public void setSalesTax_Type(String salesTax_Type) {
		this.salesTax_Type = salesTax_Type;
	}
	public String getSalesTax_Value() {
		return salesTax_Value;
	}
	public void setSalesTax_Value(String salesTax_Value) {
		this.salesTax_Value = salesTax_Value;
	}
	public String getOccupancy_Type() {
		return occupancy_Type;
	}
	public void setOccupancy_Type(String occupancy_Type) {
		this.occupancy_Type = occupancy_Type;
	}
	public String getOccupancy_Value() {
		return occupancy_Value;
	}
	public void setOccupancy_Value(String occupancy_Value) {
		this.occupancy_Value = occupancy_Value;
	}
	public String getEnergy_Type() {
		return energy_Type;
	}
	public void setEnergy_Type(String energy_Type) {
		this.energy_Type = energy_Type;
	}
	public String getEnergy_Value() {
		return energy_Value;
	}
	public void setEnergy_Value(String energy_Value) {
		this.energy_Value = energy_Value;
	}
	public String getStarCate() {
		return starCate;
	}
	public void setStarCate(String starCate) {
		this.starCate = starCate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return Tel;
	}
	public void setTel(String tel) {
		Tel = tel;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getHotel_Name() {
		return hotel_Name;
	}
	public void setHotel_Name(String hotel_Name) {
		this.hotel_Name = hotel_Name;
	}
	public String getStandardCancell_Type() {
		return standardCancell_Type;
	}
	public void setStandardCancell_Type(String standardCancell_Type) {
		this.standardCancell_Type = standardCancell_Type;
	}
	public String getStandardCancell_Value() {
		return standardCancell_Value;
	}
	public void setStandardCancell_Value(String standardCancell_Value) {
		this.standardCancell_Value = standardCancell_Value;
	}
	public String getNoShow_Type() {
		return noShow_Type;
	}
	public void setNoShow_Type(String noShow_Type) {
		this.noShow_Type = noShow_Type;
	}
	public String getNoShow_Value() {
		return noShow_Value;
	}
	public void setNoShow_Value(String noShow_Value) {
		this.noShow_Value = noShow_Value;
	}
	public String getCancellation_ApplyIfLessThan() {
		return Cancellation_ApplyIfLessThan;
	}
	public void setCancellation_ApplyIfLessThan(String cancellation_ApplyIfLessThan) {
		Cancellation_ApplyIfLessThan = cancellation_ApplyIfLessThan;
	}
	public String getCancellation_Buffer() {
		return Cancellation_Buffer;
	}
	public void setCancellation_Buffer(String cancellation_Buffer) {
		Cancellation_Buffer = cancellation_Buffer;
	}
	public String getPM() {
		return PM;
	}
	public void setPM(String pM) {
		PM = pM;
	}
	public String getHotelNetRate() {
		return HotelNetRate;
	}
	public void setHotelNetRate(String hotelNetRate) {
		HotelNetRate = hotelNetRate;
	}
	public String getHotelBookingFee() {
		return HotelBookingFee;
	}
	public void setHotelBookingFee(String hotelBookingFee) {
		HotelBookingFee = hotelBookingFee;
	}
	
	


}
