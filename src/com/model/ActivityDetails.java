package com.model;

import java.util.ArrayList;

public class ActivityDetails {
	
	private String rate_NetRate;
	private String rate_SellRate;
	
	private boolean isBookingEngineLoaded = true;
	private boolean isAddToCart = true;
	private boolean isProgramsAvailabilityActivityTypes = true;
	private boolean isActivityRemoved = true;
	private boolean isCancelPolicy = true;
	private boolean isMoreInfo = true;
	
	private ArrayList<String> hotelSubTotal;
	private ArrayList<String> hotelTotalTax;
	private ArrayList<String> hotelTotalRate;
	private ArrayList<String> hotelRatesName;
	
	private String actSubTotal;
	private String actTotalTax;
	private String actTotalRate;
	
	private boolean isCountryLoaded = true;
	private boolean isCountryListLoaded = true;
	private boolean isCityLoaded = true;
	private boolean isDateFromLoaded = true;
	private boolean isDateToLoaded = true;
	private boolean isAdultsLoaded = true;
	private boolean isChildLoaded = true;
	private boolean isCalenderAvailble = true;
	private boolean isActivityResultsAvailability = true;
	private String showAdd;
	private String HideAdd;
	private String traceId;
	
	private boolean isHotelType = true;
	private boolean isHotelName = true;
	private boolean isPreCurrency = true;
	private boolean isPromoCode = true;
	private boolean isCurrencyListLoaded = true;
	private boolean isHotelTypeListLoaded = true;
	private boolean isStarRating = true;
	private String activityPagePax;
	private String hotel2PagePax;

	private String ccFee;
	private String showResultsAdd;
	private String HideResultsAdd;
	private boolean isResultsProgramCat = true;
	private boolean isResultsPreCurrency = true;
	private boolean isResultsPromoCode = true;
	private boolean isPriceRangeFilter = true;
	private boolean isProgramAvailablity = true;
	private boolean isActivityNameFilter = true;
	private boolean isResultsCurrencyListLoaded = true;
	private boolean isResultsProgCatListLoaded = true;

	private boolean isResultsHotelStarRating = true;
	private boolean isResultsHotelType = true;
	private boolean isResultsHotelName = true;
	private boolean isResultsHotelPrefCurrency = true;
	private boolean isResultsHotelPCode = true;
	private boolean isResultsHotelPRF = true;
	private boolean isResultsHotelPRT = true;
	private boolean isHotelNameFilter = true;
	private boolean isHotelResultsTwo = true;
	private int allPaxCount;
	
	private ArrayList<String> hotel1_PaymentsPagePolicy;
	private ArrayList<String> hotel2_PaymentsPagePolicy;	
	private ArrayList<String> hotel1_ConfirmationPagePolicy;
	private ArrayList<String> hotel2_ConfirmationPagePolicy;
	
	private ArrayList<String> adulttitlePayments_List;
	private ArrayList<String> adultFNamePayments_List;
	private ArrayList<String> adultLNamePayments_List;
	private ArrayList<String> childtitlePayments_List;
	private ArrayList<String> childFNamePayments_List;
	private ArrayList<String> childLNamePayments_List;
	
	private ArrayList<String> adulttitleConfirmations_List;
	private ArrayList<String> adultFNameConfirmations_List;
	private ArrayList<String> adultLNameConfirmations_List;
	private ArrayList<String> childtitleConfirmations_List;
	private ArrayList<String> childFNameConfirmations_List;
	private ArrayList<String> childLNameConfirmations_List;
	
	private ArrayList<String> confirmationPage_HotelName;
	private ArrayList<String> confirmationPage_HotelBookingStatus;				
	private ArrayList<String> confirmationPage_HotelCheckInDate;
	private ArrayList<String> confirmationPage_HotelCheckOutDate;
	private ArrayList<String> confirmationPage_HotelNumberofRooms;
	private ArrayList<String> confirmationPage_HotelNumberofNights;				
	private ArrayList<String> Hotel_SubTotal;
	private ArrayList<String> Hotel_TotalTaxServices;
	private ArrayList<String> Hotel_Totals;
	
	private String activityCurrency;
	private String actDescription = "";
	private int daysCount;
	private String mainRate;
	private String CurrentDate;
	private boolean isResultsAvailable;
	private String addAndContinue;
	private int activityCount;
	private String paymentDetails;
	private int resulteBlockSizeOnReq;
	private ArrayList<String> onRequestLabels;
	
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paymentCancelPolicy;
	
	private String shoppingCart_TotalBasketValue;
	private String shoppingCart_CartCurrency;
	private String shoppingCart_subTotal;
	private String shoppingCart_TotalTax;
	private String shoppingCart_TotalValue;
	private String shoppingCart_AmountNow;
	private String shoppingCart_AmountCheckIn;
		
	private ArrayList<String> resultsPage_ActivityTypes;
	private ArrayList<String> resultsPage_City;
	private ArrayList<String> resultsPage_Period;
	private ArrayList<String> resultsPage_RateType;
	private ArrayList<String> resultsPage_QTY;
	private ArrayList<String> resultsPage_DailyRate;
	
	private ArrayList<Integer> beforeSorting; 
	private ArrayList<Integer> afterSorting;
	private ArrayList<Integer> beforeSortingDescending; 
	private ArrayList<Integer> afterSortingDescending;
	
	
	private long timeLoadpayment;
	private long timeConfirmBooking;
	private long timeSearchButtClickTime;
	private long timeResultsAvailable;
	private long timeAddedtoCart;
	private long timeAailableClick;
	private long timeFirstSearch, timeFirstResults;
	
	private String cartCurrency;
	private String cartVallue;
	
	private String minPrize;
	private String pageLoadedResults;
	private String maxPrize;
	
	private String paymentPage_Title;
	private String paymentPage_FName;
	private String paymentPage_LName;
	private String paymentPage_TP;
	private String paymentPage_Email;
	private String paymentPage_address;
	private String paymentPage_country;
	private String paymentPage_city;
	private String paymentPage_State = "";
	private String paymentPage_PostalCode = "";
	
	private ArrayList<String> resultsPage_cusTitle;
	private ArrayList<String> resultsPage_cusFName;
	private ArrayList<String> resultsPage_cusLName;
	
	private String paymentBilling_CardTotal;
	private String paymentBilling_CardCurrency;
	private String paymentBilling_subTotal;
	private String paymentBilling_TotalTax;
	private String paymentBilling_TotalValue;
	private String paymentBilling_AmountNow;
	private String paymentBilling_AmountCheckIn;
	
	private String customerNotes;
	private String activityNotes;
	
	private String paymentPopUp_ActivityName;
	private String paymentPopUp_Status;
	private String paymentGatewayTotal;
	
	/////////////////////////////////////////////////////////////////////////
	
	private String Quote_QuotationNo;
	private String quote_confirmation_BookingRefference;
	private String quote_reservationNo = "Not Available";
	private String quote_confirmation_CusMailAddress;
	
	private ArrayList<String> quote_confirmationPage_HotelName;
	private ArrayList<String> quote_confirmationPage_HotelBookingStatus;				
	private ArrayList<String> quote_confirmationPage_HotelCheckInDate;
	private ArrayList<String> quote_confirmationPage_HotelCheckOutDate;
	private ArrayList<String> quote_confirmationPage_HotelNumberofRooms;
	private ArrayList<String> quote_confirmationPage_HotelNumberofNights;				
	private ArrayList<String> quote_Hotel_SubTotal;
	private ArrayList<String> quote_Hotel_TotalTaxServices;
	private ArrayList<String> quote_Hotel_Totals;
	
	private String quote_confirmation_BI_ActivityName;
	private String quote_confirmation_BI_City;
	private String quote_confirmation_BI_ActType;
	private String quote_confirmation_BI_BookingStatus;
	private String quote_confirmation_BI_SelectedDate;
	
	private ArrayList<String> quote_confirmation_RateType;
	private ArrayList<String> quote_confirmation_DailyRate;
	private ArrayList<String> quote_confirmation_QTY;
	
	private String quote_confirmation_Currency1;
	private String quote_confirmation_Currency2;
	
	private String quote_confirmation_ActivityRate;
	private String quote_confirmation_SubTotal;
	private String quote_confirmation_Tax;
	private String quote_confirmation_Total;
	
	private String quote_confirmation_SubTotal2;
	private String quote_confirmation_TotalTaxOtherCharges;
	private String quote_confirmation_Total2;
	
	private String quote_confirmation_FName = "Not Available";
	private String quote_confirmation_LName = "Not Available";
	private String quote_confirmation_TP = "Not Available";
	private String quote_confirmation_Email = "Not Available";
	private String quote_confirmation_address = "Not Available";
	private String quote_confirmation_country = "Not Available";
	private String quote_confirmation_city = "Not Available";
	private String quote_confirmation_State = "Not Available";
	private String quote_confirmation_postalCode = "Not Available";
	
	///////////////////////////
	
	private String confirmation_BookingRefference;
	private String reservationNo = "Not Available";
	private String confirmation_CusMailAddress;
	private String confirmation_BI_ActivityName;
	private String confirmation_BI_City;
	private String confirmation_BI_ActType;
	private String confirmation_BI_BookingStatus;
	private String confirmation_BI_SelectedDate;
	
	private ArrayList<String> confirmation_RateType;
	private ArrayList<String> confirmation_DailyRate;
	private ArrayList<String> confirmation_QTY;
	
	private String confirmation_Currency1;
	private String confirmation_Currency2;
	
	private String confirmation_ActivityRate;
	private String confirmation_SubTotal;
	private String confirmation_Tax;
	private String confirmation_Total;
	private String confirmation_SubTotal2;
	private String confirmation_BookingFee;
	private String confirmation_TotalTaxOtherCharges;
	private String confirmation_Total2;
	private String confirmation_AmountNow;
	private String confirmation_AmountDueCheckIn;
	
	private String confirmation_FName = "Not Available";
	private String confirmation_LName = "Not Available";
	private String confirmation_TP = "Not Available";
	private String confirmation_Email = "Not Available";
	private String confirmation_address = "Not Available";
	private String confirmation_country = "Not Available";
	private String confirmation_city = "Not Available";
	private String confirmation_State = "Not Available";
	private String confirmation_postalCode = "Not Available";
	
	private String confirmation_AODActivityName;
	private ArrayList<String> confirmationPage_cusTitle;
	private ArrayList<String> confirmationPage_cusFName;
	private ArrayList<String> confirmationPage_cusLName;
	
	private String confirmation_BillingInfo_TotalValue;
	private ArrayList<String> confirmCancelPolicy;
	
	private String prizeChanged_1;
	private String prizeResults_1;
	private String prizeChanged_2;
	private String prizeResults_2;
	
	private String status = "Available";
	private int gtaActivity;
	private int hbActivity;
	private int rezActivity;
	
	

	
	public String getQuote_confirmation_BookingRefference() {
		return quote_confirmation_BookingRefference;
	}
	public void setQuote_confirmation_BookingRefference(String quote_confirmation_BookingRefference) {
		this.quote_confirmation_BookingRefference = quote_confirmation_BookingRefference;
	}
	public String getQuote_reservationNo() {
		return quote_reservationNo;
	}
	public void setQuote_reservationNo(String quote_reservationNo) {
		this.quote_reservationNo = quote_reservationNo;
	}
	public String getQuote_confirmation_CusMailAddress() {
		return quote_confirmation_CusMailAddress;
	}
	public void setQuote_confirmation_CusMailAddress(String quote_confirmation_CusMailAddress) {
		this.quote_confirmation_CusMailAddress = quote_confirmation_CusMailAddress;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelName() {
		return quote_confirmationPage_HotelName;
	}
	public void setQuote_confirmationPage_HotelName(ArrayList<String> quote_confirmationPage_HotelName) {
		this.quote_confirmationPage_HotelName = quote_confirmationPage_HotelName;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelBookingStatus() {
		return quote_confirmationPage_HotelBookingStatus;
	}
	public void setQuote_confirmationPage_HotelBookingStatus(ArrayList<String> quote_confirmationPage_HotelBookingStatus) {
		this.quote_confirmationPage_HotelBookingStatus = quote_confirmationPage_HotelBookingStatus;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelCheckInDate() {
		return quote_confirmationPage_HotelCheckInDate;
	}
	public void setQuote_confirmationPage_HotelCheckInDate(ArrayList<String> quote_confirmationPage_HotelCheckInDate) {
		this.quote_confirmationPage_HotelCheckInDate = quote_confirmationPage_HotelCheckInDate;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelCheckOutDate() {
		return quote_confirmationPage_HotelCheckOutDate;
	}
	public void setQuote_confirmationPage_HotelCheckOutDate(ArrayList<String> quote_confirmationPage_HotelCheckOutDate) {
		this.quote_confirmationPage_HotelCheckOutDate = quote_confirmationPage_HotelCheckOutDate;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelNumberofRooms() {
		return quote_confirmationPage_HotelNumberofRooms;
	}
	public void setQuote_confirmationPage_HotelNumberofRooms(ArrayList<String> quote_confirmationPage_HotelNumberofRooms) {
		this.quote_confirmationPage_HotelNumberofRooms = quote_confirmationPage_HotelNumberofRooms;
	}
	public ArrayList<String> getQuote_confirmationPage_HotelNumberofNights() {
		return quote_confirmationPage_HotelNumberofNights;
	}
	public void setQuote_confirmationPage_HotelNumberofNights(ArrayList<String> quote_confirmationPage_HotelNumberofNights) {
		this.quote_confirmationPage_HotelNumberofNights = quote_confirmationPage_HotelNumberofNights;
	}
	public ArrayList<String> getQuote_Hotel_SubTotal() {
		return quote_Hotel_SubTotal;
	}
	public void setQuote_Hotel_SubTotal(ArrayList<String> quote_Hotel_SubTotal) {
		this.quote_Hotel_SubTotal = quote_Hotel_SubTotal;
	}
	public ArrayList<String> getQuote_Hotel_TotalTaxServices() {
		return quote_Hotel_TotalTaxServices;
	}
	public void setQuote_Hotel_TotalTaxServices(ArrayList<String> quote_Hotel_TotalTaxServices) {
		this.quote_Hotel_TotalTaxServices = quote_Hotel_TotalTaxServices;
	}
	public ArrayList<String> getQuote_Hotel_Totals() {
		return quote_Hotel_Totals;
	}
	public void setQuote_Hotel_Totals(ArrayList<String> quote_Hotel_Totals) {
		this.quote_Hotel_Totals = quote_Hotel_Totals;
	}
	public String getQuote_confirmation_BI_ActivityName() {
		return quote_confirmation_BI_ActivityName;
	}
	public void setQuote_confirmation_BI_ActivityName(String quote_confirmation_BI_ActivityName) {
		this.quote_confirmation_BI_ActivityName = quote_confirmation_BI_ActivityName;
	}
	public String getQuote_confirmation_BI_City() {
		return quote_confirmation_BI_City;
	}
	public void setQuote_confirmation_BI_City(String quote_confirmation_BI_City) {
		this.quote_confirmation_BI_City = quote_confirmation_BI_City;
	}
	public String getQuote_confirmation_BI_ActType() {
		return quote_confirmation_BI_ActType;
	}
	public void setQuote_confirmation_BI_ActType(String quote_confirmation_BI_ActType) {
		this.quote_confirmation_BI_ActType = quote_confirmation_BI_ActType;
	}
	public String getQuote_confirmation_BI_BookingStatus() {
		return quote_confirmation_BI_BookingStatus;
	}
	public void setQuote_confirmation_BI_BookingStatus(String quote_confirmation_BI_BookingStatus) {
		this.quote_confirmation_BI_BookingStatus = quote_confirmation_BI_BookingStatus;
	}
	public String getQuote_confirmation_BI_SelectedDate() {
		return quote_confirmation_BI_SelectedDate;
	}
	public void setQuote_confirmation_BI_SelectedDate(String quote_confirmation_BI_SelectedDate) {
		this.quote_confirmation_BI_SelectedDate = quote_confirmation_BI_SelectedDate;
	}
	public ArrayList<String> getQuote_confirmation_RateType() {
		return quote_confirmation_RateType;
	}
	public void setQuote_confirmation_RateType(ArrayList<String> quote_confirmation_RateType) {
		this.quote_confirmation_RateType = quote_confirmation_RateType;
	}
	public ArrayList<String> getQuote_confirmation_DailyRate() {
		return quote_confirmation_DailyRate;
	}
	public void setQuote_confirmation_DailyRate(ArrayList<String> quote_confirmation_DailyRate) {
		this.quote_confirmation_DailyRate = quote_confirmation_DailyRate;
	}
	public ArrayList<String> getQuote_confirmation_QTY() {
		return quote_confirmation_QTY;
	}
	public void setQuote_confirmation_QTY(ArrayList<String> quote_confirmation_QTY) {
		this.quote_confirmation_QTY = quote_confirmation_QTY;
	}
	public String getQuote_confirmation_Currency1() {
		return quote_confirmation_Currency1;
	}
	public void setQuote_confirmation_Currency1(String quote_confirmation_Currency1) {
		this.quote_confirmation_Currency1 = quote_confirmation_Currency1;
	}
	public String getQuote_confirmation_Currency2() {
		return quote_confirmation_Currency2;
	}
	public void setQuote_confirmation_Currency2(String quote_confirmation_Currency2) {
		this.quote_confirmation_Currency2 = quote_confirmation_Currency2;
	}
	public String getQuote_confirmation_ActivityRate() {
		return quote_confirmation_ActivityRate;
	}
	public void setQuote_confirmation_ActivityRate(String quote_confirmation_ActivityRate) {
		this.quote_confirmation_ActivityRate = quote_confirmation_ActivityRate;
	}
	public String getQuote_confirmation_SubTotal() {
		return quote_confirmation_SubTotal;
	}
	public void setQuote_confirmation_SubTotal(String quote_confirmation_SubTotal) {
		this.quote_confirmation_SubTotal = quote_confirmation_SubTotal;
	}
	public String getQuote_confirmation_Tax() {
		return quote_confirmation_Tax;
	}
	public void setQuote_confirmation_Tax(String quote_confirmation_Tax) {
		this.quote_confirmation_Tax = quote_confirmation_Tax;
	}
	public String getQuote_confirmation_Total() {
		return quote_confirmation_Total;
	}
	public void setQuote_confirmation_Total(String quote_confirmation_Total) {
		this.quote_confirmation_Total = quote_confirmation_Total;
	}
	public String getQuote_confirmation_SubTotal2() {
		return quote_confirmation_SubTotal2;
	}
	public void setQuote_confirmation_SubTotal2(String quote_confirmation_SubTotal2) {
		this.quote_confirmation_SubTotal2 = quote_confirmation_SubTotal2;
	}
	public String getQuote_confirmation_TotalTaxOtherCharges() {
		return quote_confirmation_TotalTaxOtherCharges;
	}
	public void setQuote_confirmation_TotalTaxOtherCharges(String quote_confirmation_TotalTaxOtherCharges) {
		this.quote_confirmation_TotalTaxOtherCharges = quote_confirmation_TotalTaxOtherCharges;
	}
	public String getQuote_confirmation_Total2() {
		return quote_confirmation_Total2;
	}
	public void setQuote_confirmation_Total2(String quote_confirmation_Total2) {
		this.quote_confirmation_Total2 = quote_confirmation_Total2;
	}
	public String getQuote_confirmation_FName() {
		return quote_confirmation_FName;
	}
	public void setQuote_confirmation_FName(String quote_confirmation_FName) {
		this.quote_confirmation_FName = quote_confirmation_FName;
	}
	public String getQuote_confirmation_LName() {
		return quote_confirmation_LName;
	}
	public void setQuote_confirmation_LName(String quote_confirmation_LName) {
		this.quote_confirmation_LName = quote_confirmation_LName;
	}
	public String getQuote_confirmation_TP() {
		return quote_confirmation_TP;
	}
	public void setQuote_confirmation_TP(String quote_confirmation_TP) {
		this.quote_confirmation_TP = quote_confirmation_TP;
	}
	public String getQuote_confirmation_Email() {
		return quote_confirmation_Email;
	}
	public void setQuote_confirmation_Email(String quote_confirmation_Email) {
		this.quote_confirmation_Email = quote_confirmation_Email;
	}
	public String getQuote_confirmation_address() {
		return quote_confirmation_address;
	}
	public void setQuote_confirmation_address(String quote_confirmation_address) {
		this.quote_confirmation_address = quote_confirmation_address;
	}
	public String getQuote_confirmation_country() {
		return quote_confirmation_country;
	}
	public void setQuote_confirmation_country(String quote_confirmation_country) {
		this.quote_confirmation_country = quote_confirmation_country;
	}
	public String getQuote_confirmation_city() {
		return quote_confirmation_city;
	}
	public void setQuote_confirmation_city(String quote_confirmation_city) {
		this.quote_confirmation_city = quote_confirmation_city;
	}
	public String getQuote_confirmation_State() {
		return quote_confirmation_State;
	}
	public void setQuote_confirmation_State(String quote_confirmation_State) {
		this.quote_confirmation_State = quote_confirmation_State;
	}
	public String getQuote_confirmation_postalCode() {
		return quote_confirmation_postalCode;
	}
	public void setQuote_confirmation_postalCode(String quote_confirmation_postalCode) {
		this.quote_confirmation_postalCode = quote_confirmation_postalCode;
	}
	public String getShoppingCart_TotalBasketValue() {
		return shoppingCart_TotalBasketValue;
	}
	public void setShoppingCart_TotalBasketValue(
			String shoppingCart_TotalBasketValue) {
		this.shoppingCart_TotalBasketValue = shoppingCart_TotalBasketValue;
	}
	public String getConfirmation_ActivityRate() {
		return confirmation_ActivityRate;
	}
	public void setConfirmation_ActivityRate(String confirmation_ActivityRate) {
		this.confirmation_ActivityRate = confirmation_ActivityRate;
	}
	public String getCcFee() {
		return ccFee;
	}
	public void setCcFee(String ccFee) {
		this.ccFee = ccFee;
	}
	public String getActSubTotal() {
		return actSubTotal;
	}
	public void setActSubTotal(String actSubTotal) {
		this.actSubTotal = actSubTotal;
	}
	public String getActTotalTax() {
		return actTotalTax;
	}
	public void setActTotalTax(String actTotalTax) {
		this.actTotalTax = actTotalTax;
	}
	public String getActTotalRate() {
		return actTotalRate;
	}
	public void setActTotalRate(String actTotalRate) {
		this.actTotalRate = actTotalRate;
	}
	public ArrayList<String> getHotelRatesName() {
		return hotelRatesName;
	}
	public void setHotelRatesName(ArrayList<String> hotelRatesName) {
		this.hotelRatesName = hotelRatesName;
	}
	public ArrayList<String> getHotelSubTotal() {
		return hotelSubTotal;
	}
	public void setHotelSubTotal(ArrayList<String> hotelSubTotal) {
		this.hotelSubTotal = hotelSubTotal;
	}
	public ArrayList<String> getHotelTotalTax() {
		return hotelTotalTax;
	}
	public void setHotelTotalTax(ArrayList<String> hotelTotalTax) {
		this.hotelTotalTax = hotelTotalTax;
	}
	public ArrayList<String> getHotelTotalRate() {
		return hotelTotalRate;
	}
	public void setHotelTotalRate(ArrayList<String> hotelTotalRate) {
		this.hotelTotalRate = hotelTotalRate;
	}
	public ArrayList<String> getHotel1_PaymentsPagePolicy() {
		return hotel1_PaymentsPagePolicy;
	}
	public void setHotel1_PaymentsPagePolicy(
			ArrayList<String> hotel1_PaymentsPagePolicy) {
		this.hotel1_PaymentsPagePolicy = hotel1_PaymentsPagePolicy;
	}
	public ArrayList<String> getHotel2_PaymentsPagePolicy() {
		return hotel2_PaymentsPagePolicy;
	}
	public void setHotel2_PaymentsPagePolicy(
			ArrayList<String> hotel2_PaymentsPagePolicy) {
		this.hotel2_PaymentsPagePolicy = hotel2_PaymentsPagePolicy;
	}
	public ArrayList<String> getHotel1_ConfirmationPagePolicy() {
		return hotel1_ConfirmationPagePolicy;
	}
	public void setHotel1_ConfirmationPagePolicy(
			ArrayList<String> hotel1_ConfirmationPagePolicy) {
		this.hotel1_ConfirmationPagePolicy = hotel1_ConfirmationPagePolicy;
	}
	public ArrayList<String> getHotel2_ConfirmationPagePolicy() {
		return hotel2_ConfirmationPagePolicy;
	}
	public void setHotel2_ConfirmationPagePolicy(
			ArrayList<String> hotel2_ConfirmationPagePolicy) {
		this.hotel2_ConfirmationPagePolicy = hotel2_ConfirmationPagePolicy;
	}
	public boolean isHotelResultsTwo() {
		return isHotelResultsTwo;
	}
	public void setHotelResultsTwo(boolean isHotelResultsTwo) {
		this.isHotelResultsTwo = isHotelResultsTwo;
	}
	public String getHotel2PagePax() {
		return hotel2PagePax;
	}
	public void setHotel2PagePax(String hotel2PagePax) {
		this.hotel2PagePax = hotel2PagePax;
	}
	public String getActivityPagePax() {
		return activityPagePax;
	}
	public void setActivityPagePax(String activityPagePax) {
		this.activityPagePax = activityPagePax;
	}
	public boolean isStarRating() {
		return isStarRating;
	}
	public void setStarRating(boolean isStarRating) {
		this.isStarRating = isStarRating;
	}
	public boolean isHotelName() {
		return isHotelName;
	}
	public void setHotelName(boolean isHotelName) {
		this.isHotelName = isHotelName;
	}
	public boolean isHotelTypeListLoaded() {
		return isHotelTypeListLoaded;
	}
	public void setHotelTypeListLoaded(boolean isHotelTypeListLoaded) {
		this.isHotelTypeListLoaded = isHotelTypeListLoaded;
	}
	public boolean isHotelType() {
		return isHotelType;
	}
	public void setHotelType(boolean isHotelType) {
		this.isHotelType = isHotelType;
	}
	

	public boolean isActivityResultsAvailability() {
		return isActivityResultsAvailability;
	}
	public void setActivityResultsAvailability(boolean isActivityResultsAvailability) {
		this.isActivityResultsAvailability = isActivityResultsAvailability;
	}
	
	public ArrayList<String> getConfirmationPage_HotelName() {
		return confirmationPage_HotelName;
	}
	public void setConfirmationPage_HotelName(
			ArrayList<String> confirmationPage_HotelName) {
		this.confirmationPage_HotelName = confirmationPage_HotelName;
	}
	public ArrayList<String> getConfirmationPage_HotelBookingStatus() {
		return confirmationPage_HotelBookingStatus;
	}
	public void setConfirmationPage_HotelBookingStatus(
			ArrayList<String> confirmationPage_HotelBookingStatus) {
		this.confirmationPage_HotelBookingStatus = confirmationPage_HotelBookingStatus;
	}
	public ArrayList<String> getConfirmationPage_HotelCheckInDate() {
		return confirmationPage_HotelCheckInDate;
	}
	public void setConfirmationPage_HotelCheckInDate(
			ArrayList<String> confirmationPage_HotelCheckInDate) {
		this.confirmationPage_HotelCheckInDate = confirmationPage_HotelCheckInDate;
	}
	public ArrayList<String> getConfirmationPage_HotelCheckOutDate() {
		return confirmationPage_HotelCheckOutDate;
	}
	public void setConfirmationPage_HotelCheckOutDate(
			ArrayList<String> confirmationPage_HotelCheckOutDate) {
		this.confirmationPage_HotelCheckOutDate = confirmationPage_HotelCheckOutDate;
	}
	public ArrayList<String> getConfirmationPage_HotelNumberofRooms() {
		return confirmationPage_HotelNumberofRooms;
	}
	public void setConfirmationPage_HotelNumberofRooms(
			ArrayList<String> confirmationPage_HotelNumberofRooms) {
		this.confirmationPage_HotelNumberofRooms = confirmationPage_HotelNumberofRooms;
	}
	public ArrayList<String> getConfirmationPage_HotelNumberofNights() {
		return confirmationPage_HotelNumberofNights;
	}
	public void setConfirmationPage_HotelNumberofNights(
			ArrayList<String> confirmationPage_HotelNumberofNights) {
		this.confirmationPage_HotelNumberofNights = confirmationPage_HotelNumberofNights;
	}
	public ArrayList<String> getHotel_SubTotal() {
		return Hotel_SubTotal;
	}
	public void setHotel_SubTotal(ArrayList<String> hotel_SubTotal) {
		Hotel_SubTotal = hotel_SubTotal;
	}
	public ArrayList<String> getHotel_TotalTaxServices() {
		return Hotel_TotalTaxServices;
	}
	public void setHotel_TotalTaxServices(ArrayList<String> hotel_TotalTaxServices) {
		Hotel_TotalTaxServices = hotel_TotalTaxServices;
	}
	public ArrayList<String> getHotel_Totals() {
		return Hotel_Totals;
	}
	public void setHotel_Totals(ArrayList<String> hotel_Totals) {
		Hotel_Totals = hotel_Totals;
	}
	public ArrayList<String> getAdulttitleConfirmations_List() {
		return adulttitleConfirmations_List;
	}
	public void setAdulttitleConfirmations_List(
			ArrayList<String> adulttitleConfirmations_List) {
		this.adulttitleConfirmations_List = adulttitleConfirmations_List;
	}
	public ArrayList<String> getAdultFNameConfirmations_List() {
		return adultFNameConfirmations_List;
	}
	public void setAdultFNameConfirmations_List(
			ArrayList<String> adultFNameConfirmations_List) {
		this.adultFNameConfirmations_List = adultFNameConfirmations_List;
	}
	public ArrayList<String> getAdultLNameConfirmations_List() {
		return adultLNameConfirmations_List;
	}
	public void setAdultLNameConfirmations_List(
			ArrayList<String> adultLNameConfirmations_List) {
		this.adultLNameConfirmations_List = adultLNameConfirmations_List;
	}
	public ArrayList<String> getChildtitleConfirmations_List() {
		return childtitleConfirmations_List;
	}
	public void setChildtitleConfirmations_List(
			ArrayList<String> childtitleConfirmations_List) {
		this.childtitleConfirmations_List = childtitleConfirmations_List;
	}
	public ArrayList<String> getChildFNameConfirmations_List() {
		return childFNameConfirmations_List;
	}
	public void setChildFNameConfirmations_List(
			ArrayList<String> childFNameConfirmations_List) {
		this.childFNameConfirmations_List = childFNameConfirmations_List;
	}
	public ArrayList<String> getChildLNameConfirmations_List() {
		return childLNameConfirmations_List;
	}
	public void setChildLNameConfirmations_List(
			ArrayList<String> childLNameConfirmations_List) {
		this.childLNameConfirmations_List = childLNameConfirmations_List;
	}
	public ArrayList<String> getAdulttitlePayments_List() {
		return adulttitlePayments_List;
	}
	public void setAdulttitlePayments_List(ArrayList<String> adulttitlePayments_List) {
		this.adulttitlePayments_List = adulttitlePayments_List;
	}
	public ArrayList<String> getAdultFNamePayments_List() {
		return adultFNamePayments_List;
	}
	public void setAdultFNamePayments_List(ArrayList<String> adultFNamePayments_List) {
		this.adultFNamePayments_List = adultFNamePayments_List;
	}
	public ArrayList<String> getAdultLNamePayments_List() {
		return adultLNamePayments_List;
	}
	public void setAdultLNamePayments_List(ArrayList<String> adultLNamePayments_List) {
		this.adultLNamePayments_List = adultLNamePayments_List;
	}
	public ArrayList<String> getChildtitlePayments_List() {
		return childtitlePayments_List;
	}
	public void setChildtitlePayments_List(ArrayList<String> childtitlePayments_List) {
		this.childtitlePayments_List = childtitlePayments_List;
	}
	public ArrayList<String> getChildFNamePayments_List() {
		return childFNamePayments_List;
	}
	public void setChildFNamePayments_List(ArrayList<String> childFNamePayments_List) {
		this.childFNamePayments_List = childFNamePayments_List;
	}
	public ArrayList<String> getChildLNamePayments_List() {
		return childLNamePayments_List;
	}
	public void setChildLNamePayments_List(ArrayList<String> childLNamePayments_List) {
		this.childLNamePayments_List = childLNamePayments_List;
	}
	public int getAllPaxCount() {
		return allPaxCount;
	}
	public void setAllPaxCount(int allPaxCount) {
		this.allPaxCount = allPaxCount;
	}
	public boolean isHotelNameFilter() {
		return isHotelNameFilter;
	}
	public void setHotelNameFilter(boolean isHotelNameFilter) {
		this.isHotelNameFilter = isHotelNameFilter;
	}
	public boolean isResultsHotelStarRating() {
		return isResultsHotelStarRating;
	}
	public void setResultsHotelStarRating(boolean isResultsHotelStarRating) {
		this.isResultsHotelStarRating = isResultsHotelStarRating;
	}
	public boolean isResultsHotelType() {
		return isResultsHotelType;
	}
	public void setResultsHotelType(boolean isResultsHotelType) {
		this.isResultsHotelType = isResultsHotelType;
	}
	public boolean isResultsHotelName() {
		return isResultsHotelName;
	}
	public void setResultsHotelName(boolean isResultsHotelName) {
		this.isResultsHotelName = isResultsHotelName;
	}
	public boolean isResultsHotelPrefCurrency() {
		return isResultsHotelPrefCurrency;
	}
	public void setResultsHotelPrefCurrency(boolean isResultsHotelPrefCurrency) {
		this.isResultsHotelPrefCurrency = isResultsHotelPrefCurrency;
	}
	public boolean isResultsHotelPCode() {
		return isResultsHotelPCode;
	}
	public void setResultsHotelPCode(boolean isResultsHotelPCode) {
		this.isResultsHotelPCode = isResultsHotelPCode;
	}
	public boolean isResultsHotelPRF() {
		return isResultsHotelPRF;
	}
	public void setResultsHotelPRF(boolean isResultsHotelPRF) {
		this.isResultsHotelPRF = isResultsHotelPRF;
	}
	public boolean isResultsHotelPRT() {
		return isResultsHotelPRT;
	}
	public void setResultsHotelPRT(boolean isResultsHotelPRT) {
		this.isResultsHotelPRT = isResultsHotelPRT;
	}
	public int getRezActivity() {
		return rezActivity;
	}
	public void setRezActivity(int rezActivity) {
		this.rezActivity = rezActivity;
	}
	public int getGtaActivity() {
		return gtaActivity;
	}
	public void setGtaActivity(int gtaActivity) {
		this.gtaActivity = gtaActivity;
	}
	public int getHbActivity() {
		return hbActivity;
	}
	public void setHbActivity(int hbActivity) {
		this.hbActivity = hbActivity;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	public boolean isResultsCurrencyListLoaded() {
		return isResultsCurrencyListLoaded;
	}
	public void setResultsCurrencyListLoaded(boolean isResultsCurrencyListLoaded) {
		this.isResultsCurrencyListLoaded = isResultsCurrencyListLoaded;
	}
	public boolean isResultsProgCatListLoaded() {
		return isResultsProgCatListLoaded;
	}
	public void setResultsProgCatListLoaded(boolean isResultsProgCatListLoaded) {
		this.isResultsProgCatListLoaded = isResultsProgCatListLoaded;
	}
	
	public boolean isCurrencyListLoaded() {
		return isCurrencyListLoaded;
	}
	public void setCurrencyListLoaded(boolean isCurrencyListLoaded) {
		this.isCurrencyListLoaded = isCurrencyListLoaded;
	}
	public boolean isAdultsLoaded() {
		return isAdultsLoaded;
	}
	public void setAdultsLoaded(boolean isAdultsLoaded) {
		this.isAdultsLoaded = isAdultsLoaded;
	}
	public boolean isChildLoaded() {
		return isChildLoaded;
	}
	public void setChildLoaded(boolean isChildLoaded) {
		this.isChildLoaded = isChildLoaded;
	}
	public boolean isActivityNameFilter() {
		return isActivityNameFilter;
	}
	public void setActivityNameFilter(boolean isActivityNameFilter) {
		this.isActivityNameFilter = isActivityNameFilter;
	}
	public boolean isPriceRangeFilter() {
		return isPriceRangeFilter;
	}
	public void setPriceRangeFilter(boolean isPriceRangeFilter) {
		this.isPriceRangeFilter = isPriceRangeFilter;
	}
	public boolean isProgramAvailablity() {
		return isProgramAvailablity;
	}
	public void setProgramAvailablity(boolean isProgramAvailablity) {
		this.isProgramAvailablity = isProgramAvailablity;
	}
	public String getShowResultsAdd() {
		return showResultsAdd;
	}
	public void setShowResultsAdd(String showResultsAdd) {
		this.showResultsAdd = showResultsAdd;
	}
	public String getHideResultsAdd() {
		return HideResultsAdd;
	}
	public void setHideResultsAdd(String hideResultsAdd) {
		HideResultsAdd = hideResultsAdd;
	}
	public boolean isResultsProgramCat() {
		return isResultsProgramCat;
	}
	public void setResultsProgramCat(boolean isResultsProgramCat) {
		this.isResultsProgramCat = isResultsProgramCat;
	}
	public boolean isResultsPreCurrency() {
		return isResultsPreCurrency;
	}
	public void setResultsPreCurrency(boolean isResultsPreCurrency) {
		this.isResultsPreCurrency = isResultsPreCurrency;
	}
	public boolean isResultsPromoCode() {
		return isResultsPromoCode;
	}
	public void setResultsPromoCode(boolean isResultsPromoCode) {
		this.isResultsPromoCode = isResultsPromoCode;
	}
	
	
	public boolean isPreCurrency() {
		return isPreCurrency;
	}
	public void setPreCurrency(boolean isPreCurrency) {
		this.isPreCurrency = isPreCurrency;
	}
	public boolean isPromoCode() {
		return isPromoCode;
	}
	public void setPromoCode(boolean isPromoCode) {
		this.isPromoCode = isPromoCode;
	}
	public String getShowAdd() {
		return showAdd;
	}
	public void setShowAdd(String showAdd) {
		this.showAdd = showAdd;
	}
	public String getHideAdd() {
		return HideAdd;
	}
	public void setHideAdd(String hideAdd) {
		HideAdd = hideAdd;
	}
	public boolean isCountryListLoaded() {
		return isCountryListLoaded;
	}
	public void setCountryListLoaded(boolean isCountryListLoaded) {
		this.isCountryListLoaded = isCountryListLoaded;
	}
	public boolean isCountryLoaded() {
		return isCountryLoaded;
	}
	public void setCountryLoaded(boolean isCountryLoaded) {
		this.isCountryLoaded = isCountryLoaded;
	}
	public boolean isCityLoaded() {
		return isCityLoaded;
	}
	public void setCityLoaded(boolean isCityLoaded) {
		this.isCityLoaded = isCityLoaded;
	}
	public boolean isDateFromLoaded() {
		return isDateFromLoaded;
	}
	public void setDateFromLoaded(boolean isDateFromLoaded) {
		this.isDateFromLoaded = isDateFromLoaded;
	}
	public boolean isDateToLoaded() {
		return isDateToLoaded;
	}
	public void setDateToLoaded(boolean isDateToLoaded) {
		this.isDateToLoaded = isDateToLoaded;
	}
	public boolean isCalenderAvailble() {
		return isCalenderAvailble;
	}
	public void setCalenderAvailble(boolean isCalenderAvailble) {
		this.isCalenderAvailble = isCalenderAvailble;
	}
	
	public String getQuote_QuotationNo() {
		return Quote_QuotationNo;
	}
	public void setQuote_QuotationNo(String quote_QuotationNo) {
		Quote_QuotationNo = quote_QuotationNo;
	}
	
	
	public boolean isBookingEngineLoaded() {
		return isBookingEngineLoaded;
	}
	public void setBookingEngineLoaded(boolean isBookingEngineLoaded) {
		this.isBookingEngineLoaded = isBookingEngineLoaded;
	}
	public boolean isAddToCart() {
		return isAddToCart;
	}
	public void setAddToCart(boolean isAddToCart) {
		this.isAddToCart = isAddToCart;
	}
	public boolean isProgramsAvailabilityActivityTypes() {
		return isProgramsAvailabilityActivityTypes;
	}
	public void setProgramsAvailabilityActivityTypes(
			boolean isProgramsAvailabilityActivityTypes) {
		this.isProgramsAvailabilityActivityTypes = isProgramsAvailabilityActivityTypes;
	}
	public boolean isActivityRemoved() {
		return isActivityRemoved;
	}
	public void setActivityRemoved(boolean isActivityRemoved) {
		this.isActivityRemoved = isActivityRemoved;
	}
	public boolean isCancelPolicy() {
		return isCancelPolicy;
	}
	public void setCancelPolicy(boolean isCancelPolicy) {
		this.isCancelPolicy = isCancelPolicy;
	}
	public boolean isMoreInfo() {
		return isMoreInfo;
	}
	public void setMoreInfo(boolean isMoreInfo) {
		this.isMoreInfo = isMoreInfo;
	}
	public ArrayList<Integer> getBeforeSortingDescending() {
		return beforeSortingDescending;
	}
	public void setBeforeSortingDescending(
			ArrayList<Integer> beforeSortingDescending) {
		this.beforeSortingDescending = beforeSortingDescending;
	}
	public ArrayList<Integer> getAfterSortingDescending() {
		return afterSortingDescending;
	}
	public void setAfterSortingDescending(ArrayList<Integer> afterSortingDescending) {
		this.afterSortingDescending = afterSortingDescending;
	}
	public String getPageLoadedResults() {
		return pageLoadedResults;
	}
	public void setPageLoadedResults(String pageLoadedResults) {
		this.pageLoadedResults = pageLoadedResults;
	}
	public String getPrizeChanged_1() {
		return prizeChanged_1;
	}
	public void setPrizeChanged_1(String prizeChanged_1) {
		this.prizeChanged_1 = prizeChanged_1;
	}
	public String getPrizeResults_1() {
		return prizeResults_1;
	}
	public void setPrizeResults_1(String prizeResults_1) {
		this.prizeResults_1 = prizeResults_1;
	}
	public String getPrizeChanged_2() {
		return prizeChanged_2;
	}
	public void setPrizeChanged_2(String prizeChanged_2) {
		this.prizeChanged_2 = prizeChanged_2;
	}
	public String getPrizeResults_2() {
		return prizeResults_2;
	}
	public void setPrizeResults_2(String prizeResults_2) {
		this.prizeResults_2 = prizeResults_2;
	}
	public String getMinPrize() {
		return minPrize;
	}
	public void setMinPrize(String minPrize) {
		this.minPrize = minPrize;
	}
	public String getMaxPrize() {
		return maxPrize;
	}
	public void setMaxPrize(String maxPrize) {
		this.maxPrize = maxPrize;
	}
	public ArrayList<String> getOnRequestLabels() {
		return onRequestLabels;
	}
	public void setOnRequestLabels(ArrayList<String> onRequestLabels) {
		this.onRequestLabels = onRequestLabels;
	}
	public int getResulteBlockSizeOnReq() {
		return resulteBlockSizeOnReq;
	}
	public void setResulteBlockSizeOnReq(int resulteBlockSizeOnReq) {
		this.resulteBlockSizeOnReq = resulteBlockSizeOnReq;
	}
	public ArrayList<Integer> getBeforeSorting() {
		return beforeSorting;
	}
	public void setBeforeSorting(ArrayList<Integer> beforeSorting) {
		this.beforeSorting = beforeSorting;
	}
	public ArrayList<Integer> getAfterSorting() {
		return afterSorting;
	}
	public void setAfterSorting(ArrayList<Integer> afterSorting) {
		this.afterSorting = afterSorting;
	}
	public String getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public String getPaymentPage_PostalCode() {
		return paymentPage_PostalCode;
	}
	public void setPaymentPage_PostalCode(String paymentPage_PostalCode) {
		this.paymentPage_PostalCode = paymentPage_PostalCode;
	}
	public String getConfirmation_postalCode() {
		return confirmation_postalCode;
	}
	public void setConfirmation_postalCode(String confirmation_postalCode) {
		this.confirmation_postalCode = confirmation_postalCode;
	}
	public String getConfirmation_CusMailAddress() {
		return confirmation_CusMailAddress;
	}
	public void setConfirmation_CusMailAddress(String confirmation_CusMailAddress) {
		this.confirmation_CusMailAddress = confirmation_CusMailAddress;
	}
	public String getShoppingCart_CartCurrency() {
		return shoppingCart_CartCurrency;
	}
	public void setShoppingCart_CartCurrency(String shoppingCart_CartCurrency) {
		this.shoppingCart_CartCurrency = shoppingCart_CartCurrency;
	}
	public String getShoppingCart_subTotal() {
		return shoppingCart_subTotal;
	}
	public void setShoppingCart_subTotal(String shoppingCart_subTotal) {
		this.shoppingCart_subTotal = shoppingCart_subTotal;
	}
	public String getShoppingCart_TotalTax() {
		return shoppingCart_TotalTax;
	}
	public void setShoppingCart_TotalTax(String shoppingCart_TotalTax) {
		this.shoppingCart_TotalTax = shoppingCart_TotalTax;
	}
	public String getShoppingCart_TotalValue() {
		return shoppingCart_TotalValue;
	}
	public void setShoppingCart_TotalValue(String shoppingCart_TotalValue) {
		this.shoppingCart_TotalValue = shoppingCart_TotalValue;
	}
	public String getShoppingCart_AmountNow() {
		return shoppingCart_AmountNow;
	}
	public void setShoppingCart_AmountNow(String shoppingCart_AmountNow) {
		this.shoppingCart_AmountNow = shoppingCart_AmountNow;
	}
	public String getShoppingCart_AmountCheckIn() {
		return shoppingCart_AmountCheckIn;
	}
	public void setShoppingCart_AmountCheckIn(String shoppingCart_AmountCheckIn) {
		this.shoppingCart_AmountCheckIn = shoppingCart_AmountCheckIn;
	}
	public String getPaymentBilling_CardCurrency() {
		return paymentBilling_CardCurrency;
	}
	public void setPaymentBilling_CardCurrency(String paymentBilling_CardCurrency) {
		this.paymentBilling_CardCurrency = paymentBilling_CardCurrency;
	}
	public String getPaymentBilling_CardTotal() {
		return paymentBilling_CardTotal;
	}
	public void setPaymentBilling_CardTotal(String paymentBilling_CardTotal) {
		this.paymentBilling_CardTotal = paymentBilling_CardTotal;
	}
	public String getPaymentBilling_subTotal() {
		return paymentBilling_subTotal;
	}
	public void setPaymentBilling_subTotal(String paymentBilling_subTotal) {
		this.paymentBilling_subTotal = paymentBilling_subTotal;
	}
	public String getPaymentBilling_TotalTax() {
		return paymentBilling_TotalTax;
	}
	public void setPaymentBilling_TotalTax(String paymentBilling_TotalTax) {
		this.paymentBilling_TotalTax = paymentBilling_TotalTax;
	}
	public String getPaymentBilling_TotalValue() {
		return paymentBilling_TotalValue;
	}
	public void setPaymentBilling_TotalValue(String paymentBilling_TotalValue) {
		this.paymentBilling_TotalValue = paymentBilling_TotalValue;
	}
	public String getPaymentBilling_AmountNow() {
		return paymentBilling_AmountNow;
	}
	public void setPaymentBilling_AmountNow(String paymentBilling_AmountNow) {
		this.paymentBilling_AmountNow = paymentBilling_AmountNow;
	}
	public String getPaymentBilling_AmountCheckIn() {
		return paymentBilling_AmountCheckIn;
	}
	public void setPaymentBilling_AmountCheckIn(String paymentBilling_AmountCheckIn) {
		this.paymentBilling_AmountCheckIn = paymentBilling_AmountCheckIn;
	}
	public String getRate_NetRate() {
		return rate_NetRate;
	}
	public void setRate_NetRate(String rate_NetRate) {
		this.rate_NetRate = rate_NetRate;
	}
	public String getRate_SellRate() {
		return rate_SellRate;
	}
	public void setRate_SellRate(String rate_SellRate) {
		this.rate_SellRate = rate_SellRate;
	}
	public String getConfirmation_BookingRefference() {
		return confirmation_BookingRefference;
	}
	public void setConfirmation_BookingRefference(
			String confirmation_BookingRefference) {
		this.confirmation_BookingRefference = confirmation_BookingRefference;
	}
	public String getReservationNo() {
		return reservationNo;
	}
	public void setReservationNo(String reservationNo) {
		this.reservationNo = reservationNo;
	}
	public String getConfirmation_BI_ActivityName() {
		return confirmation_BI_ActivityName;
	}
	public void setConfirmation_BI_ActivityName(String confirmation_BI_ActivityName) {
		this.confirmation_BI_ActivityName = confirmation_BI_ActivityName;
	}
	public String getConfirmation_BI_City() {
		return confirmation_BI_City;
	}
	public void setConfirmation_BI_City(String confirmation_BI_City) {
		this.confirmation_BI_City = confirmation_BI_City;
	}
	public String getConfirmation_BI_ActType() {
		return confirmation_BI_ActType;
	}
	public void setConfirmation_BI_ActType(String confirmation_BI_ActType) {
		this.confirmation_BI_ActType = confirmation_BI_ActType;
	}
	public String getConfirmation_BI_BookingStatus() {
		return confirmation_BI_BookingStatus;
	}
	public void setConfirmation_BI_BookingStatus(
			String confirmation_BI_BookingStatus) {
		this.confirmation_BI_BookingStatus = confirmation_BI_BookingStatus;
	}
	public String getConfirmation_BI_SelectedDate() {
		return confirmation_BI_SelectedDate;
	}
	public void setConfirmation_BI_SelectedDate(String confirmation_BI_SelectedDate) {
		this.confirmation_BI_SelectedDate = confirmation_BI_SelectedDate;
	}
	public ArrayList<String> getConfirmation_RateType() {
		return confirmation_RateType;
	}
	public void setConfirmation_RateType(ArrayList<String> confirmation_RateType) {
		this.confirmation_RateType = confirmation_RateType;
	}
	public ArrayList<String> getConfirmation_DailyRate() {
		return confirmation_DailyRate;
	}
	public void setConfirmation_DailyRate(ArrayList<String> confirmation_DailyRate) {
		this.confirmation_DailyRate = confirmation_DailyRate;
	}
	public ArrayList<String> getConfirmation_QTY() {
		return confirmation_QTY;
	}
	public void setConfirmation_QTY(ArrayList<String> confirmation_QTY) {
		this.confirmation_QTY = confirmation_QTY;
	}
	public String getConfirmation_Currency1() {
		return confirmation_Currency1;
	}
	public void setConfirmation_Currency1(String confirmation_Currency1) {
		this.confirmation_Currency1 = confirmation_Currency1;
	}
	public String getConfirmation_Currency2() {
		return confirmation_Currency2;
	}
	public void setConfirmation_Currency2(String confirmation_Currency2) {
		this.confirmation_Currency2 = confirmation_Currency2;
	}
	public String getConfirmation_SubTotal() {
		return confirmation_SubTotal;
	}
	public void setConfirmation_SubTotal(String confirmation_SubTotal) {
		this.confirmation_SubTotal = confirmation_SubTotal;
	}
	public String getConfirmation_Tax() {
		return confirmation_Tax;
	}
	public void setConfirmation_Tax(String confirmation_Tax) {
		this.confirmation_Tax = confirmation_Tax;
	}
	public String getConfirmation_Total() {
		return confirmation_Total;
	}
	public void setConfirmation_Total(String confirmation_Total) {
		this.confirmation_Total = confirmation_Total;
	}
	public String getConfirmation_SubTotal2() {
		return confirmation_SubTotal2;
	}
	public void setConfirmation_SubTotal2(String confirmation_SubTotal2) {
		this.confirmation_SubTotal2 = confirmation_SubTotal2;
	}
	public String getConfirmation_BookingFee() {
		return confirmation_BookingFee;
	}
	public void setConfirmation_BookingFee(String confirmation_BookingFee) {
		this.confirmation_BookingFee = confirmation_BookingFee;
	}
	public String getConfirmation_TotalTaxOtherCharges() {
		return confirmation_TotalTaxOtherCharges;
	}
	public void setConfirmation_TotalTaxOtherCharges(
			String confirmation_TotalTaxOtherCharges) {
		this.confirmation_TotalTaxOtherCharges = confirmation_TotalTaxOtherCharges;
	}
	public String getConfirmation_Total2() {
		return confirmation_Total2;
	}
	public void setConfirmation_Total2(String confirmation_Total2) {
		this.confirmation_Total2 = confirmation_Total2;
	}
	public String getConfirmation_AmountNow() {
		return confirmation_AmountNow;
	}
	public void setConfirmation_AmountNow(String confirmation_AmountNow) {
		this.confirmation_AmountNow = confirmation_AmountNow;
	}
	public String getConfirmation_AmountDueCheckIn() {
		return confirmation_AmountDueCheckIn;
	}
	public void setConfirmation_AmountDueCheckIn(
			String confirmation_AmountDueCheckIn) {
		this.confirmation_AmountDueCheckIn = confirmation_AmountDueCheckIn;
	}
	public String getConfirmation_FName() {
		return confirmation_FName;
	}
	public void setConfirmation_FName(String confirmation_FName) {
		this.confirmation_FName = confirmation_FName;
	}
	public String getConfirmation_LName() {
		return confirmation_LName;
	}
	public void setConfirmation_LName(String confirmation_LName) {
		this.confirmation_LName = confirmation_LName;
	}
	public String getConfirmation_TP() {
		return confirmation_TP;
	}
	public void setConfirmation_TP(String confirmation_TP) {
		this.confirmation_TP = confirmation_TP;
	}
	public String getConfirmation_Email() {
		return confirmation_Email;
	}
	public void setConfirmation_Email(String confirmation_Email) {
		this.confirmation_Email = confirmation_Email;
	}
	public String getConfirmation_address() {
		return confirmation_address;
	}
	public void setConfirmation_address(String confirmation_address) {
		this.confirmation_address = confirmation_address;
	}
	public String getConfirmation_country() {
		return confirmation_country;
	}
	public void setConfirmation_country(String confirmation_country) {
		this.confirmation_country = confirmation_country;
	}
	public String getConfirmation_city() {
		return confirmation_city;
	}
	public void setConfirmation_city(String confirmation_city) {
		this.confirmation_city = confirmation_city;
	}
	public String getConfirmation_State() {
		return confirmation_State;
	}
	public void setConfirmation_State(String confirmation_State) {
		this.confirmation_State = confirmation_State;
	}
	public String getConfirmation_AODActivityName() {
		return confirmation_AODActivityName;
	}
	public void setConfirmation_AODActivityName(String confirmation_AODActivityName) {
		this.confirmation_AODActivityName = confirmation_AODActivityName;
	}
	public ArrayList<String> getConfirmationPage_cusTitle() {
		return confirmationPage_cusTitle;
	}
	public void setConfirmationPage_cusTitle(
			ArrayList<String> confirmationPage_cusTitle) {
		this.confirmationPage_cusTitle = confirmationPage_cusTitle;
	}
	public ArrayList<String> getConfirmationPage_cusFName() {
		return confirmationPage_cusFName;
	}
	public void setConfirmationPage_cusFName(
			ArrayList<String> confirmationPage_cusFName) {
		this.confirmationPage_cusFName = confirmationPage_cusFName;
	}
	public ArrayList<String> getConfirmationPage_cusLName() {
		return confirmationPage_cusLName;
	}
	public void setConfirmationPage_cusLName(
			ArrayList<String> confirmationPage_cusLName) {
		this.confirmationPage_cusLName = confirmationPage_cusLName;
	}
	public String getConfirmation_BillingInfo_TotalValue() {
		return confirmation_BillingInfo_TotalValue;
	}
	public void setConfirmation_BillingInfo_TotalValue(
			String confirmation_BillingInfo_TotalValue) {
		this.confirmation_BillingInfo_TotalValue = confirmation_BillingInfo_TotalValue;
	}
	public String getPaymentGatewayTotal() {
		return paymentGatewayTotal;
	}
	public void setPaymentGatewayTotal(String paymentGatewayTotal) {
		this.paymentGatewayTotal = paymentGatewayTotal;
	}
	public String getPaymentPopUp_ActivityName() {
		return paymentPopUp_ActivityName;
	}
	public void setPaymentPopUp_ActivityName(String paymentPopUp_ActivityName) {
		this.paymentPopUp_ActivityName = paymentPopUp_ActivityName;
	}
	public String getPaymentPopUp_Status() {
		return paymentPopUp_Status;
	}
	public void setPaymentPopUp_Status(String paymentPopUp_Status) {
		this.paymentPopUp_Status = paymentPopUp_Status;
	}
	public String getCustomerNotes() {
		return customerNotes;
	}
	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}
	public String getActivityNotes() {
		return activityNotes;
	}
	public void setActivityNotes(String activityNotes) {
		this.activityNotes = activityNotes;
	}
	public ArrayList<String> getResultsPage_cusTitle() {
		return resultsPage_cusTitle;
	}
	public void setResultsPage_cusTitle(ArrayList<String> resultsPage_cusTitle) {
		this.resultsPage_cusTitle = resultsPage_cusTitle;
	}
	public ArrayList<String> getResultsPage_cusFName() {
		return resultsPage_cusFName;
	}
	public void setResultsPage_cusFName(ArrayList<String> resultsPage_cusFName) {
		this.resultsPage_cusFName = resultsPage_cusFName;
	}
	public ArrayList<String> getResultsPage_cusLName() {
		return resultsPage_cusLName;
	}
	public void setResultsPage_cusLName(ArrayList<String> resultsPage_cusLName) {
		this.resultsPage_cusLName = resultsPage_cusLName;
	}
	public int getActivityCount() {
		return activityCount;
	}
	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
	}
	public String getPaymentPage_Title() {
		return paymentPage_Title;
	}
	public void setPaymentPage_Title(String paymentPage_Title) {
		this.paymentPage_Title = paymentPage_Title;
	}
	public String getPaymentPage_FName() {
		return paymentPage_FName;
	}
	public void setPaymentPage_FName(String paymentPage_FName) {
		this.paymentPage_FName = paymentPage_FName;
	}
	public String getPaymentPage_LName() {
		return paymentPage_LName;
	}
	public void setPaymentPage_LName(String paymentPage_LName) {
		this.paymentPage_LName = paymentPage_LName;
	}
	public String getPaymentPage_TP() {
		return paymentPage_TP;
	}
	public void setPaymentPage_TP(String paymentPage_TP) {
		this.paymentPage_TP = paymentPage_TP;
	}
	public String getPaymentPage_Email() {
		return paymentPage_Email;
	}
	public void setPaymentPage_Email(String paymentPage_Email) {
		this.paymentPage_Email = paymentPage_Email;
	}
	public String getPaymentPage_address() {
		return paymentPage_address;
	}
	public void setPaymentPage_address(String paymentPage_address) {
		this.paymentPage_address = paymentPage_address;
	}
	public String getPaymentPage_country() {
		return paymentPage_country;
	}
	public void setPaymentPage_country(String paymentPage_country) {
		this.paymentPage_country = paymentPage_country;
	}
	public String getPaymentPage_city() {
		return paymentPage_city;
	}
	public void setPaymentPage_city(String paymentPage_city) {
		this.paymentPage_city = paymentPage_city;
	}
	public String getPaymentPage_State() {
		return paymentPage_State;
	}
	public void setPaymentPage_State(String paymentPage_State) {
		this.paymentPage_State = paymentPage_State;
	}
	public String getAddAndContinue() {
		return addAndContinue;
	}
	public void setAddAndContinue(String addAndContinue) {
		this.addAndContinue = addAndContinue;
	}
	public String getCartCurrency() {
		return cartCurrency;
	}
	public void setCartCurrency(String cartCurrency) {
		this.cartCurrency = cartCurrency;
	}
	public String getCartVallue() {
		return cartVallue;
	}
	public void setCartVallue(String cartVallue) {
		this.cartVallue = cartVallue;
	}
	
	public boolean isResultsAvailable() {
		return isResultsAvailable;
	}
	public void setResultsAvailable(boolean isResultsAvailable) {
		this.isResultsAvailable = isResultsAvailable;
	}
	
	public String getActivityCurrency() {
		return activityCurrency;
	}
	public void setActivityCurrency(String activityCurrency) {
		this.activityCurrency = activityCurrency;
	}
	public String getActDescription() {
		return actDescription;
	}
	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}
	public ArrayList<String> getCancelPolicy() {
		return cancelPolicy;
	}
	public void setCancelPolicy(ArrayList<String> cancelPolicy) {
		this.cancelPolicy = cancelPolicy;
	}
	public ArrayList<String> getPaymentCancelPolicy() {
		return paymentCancelPolicy;
	}
	public void setPaymentCancelPolicy(ArrayList<String> paymentCancelPolicy) {
		this.paymentCancelPolicy = paymentCancelPolicy;
	}
	public ArrayList<String> getConfirmCancelPolicy() {
		return confirmCancelPolicy;
	}
	public void setConfirmCancelPolicy(ArrayList<String> confirmCancelPolicy) {
		this.confirmCancelPolicy = confirmCancelPolicy;
	}
	public long getTimeLoadpayment() {
		return timeLoadpayment;
	}
	public void setTimeLoadpayment(long timeLoadpayment) {
		this.timeLoadpayment = timeLoadpayment;
	}
	public long getTimeConfirmBooking() {
		return timeConfirmBooking;
	}
	public void setTimeConfirmBooking(long timeConfirmBooking) {
		this.timeConfirmBooking = timeConfirmBooking;
	}
	public long getTimeSearchButtClickTime() {
		return timeSearchButtClickTime;
	}
	public void setTimeSearchButtClickTime(long timeSearchButtClickTime) {
		this.timeSearchButtClickTime = timeSearchButtClickTime;
	}
	public long getTimeResultsAvailable() {
		return timeResultsAvailable;
	}
	public void setTimeResultsAvailable(long timeResultsAvailable) {
		this.timeResultsAvailable = timeResultsAvailable;
	}
	public long getTimeAddedtoCart() {
		return timeAddedtoCart;
	}
	public void setTimeAddedtoCart(long timeAddedtoCart) {
		this.timeAddedtoCart = timeAddedtoCart;
	}
	public long getTimeAailableClick() {
		return timeAailableClick;
	}
	public void setTimeAailableClick(long timeAailableClick) {
		this.timeAailableClick = timeAailableClick;
	}
	public long getTimeFirstSearch() {
		return timeFirstSearch;
	}
	public void setTimeFirstSearch(long timeFirstSearch) {
		this.timeFirstSearch = timeFirstSearch;
	}
	public long getTimeFirstResults() {
		return timeFirstResults;
	}
	public void setTimeFirstResults(long timeFirstResults) {
		this.timeFirstResults = timeFirstResults;
	}
	
	public int getDaysCount() {
		return daysCount;
	}
	public void setDaysCount(int daysCount) {
		this.daysCount = daysCount;
	}
	
	public String getMainRate() {
		return mainRate;
	}
	public void setMainRate(String mainRate) {
		this.mainRate = mainRate;
	}
	
	public String getCurrentDate() {
		return CurrentDate;
	}
	public void setCurrentDate(String currentDate) {
		CurrentDate = currentDate;
	}
	public ArrayList<String> getResultsPage_ActivityTypes() {
		return resultsPage_ActivityTypes;
	}
	public void setResultsPage_ActivityTypes(
			ArrayList<String> resultsPage_ActivityTypes) {
		this.resultsPage_ActivityTypes = resultsPage_ActivityTypes;
	}
	public ArrayList<String> getResultsPage_City() {
		return resultsPage_City;
	}
	public void setResultsPage_City(ArrayList<String> resultsPage_City) {
		this.resultsPage_City = resultsPage_City;
	}
	public ArrayList<String> getResultsPage_Period() {
		return resultsPage_Period;
	}
	public void setResultsPage_Period(ArrayList<String> resultsPage_Period) {
		this.resultsPage_Period = resultsPage_Period;
	}
	public ArrayList<String> getResultsPage_RateType() {
		return resultsPage_RateType;
	}
	public void setResultsPage_RateType(ArrayList<String> resultsPage_RateType) {
		this.resultsPage_RateType = resultsPage_RateType;
	}
	public ArrayList<String> getResultsPage_QTY() {
		return resultsPage_QTY;
	}
	public void setResultsPage_QTY(ArrayList<String> resultsPage_QTY) {
		this.resultsPage_QTY = resultsPage_QTY;
	}
	public ArrayList<String> getResultsPage_DailyRate() {
		return resultsPage_DailyRate;
	}
	public void setResultsPage_DailyRate(ArrayList<String> resultsPage_DailyRate) {
		this.resultsPage_DailyRate = resultsPage_DailyRate;
	}
	
	
	
	
	
	
}
