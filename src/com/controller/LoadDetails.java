package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.model.*;

public class LoadDetails {
	
	Map<Integer, String> SearchMap 		= null;
	Map<Integer, String> CustomerMap 	= null;

	public LoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		SearchMap          		= sheetlist.get(0);
		CustomerMap          	= sheetlist.get(1);
	}
	
	public TreeMap<String, Search> loadActivityReservation(Map<Integer, String> searchInfoDeMap){
		
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		TreeMap<String, Search> activitySearchInfoMap = new TreeMap<String, Search>();
		
		while(it.hasNext()) {
			
			Search search = new Search();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioCount(values[0]);
			search.setSellingCurrency(values[1]);
			search.setCountry(values[2]);
			search.setDestination(values[3]);
			search.setDateFrom(values[4]);
			search.setDateTo(values[5]);
			search.setRooms(values[6]);			
			search.setAdults(values[7]);
			search.setChildren(values[8]);
			search.setAgeOfChildren(values[9]);
			search.setProgramCategory(values[10]);
			search.setPreferCurrency(values[11]);
			search.setPromotionCode(values[12]);
			search.setActivities(values[13]);
			search.setHotels(values[14]);
			search.setActivityDate(values[15]);
			search.setScenariotype(values[16]);
			search.setBooking_channel(values[17]);
			search.setQuotationReq(values[18]);
			
			activitySearchInfoMap.put(values[0], search);
		}
		
		return activitySearchInfoMap;
	}	
	
	public TreeMap<String, Search> loadPaymentDetails(Map<Integer, String> paymentsMapDe, TreeMap<String, Search> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = paymentsMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			PaymentDetails paymentDetails = new PaymentDetails();
			String[] values = it.next().getValue().split(",");

			paymentDetails.setScenarioId(values[0]);
			paymentDetails.setCustomerTitle(values[1]);
			paymentDetails.setCustomerName(values[2]);
			paymentDetails.setCustomerLastName(values[3]);
			paymentDetails.setTel(values[4]);
			paymentDetails.setEmail(values[5]);
			paymentDetails.setAddress(values[6]);
			paymentDetails.setAddress_1(values[7]);
			paymentDetails.setCountry(values[8]);
			paymentDetails.setCity(values[9]);
			paymentDetails.setState(values[10]);
			paymentDetails.setPostalCode(values[11]);
						
			try {				
				searchInfoDeMap.get(values[0]).addPaymentInfo(paymentDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
	
	
	public TreeMap<String, Search> loadActivityDetails(Map<Integer, String> activityInfoMapDe, TreeMap<String, Search> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = activityInfoMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			ActivityInfo activityInventory = new ActivityInfo();
			String[] values = it.next().getValue().split(",");
			
			activityInventory.setStandardCancell_Type(values[1]);
			activityInventory.setStandardCancell_Value(values[2]);	
			activityInventory.setNoShow_Type(values[3]);
			activityInventory.setNoShow_Value(values[4]);			
			activityInventory.setApplyIfLessThan(values[5]);	
			activityInventory.setCancelBuffer(values[6]);
			activityInventory.setProfir_Markup(values[7]);	
			activityInventory.setActivityNetRate(values[8]);
			
			activityInventory.setSalexTax_Type(values[9]);	
			activityInventory.setSalexTax_Value(values[10]);
			activityInventory.setMisFee_Type(values[11]);	
			activityInventory.setMisFee_Value(values[12]);
			activityInventory.setCC_Fee(values[13]);
			
						
			try {				
				searchInfoDeMap.get(values[0]).addActivityInfo(activityInventory);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
	
	public TreeMap<String, Search> loadHotelDetails(Map<Integer, String> hotelInfoMapDe, TreeMap<String, Search> searchInfoDeMap) {

		Iterator<Map.Entry<Integer, String>> it = hotelInfoMapDe.entrySet().iterator();
		
		while (it.hasNext()) {
			
			HotelInfo hotelInfo = new HotelInfo();
			String[] values = it.next().getValue().split(",");
			
			hotelInfo.setHotel_Name(values[1]);
			hotelInfo.setStandardCancell_Type(values[2]);
			hotelInfo.setStandardCancell_Value(values[3]);
			hotelInfo.setNoShow_Type(values[4]);
			hotelInfo.setNoShow_Value(values[5]);
			hotelInfo.setCancellation_ApplyIfLessThan(values[6]);
			hotelInfo.setCancellation_Buffer(values[7]);
			hotelInfo.setPM(values[8]);
			hotelInfo.setHotelNetRate(values[9]);
			hotelInfo.setHotelBookingFee(values[10]);
			hotelInfo.setCity(values[11]);
			hotelInfo.setAddress(values[12]);
			hotelInfo.setTel(values[13]);
			hotelInfo.setStarCate(values[14]);
			
			hotelInfo.setSalesTax_Type(values[15]);
			hotelInfo.setSalesTax_Value(values[16]);
			hotelInfo.setOccupancy_Type(values[17]);
			hotelInfo.setOccupancy_Value(values[18]);
			hotelInfo.setEnergy_Type(values[19]);
			hotelInfo.setEnergy_Value(values[20]);
			
			
			try {				
				searchInfoDeMap.get(values[0]).addHotelInfo(hotelInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return searchInfoDeMap;
	}
}
