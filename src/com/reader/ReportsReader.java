package com.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.WebDriver;

import bookingconfirmation.BookingConfirmation;
import bookingconfirmation.Quotation;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.ActivityInfo;
import com.model.HotelInfo;
import com.model.Search;

public class ReportsReader {
	
	private WebDriver driver         	= null;
	private ActivityDetails activitydetails;
	private BookingConfirmation confirmationDetails;
	private Search search;
	private StringBuffer  PrintWriter;
	private Map<String, String> currencyMap; 
	private Quotation quoteDetails;
	private int ScenarioCount = 0;  
	private int testCaseCount, testCasePortaiMailCount, testCaseCustomerMail, testCaseModify, payent_qty, testCountCustomer, testCVEActivity ;
	private int testcaseSupplier, testCaseCountReservation, testCaseInventory, testCaseCancel, testCVEHotel1, testCVEHotel2, testQuoteMail;
	private String noShowMatching, activityMainPolicy, activityCanPolicy1, activityCanPolicy2 ;
	private String paymentNoShowMatching, paymentMainPolicy, paymentCanPolicy1, paymentCanPolicy2 ;
	private String confirmNoShowMatching, confirmMainPolicy, confirmCanPolicy1, confirmCanPolicy2 ;
	private String resPolist1 = null;
	private String policyListOne = null ;
	private String policyListTwo  = null;
	private String noShowActual = null;
	private double rateConvertforUSD = 1.0 ;
	private double rateConvertforSearch = 1.0 ;
	private double noShowConvertforCancelPolicy = 1.0 ;
	private double totalPayableforNoshow;
	private int totalCostInUsd, amount_onePerson, totalCost;
	private String bookingTotalvalueForAct;
	private int customerQty, creditTax, pMarkup, PMarkUpHotel, totalPaxCount;
	private String currentDateforMatch, dateWithBufferDates_3, dateWithBufferDates_1, activityDescription;
	private List<String> adultList;	
	private List<String> childList;
	private List<String> hotelList;
	private int excelRoomCount, diffDays, creditCardFee, prbList;
	private int actSubTotal, actTax, actTotalVal;
	private String standardCancell_Type;
	private String standardCancell_Value;
	private String noShow_Type, noShow_Value, applyIfLessThan, cancelBuffer, activityNetrate;
	private List<String> OccuPancyListForMailFromPayment;
	private List<String> OccuPancyListForMailFromPayment_1;
	private String reportDateFrom1 = null;
	private String reportDateTo1 = null;
	private int totalAdultCount = 0;
	private int totalChildCount = 0;
	private String hotel1_policy1, hotel1_policy2, hotel1_policy3, hotel2_policy1, hotel2_policy2, hotel2_policy3, hotel1Name, hotel2Name;
	private List<String> hotelNameFronCancel;
	private List<String> standardCancellation_Type;
	private List<String> standardCancellation_Value;
	private List<String> hotelNoShow_Type;
	private List<String> hotelNoShow_Value;	
	private List<String> hotelApplyIfLessThan;
	private List<String> hotelCancellationBuffer;
	private List<String> hotelPM;
	private List<String> hotelNetRate;
	private List<String> hotelBookingFee;
	private ArrayList<String> hotelSubTotal;
	private ArrayList<String> hotelTotalTax;
	private ArrayList<String> hotelTotalRate;
	private ArrayList<String> hotelName;
	private int totalMybasketValue, subTotalAllProducts, totalTaxAllProducts;
	
	public ReportsReader(ActivityDetails activityDetails, Map<String, String> CurrencyMap, WebDriver Driver, Search searchInfo, BookingConfirmation confirmationD,  Quotation quotationDetails) {
		
		this.activitydetails = activityDetails;
		this.confirmationDetails = confirmationD;
		this.driver = Driver;
		this.search = searchInfo;
		this.currencyMap = CurrencyMap;
		this.quoteDetails = quotationDetails;
	}


	public void getBasicValidationReport(WebDriver Driver) throws ParseException, IOException {
	
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();
		hotelList = new ArrayList<String>();
		OccuPancyListForMailFromPayment = new ArrayList<String>();
		OccuPancyListForMailFromPayment_1= new ArrayList<String>();
		
		hotelSubTotal = new ArrayList<String>();
		hotelTotalTax = new ArrayList<String>();
		hotelTotalRate = new ArrayList<String>();
		hotelName = new ArrayList<String>();
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 

		//double paxCount = Integer.parseInt(search.getAdults()) + Integer.parseInt(search.getChildren());
		
		excelRoomCount = Integer.parseInt(search.getRooms());
		
		if (search.getAdults().contains("#")) {
			String adu = search.getAdults();
			String[] partsAdults = adu.split("#");
			for (String valueAdu : partsAdults) {
				adultList.add(valueAdu);
			}
		} else {
			String adu = search.getAdults();
			adultList.add(adu);
		}
		
		if (search.getChildren().contains("#")) {
			String chi = search.getChildren();
			String[] partschilds = chi.split("#");
			for (String valuechi : partschilds) {
				childList.add(valuechi);
			}
		} else {
			String chi = search.getChildren();
			childList.add(chi);
		}
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		
		if (search.getQuotationReq().equalsIgnoreCase("No")) {
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Hotel+Activity Reservation Report - Reservation No :- ["+activitydetails.getReservationNo()+"] - Date ["+currentDateforMatch+"]</p></div>");			
		}else{
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Hotel+Activity Quotation Report - Reservation No :- ["+activitydetails.getQuote_QuotationNo()+"] - Date ["+currentDateforMatch+"]</p></div>");			
		}
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
		PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
		PrintWriter.append("<p class='InfoSub'>Current [Hotel+Activity] Reservation Criteria</p>");
		PrintWriter.append("<p class='InfoSub'>Portal URL :- "+PG_Properties.getProperty("Baseurl")+"</p>");
		PrintWriter.append("<p class='InfoSub'>Activity Scenario :- "+search.getScenariotype()+"</p>");
		PrintWriter.append("<p class='InfoSub'>Selling Currency :- "+search.getSellingCurrency()+"</p>");
		PrintWriter.append("<p class='InfoSub'>Destination Name :- "+search.getDestination()+"</p>");
		PrintWriter.append("<p class='InfoSub'>Date From :- "+search.getDateFrom()+"</p>");
		PrintWriter.append("<p class='InfoSub'>Date To :- "+search.getDateTo()+"</p>");
		PrintWriter.append("<p class='InfoSub'>Quotation :- "+search.getQuotationReq()+"</p>");
		
		for (int i = 0; i < excelRoomCount; i++) {
			PrintWriter.append("<p class='InfoSub'>Occupancy :- [Room "+(i+1)+"] - No of Adults : "+adultList.get(i)+" , No of Children : "+childList.get(i)+"</p>");
		}

		PrintWriter.append("</div>");
		PrintWriter.append("<br>");
		testCaseCount = 1;
		
		if(!(PG_Properties.getProperty("PortalCurrency").equalsIgnoreCase(PG_Properties.getProperty("SupplierCurrency")))){
			
			for(Entry<String, String> entry: currencyMap.entrySet()) {
				if(PG_Properties.getProperty("SupplierCurrency").equalsIgnoreCase(entry.getKey())){
					
					rateConvertforUSD = Double.parseDouble(entry.getValue());
		
					creditCardFee =  (int) Math.ceil((Double.parseDouble(activitydetails.getCcFee())/ rateConvertforUSD));
					
					for (int i = 0; i < activitydetails.getHotelRatesName().size(); i++) {
						
						hotelName.add(activitydetails.getHotelRatesName().get(i));
						int subTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelSubTotal().get(i)) / rateConvertforUSD ));
						hotelSubTotal.add(Integer.toString(subTotal));
						int tax = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalTax().get(i)) / rateConvertforUSD ));
						hotelTotalTax.add(Integer.toString(tax));
						int total = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalRate().get(i)) / rateConvertforUSD ));
						hotelTotalRate.add(Integer.toString(total));
					}
					
					actSubTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getActSubTotal()) / rateConvertforUSD ));
					actTax = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalTax()) / rateConvertforUSD ));
					actTotalVal = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalRate()) / rateConvertforUSD ));
					
				}
			}
		}
		
		
		
		if(!(PG_Properties.getProperty("PortalCurrency").equalsIgnoreCase(search.getSellingCurrency()))){
			
			for(Entry<String, String> entry: currencyMap.entrySet()) {
				if(search.getSellingCurrency().equalsIgnoreCase(entry.getKey())){
					
					rateConvertforSearch = Double.parseDouble(entry.getValue());			
		
					creditCardFee =  (int) Math.ceil((Double.parseDouble(activitydetails.getCcFee())/ rateConvertforUSD));
					
					for (int i = 0; i < activitydetails.getHotelRatesName().size(); i++) {
						
						hotelName.add(activitydetails.getHotelRatesName().get(i));
						int subTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelSubTotal().get(i)) / rateConvertforUSD ) * rateConvertforSearch );
						hotelSubTotal.add(Integer.toString(subTotal));
						int tax = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalTax().get(i)) / rateConvertforUSD ) * rateConvertforSearch );
						hotelTotalTax.add(Integer.toString(tax));
						int total = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalRate().get(i)) / rateConvertforUSD ) * rateConvertforSearch );
						hotelTotalRate.add(Integer.toString(total));
					}
					
					actSubTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getActSubTotal()) / rateConvertforUSD ) * rateConvertforSearch);
					actTax = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalTax()) / rateConvertforUSD ) * rateConvertforSearch);
					actTotalVal = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalRate()) / rateConvertforUSD ) * rateConvertforSearch);
					
				}
			}
		}
		
				
		if(PG_Properties.getProperty("SupplierCurrency").equalsIgnoreCase(search.getSellingCurrency().toLowerCase())){	
			
			creditCardFee =  (int) Math.ceil((Double.parseDouble(activitydetails.getCcFee())/ rateConvertforUSD));
			
			for (int i = 0; i < activitydetails.getHotelRatesName().size(); i++) {
				
				hotelName.add(activitydetails.getHotelRatesName().get(i));
				int subTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelSubTotal().get(i))));
				hotelSubTotal.add(Integer.toString(subTotal));
				int tax = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalTax().get(i))));
				hotelTotalTax.add(Integer.toString(tax));
				int total = (int) Math.ceil((Double.parseDouble(activitydetails.getHotelTotalRate().get(i)))); 
				hotelTotalRate.add(Integer.toString(total));
			}
			
			actSubTotal = (int) Math.ceil((Double.parseDouble(activitydetails.getActSubTotal())));
			actTax = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalTax())));
			actTotalVal = (int) Math.ceil((Double.parseDouble(activitydetails.getActTotalRate())));
		}
		
		
		
		
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expect Values - [Excel Value]</th><th>Actual Values</th><th>Test Status</th></tr>");
		
		try {
			
			if (activitydetails.isResultsAvailable() == true) {
				
				////Booking Engine
				
				PrintWriter.append("<tr><td class='fontiiii'>Booking Engine</td><tr>"); 
				
							
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine</td>");
				PrintWriter.append("<td>Booking Engine Should be Available</td>");
				
				if (activitydetails.isBookingEngineLoaded() == true) {
					
					PrintWriter.append("<td>Booking Engine Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Booking Engine Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country Of Residence</td>");
				PrintWriter.append("<td>Country Of Residence Should be Available</td>");
				
				if (activitydetails.isCountryLoaded() == true) {
					
					PrintWriter.append("<td>Country Of Residence Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country Of Residence Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Destination</td>");
				PrintWriter.append("<td>Destination Should be Available</td>");
				
				if (activitydetails.isCityLoaded() == true) {
					
					PrintWriter.append("<td>Destination Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Destination Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Departure Date</td>");
				PrintWriter.append("<td>Departure Date Should be Available</td>");
				
				if (activitydetails.isDateFromLoaded() == true) {
					
					PrintWriter.append("<td>Departure Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Departure Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Arrival Date</td>");
				PrintWriter.append("<td>Arrival Date Should be Available</td>");
				
				if (activitydetails.isDateToLoaded() == true) {
					
					PrintWriter.append("<td>Arrival Date Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Arrival Date Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Adults Drop Down</td>");
				PrintWriter.append("<td>Adults Drop Down Should be Available</td>");
				
				if (activitydetails.isAdultsLoaded() == true) {
					
					PrintWriter.append("<td>Adults Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Adults Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Childs Drop Down</td>");
				PrintWriter.append("<td>Childs Drop Down Should be Available</td>");
				
				if (activitydetails.isChildLoaded() == true) {
					
					PrintWriter.append("<td>Childs Drop Down Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Childs Drop Down Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Country List loading</td>");
				PrintWriter.append("<td>Country List Should be Available</td>");
				
				if (activitydetails.isCountryListLoaded() == true) {
					
					PrintWriter.append("<td>Country List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Country List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Calender loading</td>");
				PrintWriter.append("<td>Calender Should be Available</td>");
				
				if (activitydetails.isCalenderAvailble() == true) {
					
					PrintWriter.append("<td>Calender Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Calender Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Type loading</td>");
				PrintWriter.append("<td>Hotel Type Should be Available</td>");
				
				if (activitydetails.isHotelType() == true) {
					
					PrintWriter.append("<td>Hotel Type Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Type Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Type List loading</td>");
				PrintWriter.append("<td>Hotel Type List Should be Available</td>");
				
				if (activitydetails.isHotelTypeListLoaded() == true) {
					
					PrintWriter.append("<td>Hotel Type List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Type List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Currency List loading</td>");
				PrintWriter.append("<td>Hotel Currency List Should be Available</td>");
				
				if (activitydetails.isCurrencyListLoaded() == true) {
					
					PrintWriter.append("<td>Hotel Currency List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Currency List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Star Rating List loading</td>");
				PrintWriter.append("<td>Hotel Star Rating List Should be Available</td>");
				
				if (activitydetails.isStarRating() == true) {
					
					PrintWriter.append("<td>Hotel Star Rating List Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Star Rating List Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Name loading</td>");
				PrintWriter.append("<td>Hotel Name Should be Available</td>");
				
				if (activitydetails.isHotelName() == true) {
					
					PrintWriter.append("<td>Hotel Name Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Name Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel PromoCode loading</td>");
				PrintWriter.append("<td>Hotel PromoCode Should be Available</td>");
				
				if (activitydetails.isPromoCode() == true) {
					
					PrintWriter.append("<td>Hotel PromoCode Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel PromoCode Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				

				/////  Results Page
				
				PrintWriter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
						
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Price range filter loading</td>");
				PrintWriter.append("<td>Price range filter Should be Available</td>");
				
				if (activitydetails.isPriceRangeFilter() == true) {
					
					PrintWriter.append("<td>Price range filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Price range filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Availability loading</td>");
				PrintWriter.append("<td>Hotel Availability Should be Available</td>");
				
				if (activitydetails.isProgramAvailablity() == true) {
					
					PrintWriter.append("<td>Hotel Availability Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Availability Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Name Filter loading</td>");
				PrintWriter.append("<td>Hotel Name Filter Should be Available</td>");
				
				if (activitydetails.isHotelNameFilter() == true) {
					
					PrintWriter.append("<td>Hotel Name Filter Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Name Filter Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Show Additional Filter Options link</td>");
				PrintWriter.append("<td>Show Additional Filter Options link Should be Available</td>");
				
				if (activitydetails.getShowResultsAdd().replaceAll(" ", "").equalsIgnoreCase("Showadditionalfilters")) {
					
					PrintWriter.append("<td>Show Additional Filter Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Show Additional Filter Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Type loading</td>");
				PrintWriter.append("<td>Hotel Type Should be Available</td>");
				
				if (activitydetails.isResultsHotelType() == true) {
					
					PrintWriter.append("<td>Hotel Type Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Type Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Name loading</td>");
				PrintWriter.append("<td>Hotel Name Should be Available</td>");
				
				if (activitydetails.isResultsHotelName() == true) {
					
					PrintWriter.append("<td>Hotel Name Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Name Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Star Category loading</td>");
				PrintWriter.append("<td>Hotel Star Category Should be Available</td>");
				
				if (activitydetails.isResultsHotelStarRating() == true) {
					
					PrintWriter.append("<td>Hotel Star Category Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Star Category Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Preferred Currency loading</td>");
				PrintWriter.append("<td>Preferred Currency Should be Available</td>");
				
				if (activitydetails.isResultsHotelPrefCurrency() == true) {
					
					PrintWriter.append("<td>Preferred Currency Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Preferred Currency Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Promotion Code loading</td>");
				PrintWriter.append("<td>Promotion Code Should be Available</td>");
				
				if (activitydetails.isResultsHotelPCode() == true) {
					
					PrintWriter.append("<td>Promotion Code Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Promotion Code Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Currency List loading</td>");
				PrintWriter.append("<td>Currency List loading Should be Available</td>");
				
				if (activitydetails.isResultsHotelPrefCurrency() == true) {
					
					PrintWriter.append("<td>Currency List loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Currency List loading Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Price Range From loading</td>");
				PrintWriter.append("<td>Hotel Price Range From Should be Available</td>");
				
				if (activitydetails.isResultsHotelPrefCurrency() == true) {
					
					PrintWriter.append("<td>Hotel Price Range From loading Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Price Range From Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel Price Range To loading</td>");
				PrintWriter.append("<td>Hotel Price Range To Should be Available</td>");
				
				if (activitydetails.isResultsHotelPrefCurrency() == true) {
					
					PrintWriter.append("<td>Hotel Price Range To Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hotel Price Range To Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hide Additional Filter Options link</td>");
				PrintWriter.append("<td>Hide Additional Filter Options link Should be Available</td>");
				
				if (activitydetails.getHideResultsAdd().replaceAll(" ", "").equalsIgnoreCase("Hideadditionalfilters")) {
					
					PrintWriter.append("<td>Hide Additional Filter Options link Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Hide Additional Filter Options link Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				
				//////////////////
				
					
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel results availability</td>");
				PrintWriter.append("<td>Results Should be available</td>");
							
				String resultsAvailable = Boolean.toString(activitydetails.isResultsAvailable());
				
				if(resultsAvailable.equals("true")){
						
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+resultsAvailable+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity results availability</td>");
				PrintWriter.append("<td>Results Should be available</td>");
							
				String activityResultsAvailable = Boolean.toString(activitydetails.isActivityResultsAvailability());
				
				if(activityResultsAvailable.equals("true")){
						
					PrintWriter.append("<td>"+activityResultsAvailable+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activityResultsAvailable+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
								
				String dateFromToCal = search.getDateFrom();
				String dateToCal = search.getDateTo();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				
				Date d1 = null;
				Date d2 = null;
				
		 
				try {
					d1 = format.parse(dateFromToCal);
					d2 = format.parse(dateToCal);
		 
					//in milliseconds
					long diffD = d2.getTime() - d1.getTime();
					diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
							
					testCaseCount ++;	
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity days count</td>");
					PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays+1)+"</td>");
							
					if(activitydetails.getDaysCount() == (diffDays+1)){
							
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>Results page days count - "+activitydetails.getDaysCount()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}

		 
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				/*testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Add to Cart</td>");
				PrintWriter.append("<td>Should be add to the cart</td>");
				
				if (activitydetails.isAddToCart() == true) {
					
					PrintWriter.append("<td>Added</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>No activity in the cart</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Remove Activity From Cart</td>");
				PrintWriter.append("<td>Should be remove</td>");
				
				if (activitydetails.isActivityRemoved() == true) {
					
					PrintWriter.append("<td>Removed</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>No activity in the cart</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				int prizeChanged_1 = Integer.parseInt(activitydetails.getPrizeChanged_1());
				int PrizeResults_1 = Integer.parseInt(activitydetails.getPrizeResults_1());
				int prizeChanged_2 = Integer.parseInt(activitydetails.getPrizeChanged_2());
				int PrizeResults_2 = Integer.parseInt(activitydetails.getPrizeResults_2());
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 1</td>");
				PrintWriter.append("<td>Min Prize is - "+search.getSellingCurrency()+" "+activitydetails.getMinPrize()+"</td>");
				
				if (activitydetails.getMinPrize().equals(activitydetails.getPageLoadedResults())) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPageLoadedResults()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 2</td>");
				PrintWriter.append("<td>Min Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_1()+"</td>");
				
				if (PrizeResults_1 >= prizeChanged_1) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_1()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Price Range Filter - Test 3</td>");
				PrintWriter.append("<td>Max Prize set to - "+search.getSellingCurrency()+" "+activitydetails.getPrizeChanged_2()+"</td>");
				
				if (prizeChanged_2 == PrizeResults_2) {
					
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Results Element 1 - "+search.getSellingCurrency()+" "+activitydetails.getPrizeResults_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				*/
				
				
				
				////
				
				
				/*for (int i = 0; i < activitydetails.getResulteBlockSizeOnReq(); i++) {
					
					if (activitydetails.getOnRequestLabels().get(i).contains("Not")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>0 - On Request programs available</td>");
						PrintWriter.append("<td>Activity Program Availability - On Request</td>");
						PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
						
						break;
						
						
					}else{
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+activitydetails.getResulteBlockSizeOnReq()+" - On Request programs available</td>");
						PrintWriter.append("<td>Activity Program Availability - "+(i+1)+" - On Request</td>");
						
						if (activitydetails.getOnRequestLabels().get(i).replaceAll(" ", "").equalsIgnoreCase("on request".replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
							
						} else {
							PrintWriter.append("<td>"+activitydetails.getOnRequestLabels().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
						}
					}
					
							
				}
				
				
				
				
					
				///
				boolean equalsStringDescend = activitydetails.getBeforeSortingDescending().equals(activitydetails.getAfterSortingDescending());
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results display order - (Highest to Lowest) [Descending]</td>");
				PrintWriter.append("<td>"+activitydetails.getAfterSortingDescending()+"</td>");
							
				try {
					if(equalsStringDescend == true){
							
						PrintWriter.append("<td>"+activitydetails.getBeforeSortingDescending()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getBeforeSortingDescending()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				} catch (Exception e1) {
					PrintWriter.append("<td>"+e1.getMessage()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				boolean equalsString = activitydetails.getBeforeSorting().equals(activitydetails.getAfterSorting());
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results display order - (Lowest to Highest) [Ascending]</td>");
				PrintWriter.append("<td>"+activitydetails.getAfterSorting()+"</td>");
							
				try {
					if(equalsString == true){
							
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getBeforeSorting()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				} catch (Exception e1) {
					PrintWriter.append("<td>"+e1.getMessage()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}*/
				
				
				///
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Results Page </td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getActivityCurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActivityCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				/*activityDescription = search.get
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity More info Description - Results Page</td>");
				PrintWriter.append("<td>"+activityDescription+"</td>");
				
				if(activitydetails.getActDescription().replaceAll(" ", "").toLowerCase().contains(activityDescription.replaceAll(" ", "").toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+activitydetails.getActDescription()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				*/
					
				
				
				for (int y = 0; y < Integer.parseInt(search.getRooms()); y++) {
					
					totalAdultCount = totalAdultCount + Integer.parseInt(adultList.get(y));
					totalChildCount = totalChildCount + Integer.parseInt(childList.get(y));																						
				}
				
				String paxCountActivity = ""+totalAdultCount+" Adult(s)  "+totalChildCount+" Children" ;
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - 1 Pax and Activity Pax counts</td>");
				PrintWriter.append("<td>Should be Equal</td>");
				
				if (activitydetails.getActivityPagePax().replaceAll(" ", "").equalsIgnoreCase(paxCountActivity.replaceAll(" ", "")) ) {
					
					PrintWriter.append("<td>Equal -> "+activitydetails.getActivityPagePax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Equal -> "+activitydetails.getActivityPagePax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
				totalPaxCount = totalAdultCount + totalChildCount;
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity More Info</td>");
				PrintWriter.append("<td>Should be Loaded</td>");
				
				if (activitydetails.isMoreInfo() == true) {
					
					PrintWriter.append("<td>Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Cancellation policy</td>");
				PrintWriter.append("<td>Should be Loaded</td>");
				
				if (activitydetails.isCancelPolicy() == true) {
					
					PrintWriter.append("<td>Loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
				
				String paxCountAHotel2 = ""+totalAdultCount+" Adult(s)  "+totalChildCount+" Children" ;
				
				testCaseCount ++;			
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Pax and Hotel - 2 Pax counts</td>");
				PrintWriter.append("<td>Should be Equal</td>");
				
				if (activitydetails.getHotel2PagePax().replaceAll(" ", "").contains(paxCountAHotel2.replaceAll(" ", "")) ) {
					
					PrintWriter.append("<td>Equal -> "+activitydetails.getHotel2PagePax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
					
				} else {
					PrintWriter.append("<td>Not Equal -> "+activitydetails.getHotel2PagePax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");		
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
					
				/////  Payment Page
				
				PrintWriter.append("<tr><td class='fontiiii'>Payments Page</td><tr>"); 
				
				PrintWriter.append("<tr><td class='fontiiii'>Payments Page - Shopping Basket</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Selling Currency - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(activitydetails.getShoppingCart_CartCurrency().toLowerCase())){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_CartCurrency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_CartCurrency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				totalMybasketValue = 0;
				
				for (int j = 0; j < hotelName.size(); j++) {
					
					totalMybasketValue = totalMybasketValue + Integer.parseInt(hotelTotalRate.get(j));
					
				}
				
				totalMybasketValue = totalMybasketValue + actTotalVal +  creditCardFee;
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>My Basket total value - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+totalMybasketValue+"</td>");
							
				if(activitydetails.getShoppingCart_TotalBasketValue().contains(Integer.toString(totalMybasketValue))){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalBasketValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalBasketValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				subTotalAllProducts = 0;
				
				for (int j = 0; j < hotelName.size(); j++) {
					
					subTotalAllProducts = subTotalAllProducts + Integer.parseInt(hotelSubTotal.get(j));
					
				}
				
				subTotalAllProducts = subTotalAllProducts + actSubTotal;
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
							
				if(activitydetails.getShoppingCart_subTotal().contains(Integer.toString(subTotalAllProducts))){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_subTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_subTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				totalTaxAllProducts = 0;
				
				for (int j = 0; j < hotelName.size(); j++) {
					
					totalTaxAllProducts = totalTaxAllProducts + Integer.parseInt(hotelTotalTax.get(j));
					
				}
				
				totalTaxAllProducts = totalTaxAllProducts + actTax + creditCardFee;
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
							
				if(activitydetails.getShoppingCart_TotalTax().contains(Integer.toString(totalTaxAllProducts))){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+totalMybasketValue+"</td>");
							
				if(activitydetails.getShoppingCart_TotalValue().contains(Integer.toString(totalMybasketValue))){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_TotalValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Shopping Basket</td>");
				PrintWriter.append("<td>"+totalMybasketValue+"</td>");
							
				if(activitydetails.getShoppingCart_AmountNow().contains(Integer.toString(totalMybasketValue))){
						
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+activitydetails.getShoppingCart_AmountNow()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				
				if (search.getQuotationReq().equalsIgnoreCase("Yes")) {
					
					////Quotation Confirmation Page
						
					PrintWriter.append("<tr><td class='fontiiii'>Quotation Confirmation Page</td><tr>"); 	
					
					String refernceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Package Booking Reference - Quotation Confirmation Page </td>");
					PrintWriter.append("<td>"+refernceName+"</td>");
					
					if(refernceName.equalsIgnoreCase(activitydetails.getQuote_confirmation_BookingRefference().replaceAll(" ", ""))){
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Email Address - Quotation Confirmation Page </td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
					
					if(activitydetails.getPaymentPage_Email().equalsIgnoreCase(activitydetails.getQuote_confirmation_CusMailAddress())){
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
										
					if (search.getHotels().contains("#")) {
						String hotel = search.getHotels();
						String[] partsHotel = hotel.split("#");
						for (String valueAdu : partsHotel) {
							hotelList.add(valueAdu);
						}
					} else {
						String hotel = search.getHotels();
						hotelList.add(hotel);
					}
					
										
					for (int i = 0; i < hotelList.size(); i++) {
						
						
						PrintWriter.append("<tr><td class='fontyG'>Hotel : "+(i+1)+"</td><tr>"); 
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>Hotel -> "+hotelList.get(i)+"</td>");
						
						if(hotelList.get(i).replaceAll(" ", "").equalsIgnoreCase(activitydetails.getQuote_confirmationPage_HotelName().get(i).replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelName().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelName().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Booking Status - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>Quote</td>");
						
						if(activitydetails.getQuote_confirmationPage_HotelBookingStatus().get(i).equalsIgnoreCase("Quote")){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelBookingStatus().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelBookingStatus().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						String dateFromDate = search.getDateFrom();
						DateFormat dfTo = new SimpleDateFormat("dd/MMM/yyyy");
						SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
						Date instanceFrom = formatInTo.parse(dateFromDate);  		
						reportDateFrom1 = dfTo.format(instanceFrom).replaceAll("/", " ");
						
					
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" CheckIn Date - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+reportDateFrom1+"</td>");
						
						if(activitydetails.getQuote_confirmationPage_HotelCheckInDate().get(i).replaceAll(" ", "").equalsIgnoreCase(reportDateFrom1.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelCheckInDate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelCheckInDate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						String dateToDate = search.getDateTo();
						Date instanceTo = formatInTo.parse(dateToDate);  		
						reportDateTo1 = dfTo.format(instanceTo).replaceAll("/", " ");
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" CheckOut Date - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+reportDateTo1+"</td>");
						
						if(activitydetails.getQuote_confirmationPage_HotelCheckOutDate().get(i).replaceAll(" ", "").equalsIgnoreCase(reportDateTo1.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelCheckOutDate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelCheckOutDate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Number of Room(s) - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+search.getRooms()+"</td>");
						
						if(search.getRooms().equalsIgnoreCase(activitydetails.getQuote_confirmationPage_HotelNumberofRooms().get(i))){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelNumberofRooms().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelNumberofRooms().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Number of Night(s) - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+diffDays+"</td>");
						
						if(Integer.toString(diffDays).equalsIgnoreCase(activitydetails.getQuote_confirmationPage_HotelNumberofNights().get(i))){
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelNumberofNights().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmationPage_HotelNumberofNights().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					
						//Rates
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Sub Total</td>");
						PrintWriter.append("<td>"+hotelSubTotal.get(i)+"</td>");
									
						if(activitydetails.getQuote_Hotel_SubTotal().get(i).contains(hotelSubTotal.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_SubTotal().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_SubTotal().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Total Tax & Service Charges</td>");
						PrintWriter.append("<td>"+hotelTotalTax.get(i)+"</td>");
									
						if(activitydetails.getQuote_Hotel_TotalTaxServices().get(i).contains(hotelTotalTax.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_TotalTaxServices().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_TotalTaxServices().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Total (Hotels)</td>");
						PrintWriter.append("<td>"+hotelTotalRate.get(i)+"</td>");
									
						if(activitydetails.getQuote_Hotel_Totals().get(i).contains(hotelTotalRate.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_Totals().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getQuote_Hotel_Totals().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
			
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
								
					if(search.getActivities().split(" - ")[0].replaceAll(" ", "").equalsIgnoreCase(activitydetails.getQuote_confirmation_BI_ActivityName().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>Quote</td>");
					
					if(activitydetails.getQuote_confirmation_BI_BookingStatus().equalsIgnoreCase("Quote")){
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_BookingStatus()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_BookingStatus()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Program City - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getDestination()+"</td>");
								
					if(search.getDestination().equalsIgnoreCase(activitydetails.getQuote_confirmation_BI_City())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Usable Date - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
								
					if(search.getActivityDate().equalsIgnoreCase(activitydetails.getQuote_confirmation_BI_SelectedDate())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					
					for (int i = 0; i < activitydetails.getResultsPage_RateType().size(); i++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate type - "+(i+1)+" - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_RateType().get(i).equalsIgnoreCase(activitydetails.getQuote_confirmation_RateType().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate - "+(i+1)+" - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_DailyRate().get(i).equalsIgnoreCase(activitydetails.getQuote_confirmation_DailyRate().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity QTY - "+(i+1)+" - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
									
						if(Integer.toString(totalPaxCount).equals(activitydetails.getQuote_confirmation_QTY().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-1 - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getQuote_confirmation_Currency1())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-2 - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getQuote_confirmation_Currency2())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
				
					//
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Activity Rate - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+actSubTotal+"</td>");
								
					if(activitydetails.getQuote_confirmation_ActivityRate().contains(Integer.toString(actSubTotal))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_ActivityRate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_ActivityRate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Sub Total - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+actSubTotal+"</td>");
								
					if(activitydetails.getQuote_confirmation_SubTotal().contains(Integer.toString(actSubTotal))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_SubTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_SubTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity  >> Total Tax & Service Charges - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+actTax+"</td>");
								
					if(activitydetails.getQuote_confirmation_Tax().contains(Integer.toString(actTax))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Tax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Tax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Total - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+actTotalVal+"</td>");
								
					if(activitydetails.getQuote_confirmation_Total().contains(Integer.toString(actTotalVal))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Total()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Total()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
								
					if(activitydetails.getQuote_confirmation_SubTotal2().contains(Integer.toString(subTotalAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
								
					if(activitydetails.getQuote_confirmation_TotalTaxOtherCharges().contains(Integer.toString(totalTaxAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getQuote_confirmation_Total2().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
								
					
					
					///// Customer details
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [First Name] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(activitydetails.getQuote_confirmation_FName())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Last Name] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(activitydetails.getQuote_confirmation_LName())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [TP No] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
								
					if(activitydetails.getPaymentPage_TP().equals(activitydetails.getQuote_confirmation_TP())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_TP()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_TP()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Email] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
								
					if(activitydetails.getPaymentPage_Email().equals(activitydetails.getQuote_confirmation_Email())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Email()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_Email()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Address] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
								
					if(activitydetails.getPaymentPage_address().equals(activitydetails.getQuote_confirmation_address())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Country] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
								
					if(activitydetails.getPaymentPage_country().equals(activitydetails.getQuote_confirmation_country())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [City] - Quotation Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
								
					if(activitydetails.getPaymentPage_city().equals(activitydetails.getQuote_confirmation_city())){
							
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getPaymentPage_country().equals("USA") || activitydetails.getPaymentPage_country().equals("Canada") || activitydetails.getPaymentPage_country().equals("Australia")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getQuote_confirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Quotation Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getQuote_confirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getQuote_confirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					
					
					
					
					PrintWriter.append("</table>");	
					
					//Quotation Mail
					
					getQuotationMial(driver);
					
					
					
				} else {
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(activitydetails.getPaymentBilling_CardCurrency().replaceAll(" ", "").contains(search.getSellingCurrency())){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardCurrency()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Package Booking Value - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getPaymentBilling_CardTotal().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_CardTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
								
					if(activitydetails.getPaymentBilling_subTotal().contains(Integer.toString(subTotalAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_subTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
								
					if(activitydetails.getPaymentBilling_TotalTax().contains(Integer.toString(totalTaxAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalTax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getPaymentBilling_TotalValue().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_TotalValue()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Payment Page Billing Info</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getPaymentBilling_AmountNow().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentBilling_AmountNow()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Value - Payment Gateway</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getPaymentGatewayTotal().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getPaymentGatewayTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
				
					//// Confirmation Page
					
					PrintWriter.append("<tr><td class='fontiiii'>Confirmation Page</td><tr>"); 	
					
					String refernceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Package Booking Reference - Confirmation Page </td>");
					PrintWriter.append("<td>"+refernceName+"</td>");
					
					if(refernceName.equalsIgnoreCase(activitydetails.getConfirmation_BookingRefference().replaceAll(" ", ""))){
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BookingRefference()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Email Address - Confirmation Page </td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
					
					if(activitydetails.getPaymentPage_Email().equalsIgnoreCase(activitydetails.getConfirmation_CusMailAddress())){
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_CusMailAddress()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					
					
					if (search.getHotels().contains("#")) {
						String hotel = search.getHotels();
						String[] partsHotel = hotel.split("#");
						for (String valueAdu : partsHotel) {
							hotelList.add(valueAdu);
						}
					} else {
						String hotel = search.getHotels();
						hotelList.add(hotel);
					}
					
					
					
					
					for (int i = 0; i < hotelList.size(); i++) {
						
						
						PrintWriter.append("<tr><td class='fontyG'>Hotel : "+(i+1)+"</td><tr>"); 
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>Hotel -> "+hotelList.get(i)+"</td>");
						
						if(hotelList.get(i).replaceAll(" ", "").equalsIgnoreCase(activitydetails.getConfirmationPage_HotelName().get(i).replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelName().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelName().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Booking Status - Confirmation Page</td>");
						PrintWriter.append("<td>Confirmed</td>");
						
						if(activitydetails.getConfirmationPage_HotelBookingStatus().get(i).equalsIgnoreCase("Confirmed")){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelBookingStatus().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelBookingStatus().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						String dateFromDate = search.getDateFrom();
						DateFormat dfTo = new SimpleDateFormat("dd/MMM/yyyy");
						SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
						Date instanceFrom = formatInTo.parse(dateFromDate);  		
						reportDateFrom1 = dfTo.format(instanceFrom).replaceAll("/", " ");
						
					
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" CheckIn Date - Confirmation Page</td>");
						PrintWriter.append("<td>"+reportDateFrom1+"</td>");
						
						if(activitydetails.getConfirmationPage_HotelCheckInDate().get(i).replaceAll(" ", "").equalsIgnoreCase(reportDateFrom1.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelCheckInDate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelCheckInDate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						String dateToDate = search.getDateTo();
						Date instanceTo = formatInTo.parse(dateToDate);  		
						reportDateTo1 = dfTo.format(instanceTo).replaceAll("/", " ");
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" CheckOut Date - Confirmation Page</td>");
						PrintWriter.append("<td>"+reportDateTo1+"</td>");
						
						if(activitydetails.getConfirmationPage_HotelCheckOutDate().get(i).replaceAll(" ", "").equalsIgnoreCase(reportDateTo1.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelCheckOutDate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelCheckOutDate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Number of Room(s) - Confirmation Page</td>");
						PrintWriter.append("<td>"+search.getRooms()+"</td>");
						
						if(search.getRooms().equalsIgnoreCase(activitydetails.getConfirmationPage_HotelNumberofRooms().get(i))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelNumberofRooms().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelNumberofRooms().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Hotel - "+(i+1)+" Number of Night(s) - Confirmation Page</td>");
						PrintWriter.append("<td>"+diffDays+"</td>");
						
						if(Integer.toString(diffDays).equalsIgnoreCase(activitydetails.getConfirmationPage_HotelNumberofNights().get(i))){
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelNumberofNights().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmationPage_HotelNumberofNights().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
					
						//Rates
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Sub Total</td>");
						PrintWriter.append("<td>"+hotelSubTotal.get(i)+"</td>");
									
						if(activitydetails.getHotel_SubTotal().get(i).contains(hotelSubTotal.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getHotel_SubTotal().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getHotel_SubTotal().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Total Tax & Service Charges</td>");
						PrintWriter.append("<td>"+hotelTotalTax.get(i)+"</td>");
									
						if(activitydetails.getHotel_TotalTaxServices().get(i).contains(hotelTotalTax.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getHotel_TotalTaxServices().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getHotel_TotalTaxServices().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotelList.get(i)+" >> Total (Hotels)</td>");
						PrintWriter.append("<td>"+hotelTotalRate.get(i)+"</td>");
									
						if(activitydetails.getHotel_Totals().get(i).contains(hotelTotalRate.get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getHotel_Totals().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {				
							PrintWriter.append("<td>"+activitydetails.getHotel_Totals().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
			
					}
					
					
					//Activity Details
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Name - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
								
					if(search.getActivities().split(" - ")[0].replaceAll(" ", "").equalsIgnoreCase(activitydetails.getConfirmation_BI_ActivityName().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_ActivityName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Booking Status - Confirmation Page</td>");
					PrintWriter.append("<td>Confirmed</td>");
					
					if(activitydetails.getConfirmation_BI_BookingStatus().equalsIgnoreCase("Confirmed")){
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Program City - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getDestination()+"</td>");
								
					if(search.getDestination().equalsIgnoreCase(activitydetails.getConfirmation_BI_City())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Usable Date - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
								
					if(search.getActivityDate().equalsIgnoreCase(activitydetails.getConfirmation_BI_SelectedDate())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_SelectedDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					//////////
					
					for (int i = 0; i < activitydetails.getResultsPage_RateType().size(); i++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate type - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_RateType().get(i).equalsIgnoreCase(activitydetails.getConfirmation_RateType().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_RateType().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Rate - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(i)+"</td>");
									
						if(activitydetails.getResultsPage_DailyRate().get(i).equalsIgnoreCase(activitydetails.getConfirmation_DailyRate().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_DailyRate().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity QTY - "+(i+1)+" - Confirmation Page</td>");
						PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
									
						if(Integer.toString(totalPaxCount).equals(activitydetails.getConfirmation_QTY().get(i))){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+activitydetails.getConfirmation_QTY().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-1 - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getConfirmation_Currency1())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency1()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Currency-2 - Confirmation Page</td>");
					PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
								
					if(search.getSellingCurrency().equalsIgnoreCase(activitydetails.getConfirmation_Currency2())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {
						
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Currency2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
				
					//
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Activity Rate - Confirmation Page</td>");
					PrintWriter.append("<td>"+actSubTotal+"</td>");
								
					if(activitydetails.getConfirmation_ActivityRate().contains(Integer.toString(actSubTotal))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_ActivityRate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_ActivityRate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Sub Total - Confirmation Page</td>");
					PrintWriter.append("<td>"+actSubTotal+"</td>");
								
					if(activitydetails.getConfirmation_SubTotal().contains(Integer.toString(actSubTotal))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity  >> Total Tax & Service Charges - Confirmation Page</td>");
					PrintWriter.append("<td>"+actTax+"</td>");
								
					if(activitydetails.getConfirmation_Tax().contains(Integer.toString(actTax))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Tax()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Tax()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity >> Total - Confirmation Page</td>");
					PrintWriter.append("<td>"+actTotalVal+"</td>");
								
					if(activitydetails.getConfirmation_Total().contains(Integer.toString(actTotalVal))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Sub Total - - Confirmation Page</td>");
					PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
								
					if(activitydetails.getConfirmation_SubTotal2().contains(Integer.toString(subTotalAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_SubTotal2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total Taxes and Other Charges - - Confirmation Page</td>");
					PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
								
					if(activitydetails.getConfirmation_TotalTaxOtherCharges().contains(Integer.toString(totalTaxAllProducts))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TotalTaxOtherCharges()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Total - Confirmation Page</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getConfirmation_Total2().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Total2()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Amount being Processed Now - Confirmation Page</td>");
					PrintWriter.append("<td>"+totalMybasketValue+"</td>");
								
					if(activitydetails.getConfirmation_AmountNow().contains(Integer.toString(totalMybasketValue))){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_AmountNow()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else {				
						PrintWriter.append("<td>"+activitydetails.getConfirmation_AmountNow()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
					}
					
					
					
					
					
					///// Customer details
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [First Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
								
					if(activitydetails.getPaymentPage_FName().equals(activitydetails.getConfirmation_FName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_FName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Last Name] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
								
					if(activitydetails.getPaymentPage_LName().equals(activitydetails.getConfirmation_LName())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_LName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [TP No] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
								
					if(activitydetails.getPaymentPage_TP().equals(activitydetails.getConfirmation_TP())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_TP()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Email] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
								
					if(activitydetails.getPaymentPage_Email().equals(activitydetails.getConfirmation_Email())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_Email()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Address] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
								
					if(activitydetails.getPaymentPage_address().equals(activitydetails.getConfirmation_address())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Country] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
								
					if(activitydetails.getPaymentPage_country().equals(activitydetails.getConfirmation_country())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;			
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [City] - Confirmation Page</td>");
					PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
								
					if(activitydetails.getPaymentPage_city().equals(activitydetails.getConfirmation_city())){
							
						PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+activitydetails.getConfirmation_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					if (activitydetails.getPaymentPage_country().equals("USA")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					if (activitydetails.getPaymentPage_country().equals("Canada")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					if (activitydetails.getPaymentPage_country().equals("Australia")) {
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [State] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_State()+"</td>");
									
						if(activitydetails.getPaymentPage_State().equals(activitydetails.getConfirmation_State())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_State()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer details [Postal Code] - Confirmation Page</td>");
						PrintWriter.append("<td>"+activitydetails.getPaymentPage_PostalCode()+"</td>");
									
						if(activitydetails.getPaymentPage_PostalCode().equals(activitydetails.getConfirmation_postalCode())){
								
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+activitydetails.getConfirmation_postalCode()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
					}
					
					
					
					//Activity Occupancy Details
					
					
					for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
					
						String paymentPageCusDetials = activitydetails.getResultsPage_cusTitle().get(i) + activitydetails.getResultsPage_cusFName().get(i) + activitydetails.getResultsPage_cusLName().get(i);
						String ConfirmationPageCusDetials = activitydetails.getConfirmationPage_cusTitle().get(i) + activitydetails.getConfirmationPage_cusFName().get(i) + activitydetails.getConfirmationPage_cusLName().get(i);
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Customer Occupancy details [Activity] - Customer["+(i+1)+"] - Confirmation Page</td>");
						PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");
									
						if(paymentPageCusDetials.equalsIgnoreCase(ConfirmationPageCusDetials)){
								
							PrintWriter.append("<td>"+ConfirmationPageCusDetials+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+ConfirmationPageCusDetials+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}			
					}
					
					
					ArrayList<String> namesFromConfirmationpage = new ArrayList<String>();
					
					for (int i = 0; i < activitydetails.getAdulttitleConfirmations_List().size(); i++) {
						
						String ConfirmationPageCusDetials = activitydetails.getAdulttitleConfirmations_List().get(i) + activitydetails.getAdultFNameConfirmations_List().get(i) + activitydetails.getAdultLNameConfirmations_List().get(i);
						namesFromConfirmationpage.add(ConfirmationPageCusDetials);
					}
					
				
					for (int i = 0; i < activitydetails.getAdulttitlePayments_List().size(); i++) {
						
						String paymentPageCusDetials = activitydetails.getAdulttitlePayments_List().get(i) + activitydetails.getAdultFNamePayments_List().get(i) + activitydetails.getAdultLNamePayments_List().get(i);
						OccuPancyListForMailFromPayment.add(paymentPageCusDetials);
						String paymentPageCusDetials_1 = activitydetails.getAdultLNamePayments_List().get(i) + activitydetails.getAdultFNamePayments_List().get(i);
						OccuPancyListForMailFromPayment_1.add(paymentPageCusDetials_1);
						
						
						innerloop : for (int j = 0; j < namesFromConfirmationpage.size(); j++) {
																	
							if(paymentPageCusDetials.contains(namesFromConfirmationpage.get(j))){
									
								testCaseCount ++;			
								PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Adult Occupancy details [Hotel] - Customer["+(i+1)+"] - Confirmation Page</td>");
								PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");						
								PrintWriter.append("<td>"+namesFromConfirmationpage.get(j)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								break innerloop;
							}
						}
					}
					
					
					for (int i = 0; i < activitydetails.getChildtitlePayments_List().size(); i++) {
						
						String paymentsChildDetails = activitydetails.getChildtitlePayments_List().get(i) + activitydetails.getChildFNamePayments_List().get(i) + activitydetails.getChildLNamePayments_List().get(i);
						String ConfirmationPageChildDetials = activitydetails.getChildtitleConfirmations_List().get(i) + activitydetails.getChildFNameConfirmations_List().get(i) + activitydetails.getChildLNameConfirmations_List().get(i);
						String paymentsChildDetails_1 = activitydetails.getChildLNamePayments_List().get(i) + activitydetails.getChildFNamePayments_List().get(i);
						
						OccuPancyListForMailFromPayment.add(paymentsChildDetails);
						OccuPancyListForMailFromPayment_1.add(paymentsChildDetails_1);
						
						testCaseCount ++;			
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Child Occupancy details - Customer["+(i+1)+"] - Confirmation Page</td>");
						PrintWriter.append("<td>"+paymentsChildDetails+"</td>");
									
						if(paymentsChildDetails.equalsIgnoreCase(ConfirmationPageChildDetials)){
								
							PrintWriter.append("<td>"+ConfirmationPageChildDetials+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+ConfirmationPageChildDetials+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
					}
					
					
					ArrayList<ActivityInfo> actInfoList = search.getActivityInfoDetails();
					
					for (ActivityInfo activityInfo : actInfoList) {
						
						pMarkup = Integer.parseInt(activityInfo.getProfir_Markup());
						standardCancell_Type = activityInfo.getStandardCancell_Type();
						standardCancell_Value = activityInfo.getStandardCancell_Value();
						noShow_Type = activityInfo.getNoShow_Type();
						noShow_Value = activityInfo.getNoShow_Value();
						applyIfLessThan = activityInfo.getApplyIfLessThan();
						cancelBuffer = activityInfo.getCancelBuffer();
						activityNetrate = activityInfo.getActivityNetRate();
					}
					
					//Activity Cancellation Policies
					
					int bufferDates = Integer.parseInt(applyIfLessThan) + Integer.parseInt(cancelBuffer);
					
					String DateTo = search.getDateTo();
					String activitySelectDate = search.getActivityDate();
					DateFormat dfTo = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
					Date instanceTo = formatInTo.parse(DateTo);  		
					String reportDateTo = dfTo.format(instanceTo);  //DateTo   
					
					String DateFrom = search.getDateFrom();
					Date instance = formatInTo.parse(DateFrom);  		
					String reportDateFrom = dfTo.format(instance);  //Datefrom
					
					Date instanceMainPol = dfTo.parse(activitySelectDate); 
					Calendar calMainPolicy = Calendar.getInstance();
					calMainPolicy.setTime(instanceMainPol);
					calMainPolicy.add(Calendar.DATE, -1);			
					String dateWithBufferDates_1 = dfTo.format(calMainPolicy.getTime());  // DateTo - 1
					
					Date instanceMainPolicy3 = dfTo.parse(currentDateforMatch);
					Calendar calMainPolicyto = Calendar.getInstance();
					calMainPolicyto.setTime(instanceMainPolicy3);
					calMainPolicyto.add(Calendar.DATE, +1);
					String currentDateforMatch_1 = dfTo.format(calMainPolicyto.getTime());   //Datefrom - 1
					
					Date instanceMainPolicy5 = dfTo.parse(activitySelectDate);
					Calendar policy1 = Calendar.getInstance();
					policy1.setTime(instanceMainPolicy5);
					policy1.add(Calendar.DATE, -(bufferDates+1));
					String dateWithBufferDates_3 = dfTo.format(policy1.getTime());   //Datefrom - [Buffer]
					
					Date instanceMainPolicy6 = dfTo.parse(dateWithBufferDates_3);
					Calendar policy12 = Calendar.getInstance();
					policy12.setTime(instanceMainPolicy6);
					policy12.add(Calendar.DATE, +1);
					String dateWithBufferDates_66 = dfTo.format(policy12.getTime());
					
					Date instanceMainPolicy4 = dfTo.parse(activitySelectDate);
					Calendar calActivityDate = Calendar.getInstance();
					calActivityDate.setTime(instanceMainPolicy4);
					calActivityDate.add(Calendar.DATE, -1);
					String dateWithSelected = dfTo.format(calActivityDate.getTime()); //Activity - 1
					
					String StandardCancellation_based = standardCancell_Type;
					String Noshow_based = noShow_Type;				
					double StandardCancellation_value = 0;				
					double Noshow_value = 0 ;
					
					if (standardCancell_Type.equalsIgnoreCase("Percentage")) {
						StandardCancellation_value = (Double.parseDouble(standardCancell_Value));
					}
					
					if (standardCancell_Type.equalsIgnoreCase("value")) {
						StandardCancellation_value = Math.ceil(Double.parseDouble(standardCancell_Value) + (Double.parseDouble(standardCancell_Value) * ((double)pMarkup/100)));
					}
					
					if (noShow_Type.equalsIgnoreCase("Percentage")) {
						Noshow_value = (Double.parseDouble(noShow_Value));
					}
					
					if (noShow_Type.equalsIgnoreCase("value")) {
						Noshow_value = Math.ceil(Double.parseDouble(noShow_Value) + (Double.parseDouble(noShow_Value) * ((double)pMarkup/100)));
					}
					
					////  Cancel policy 1
					
					PrintWriter.append("<tr><td class='fontiiii'>Activity Cancellation Policies</td><tr>"); 
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityCanPolicy1 = activitydetails.getCancelPolicy().get(0);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+StandardCancellation_value+" % of the purchase price.";						
							}
							
								
						} else {
		
							if (!(activityCanPolicy1.split(" ")[4].equals(currentDateforMatch))) {
								
								resPolist1 = "If you cancel between "+currentDateforMatch_1+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
								
							}else {
								
								resPolist1 = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_1+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							}
							
						}
											
										
					}
					
					////
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						
						if (activitydetails.getCancelPolicy().get(0).contains(",")) {
							
							activityMainPolicy = activitydetails.getCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							activityMainPolicy = activitydetails.getCancelPolicy().get(0);
						}
						
						
						policyListOne = "If you cancel between "+currentDateforMatch+" and "+dateWithBufferDates_3+" you will be refunded your purchase price.";
						
	
						
						
						if (activitydetails.getCancelPolicy().get(1).contains(",")) {
							
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							activityCanPolicy2 = activitydetails.getCancelPolicy().get(1);
						}
						
						
						if (StandardCancellation_based.contains("percentage")) {
							
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+StandardCancellation_value+" % .";	
						
						} else {
		
							policyListTwo = "If you cancel between "+dateWithBufferDates_66+" and "+dateWithSelected+" you will be charged "+search.getSellingCurrency()+" "+StandardCancellation_value+" .";
							
						}
									
	
						
					}
					
					
					//No Show Applying policy
					
					if (activitydetails.getCancelPolicy().size() == 2) {
						noShowMatching = activitydetails.getCancelPolicy().get(1);
					}
					
					if (activitydetails.getCancelPolicy().size() == 3) {
						noShowMatching = activitydetails.getCancelPolicy().get(2);
					}
					
					if (activitydetails.getCancelPolicy().size() == 4) {
						noShowMatching 	= activitydetails.getCancelPolicy().get(3);
					}
					
												
					double noShowValue = totalPayableforNoshow * (Double.parseDouble(noShow_Value) / 100);
					double roundUpValueNoShow = Math.ceil(noShowValue);
					
					if (Noshow_based.contains("percentage")) {
						
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+roundUpValueNoShow+" applies.";
						
					} else {
		
						noShowActual	= "If cancelled on or after the "+search.getActivityDate()+" No Show Fee "+search.getSellingCurrency()+" "+Noshow_value+" applies.";
						
					}
					
					
				////////
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
					
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentCanPolicy1 = activitydetails.getPaymentCancelPolicy().get(0);
						}
							
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(paymentCanPolicy1.toLowerCase().replaceAll(" ", "").equals(resPolist1.toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						
						if (activitydetails.getPaymentCancelPolicy().get(0).contains(",")) {
							
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							paymentMainPolicy = activitydetails.getPaymentCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(paymentMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getPaymentCancelPolicy().get(1).contains(",")) {
							
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							paymentCanPolicy2 = activitydetails.getPaymentCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Payment Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(paymentCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+paymentCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getPaymentCancelPolicy().size() == 2) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(1);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 3) {
						paymentNoShowMatching = activitydetails.getPaymentCancelPolicy().get(2);
					}
					
					if (activitydetails.getPaymentCancelPolicy().size() == 4) {
						paymentNoShowMatching 	= activitydetails.getPaymentCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Payment Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(paymentNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+paymentNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
				
				///////
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
					
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmCanPolicy1 = activitydetails.getConfirmCancelPolicy().get(0);
						}
											
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+resPolist1+"</td>");
						
						if(confirmCanPolicy1.toLowerCase().replaceAll(" ", "").equals(resPolist1.toLowerCase().replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy1+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}				
										
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						
						if (activitydetails.getConfirmCancelPolicy().get(0).contains(",")) {
							
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0).replace(",", "");
						}
						
						else {
							confirmMainPolicy = activitydetails.getConfirmCancelPolicy().get(0);
						}
						
						
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Refundable policy [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListOne+"</td>");
						
						if(policyListOne.toLowerCase().equals(confirmMainPolicy.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmMainPolicy+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
										
						
						
						if (activitydetails.getConfirmCancelPolicy().get(1).contains(",")) {
							
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1).replace(",", "");
						}
						
						else {
							confirmCanPolicy2 = activitydetails.getConfirmCancelPolicy().get(1);
						}
						
									
						testCaseCount++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity cancellation policy - 1 [Confirmation Page]</td>");
						PrintWriter.append("<td>"+policyListTwo+"</td>");
						
						if(policyListTwo.toLowerCase().equals(confirmCanPolicy2.toLowerCase())){
							
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmCanPolicy2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}		
						
					}
					
					
					if (activitydetails.getConfirmCancelPolicy().size() == 2) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(1);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 3) {
						confirmNoShowMatching = activitydetails.getConfirmCancelPolicy().get(2);
					}
					
					if (activitydetails.getConfirmCancelPolicy().size() == 4) {
						confirmNoShowMatching 	= activitydetails.getConfirmCancelPolicy().get(3);
					}
					
					testCaseCount++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity No-Show Fee policy [Confirmation Page]</td>");
					PrintWriter.append("<td>"+noShowActual+"</td>");
					
					if(confirmNoShowMatching.toLowerCase().equals(noShowActual.toLowerCase())){
						
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmNoShowMatching+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					//Hotel Cancellation Policies
					
					hotelNameFronCancel				= new ArrayList<String>();
					standardCancellation_Type 		= new ArrayList<String>();
					standardCancellation_Value 		= new ArrayList<String>();
					hotelNoShow_Type 				= new ArrayList<String>();
					hotelNoShow_Value 				= new ArrayList<String>();	
					hotelApplyIfLessThan 			= new ArrayList<String>();
					hotelCancellationBuffer			= new ArrayList<String>();
					hotelPM 						= new ArrayList<String>();
					hotelNetRate 					= new ArrayList<String>();
					hotelBookingFee					= new ArrayList<String>();
					
					ArrayList<HotelInfo> hotelInfoList = search.getHotelInfoDetails();
					
					for (HotelInfo hotelInfo : hotelInfoList) {
						
						hotelNameFronCancel.add(hotelInfo.getHotel_Name());
						standardCancellation_Type.add(hotelInfo.getStandardCancell_Type());
						standardCancellation_Value.add(hotelInfo.getStandardCancell_Value());
						hotelNoShow_Type.add(hotelInfo.getNoShow_Type());
						hotelNoShow_Value.add(hotelInfo.getNoShow_Value());
						hotelApplyIfLessThan.add(hotelInfo.getCancellation_ApplyIfLessThan());
						hotelCancellationBuffer.add(hotelInfo.getCancellation_Buffer());
						hotelPM.add(hotelInfo.getPM());
						hotelNetRate.add(hotelInfo.getHotelNetRate());
						hotelBookingFee.add(hotelInfo.getHotelBookingFee());
							
					}
					
					
					for (int i = 0; i < hotelList.size(); i++) {
					
						innerloop : for (int j = 0; j < hotelNameFronCancel.size(); j++) {
							
							if (hotelList.get(i).replaceAll(" ", "").equalsIgnoreCase(hotelNameFronCancel.get(j).replaceAll(" ", ""))) {
								
								// Policy Dates
								
								int hotelBufferDates = Integer.parseInt(hotelApplyIfLessThan.get(j)) + Integer.parseInt(hotelCancellationBuffer.get(j));
								
								DateFormat dfToHotel = new SimpleDateFormat("dd-MMM-yyyy");
								Date instance_date2 = dfToHotel.parse(reportDateFrom);
								Calendar policy_date2 = Calendar.getInstance();
								policy_date2.setTime(instance_date2);
								policy_date2.add(Calendar.DATE, -(hotelBufferDates+1));
								String date_2 = dfToHotel.format(policy_date2.getTime());   //Date2
								
								//reportDateFrom - date 5
								//currentDateforMatch - date 1
								
								Date instance_date3 = dfToHotel.parse(date_2);
								Calendar policy_date3 = Calendar.getInstance();
								policy_date3.setTime(instance_date3);
								policy_date3.add(Calendar.DATE, +1);
								String date_3 = dfToHotel.format(policy_date3.getTime());
								
								Date instance_date4 = dfToHotel.parse(reportDateFrom);
								Calendar policy_date4 = Calendar.getInstance();
								policy_date4.setTime(instance_date4);
								policy_date4.add(Calendar.DATE, -1);
								String date_4 = dfToHotel.format(policy_date4.getTime());
								DecimalFormat df1 = new DecimalFormat("#.00"); 
								
								
								String HotelStandardCancellation_based = standardCancellation_Type.get(j);
								String HotelNoshow_based = hotelNoShow_Type.get(j);				
								String HStandardCancellation_value = null;				
								String HNoshow_value = null;
								
								if (HotelStandardCancellation_based.equalsIgnoreCase("Percentage")) {
									HStandardCancellation_value = Double.toString((Double.parseDouble(standardCancellation_Value.get(j))));
								}
								
								if (HotelStandardCancellation_based.equalsIgnoreCase("value")) {
									int pMarkupHo = Integer.parseInt(hotelPM.get(j));
									int bookingFee = Integer.parseInt(hotelBookingFee.get(j));
									HStandardCancellation_value = df1.format(Math.ceil(bookingFee + Double.parseDouble(standardCancellation_Value.get(j)) + (Double.parseDouble(standardCancellation_Value.get(j)) * ((double)pMarkupHo/100))));
								}
								
								if (HotelStandardCancellation_based.equalsIgnoreCase("nights")) {
									HStandardCancellation_value = standardCancellation_Value.get(j);
								}
								
								
								if (HotelNoshow_based.equalsIgnoreCase("Percentage")) {
									HNoshow_value = Double.toString((Double.parseDouble(hotelNoShow_Value.get(j))));
								}
								
								if (HotelNoshow_based.equalsIgnoreCase("value")) {
									int pMarkupHo = Integer.parseInt(hotelPM.get(j));
									int bookingFee = Integer.parseInt(hotelBookingFee.get(j));
									HNoshow_value = df1.format(Math.ceil(bookingFee + Double.parseDouble(hotelNoShow_Value.get(j)) + (Double.parseDouble(hotelNoShow_Value.get(j)) * ((double)pMarkupHo/100))));
								}
								
								if (HotelNoshow_based.equalsIgnoreCase("nights")) {
									int night = Integer.parseInt(hotelNoShow_Value.get(j));
									int pMarkupHo = Integer.parseInt(hotelPM.get(j));
									int bookingFee = Integer.parseInt(hotelBookingFee.get(j));
									int rooms = Integer.parseInt(search.getRooms());
									HNoshow_value = df1.format(Math.ceil(bookingFee + (rooms * night * (Double.parseDouble(hotelNetRate.get(j)) + (Double.parseDouble(hotelNetRate.get(j)) * ((double)pMarkupHo/100))))));
								}
								
								if(j == 0){
									
									hotel1Name = hotelNameFronCancel.get(j);
									hotel1_policy1 = "If you cancel between "+currentDateforMatch+" and "+date_2+" you will be refunded your purchase price.";
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("Percentage")) {
										hotel1_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+HStandardCancellation_value+"% of the purchase price.";
									}
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("value")) {
										hotel1_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+search.getSellingCurrency()+" "+HStandardCancellation_value+" of the room and tax charge.";
									}
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("nights")) {
										hotel1_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+HStandardCancellation_value+" nights room and tax charge";
									}
									
									hotel1_policy3 = "If cancelled on or after the "+reportDateFrom+" No Show Fee "+search.getSellingCurrency()+" "+HNoshow_value+" applies.";
									
									
								}
								
								if(j == 1){
									
									hotel2Name = hotelNameFronCancel.get(j);
									hotel2_policy1 = "If you cancel between "+currentDateforMatch+" and "+date_2+" you will be refunded your purchase price.";
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("Percentage")) {
										hotel2_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+HStandardCancellation_value+"% of the purchase price.";
									}
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("value")) {
										hotel2_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+search.getSellingCurrency()+" "+HStandardCancellation_value+" of the room and tax charge.";
									}
									
									if (HotelStandardCancellation_based.equalsIgnoreCase("nights")) {
										hotel2_policy2 = "If you cancel between "+date_3+" and "+date_4+" you will be charged "+HStandardCancellation_value+" nights room and tax charge";
									}
									
									hotel2_policy3 = "If cancelled on or after the "+reportDateFrom+" No Show Fee "+search.getSellingCurrency()+" "+HNoshow_value+" applies.";
									
									
								}
								
								break innerloop;
								
						
							}						
						}					
					}
					
					PrintWriter.append("<tr><td class='fontiiii'>Hotel Cancellation Policies</td><tr>"); 
					
					for (int i = 0; i < activitydetails.getHotel1_PaymentsPagePolicy().size() ; i++) {
						
						if (i == 0) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" Refundable policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy1+"</td>");
							
							if(hotel1_policy1.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (i == 1) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" second policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy2+"</td>");
							
							if(hotel1_policy2.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (i == 2) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" No-Show policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy3+"</td>");
							
							if(hotel1_policy3.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
									
					}
					
					
					for (int i = 0; i < activitydetails.getHotel1_ConfirmationPagePolicy().size() ; i++) {
						
						if (i == 0) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" Refundable policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy1+"</td>");
							
							if(hotel1_policy1.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (i == 1) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" second policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy2+"</td>");
							
							if(hotel1_policy2.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (i == 2) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel1Name+" No-Show policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel1_policy3+"</td>");
							
							if(hotel1_policy3.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel1_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel1_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
									
					}
					
					
					
					for (int i = 0; i < activitydetails.getHotel2_PaymentsPagePolicy().size() ; i++) {
						
						if (i == 0) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" Refundable policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy1+"</td>");
							
							if(hotel2_policy1.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (i == 1) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" second policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy2+"</td>");
							
							if(hotel2_policy2.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (i == 2) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" No-Show policy [payments Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy3+"</td>");
							
							if(hotel2_policy3.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_PaymentsPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_PaymentsPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
									
					}
					
					
					for (int i = 0; i < activitydetails.getHotel2_ConfirmationPagePolicy().size() ; i++) {
						
						if (i == 0) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" Refundable policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy1+"</td>");
							
							if(hotel2_policy1.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						}
						
						if (i == 1) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" second policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy2+"</td>");
							
							if(hotel2_policy2.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
						
						if (i == 2) {
							
							testCaseCount++;
							PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>"+hotel2Name+" No-Show policy [Confirmation Page]</td>");
							PrintWriter.append("<td>"+hotel2_policy3+"</td>");
							
							if(hotel2_policy3.replaceAll(" ", "").equalsIgnoreCase(activitydetails.getHotel2_ConfirmationPagePolicy().get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+activitydetails.getHotel2_ConfirmationPagePolicy().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
									
					}
				
				
				

					PrintWriter.append("</table>");	
					
					//Mails					
					getCustomerMailsValidations(driver);
					
					
					
				}
				
				
				
				
			}else{
								
				
				PrintWriter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Activity Results Availability</td>");
				PrintWriter.append("<td>Results Should be available</td>");
				PrintWriter.append("<td>Results are not available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
				
		
			}
			
			
			
			PrintWriter.append("</body></html>");
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Report/ReservationReport_"+search.getScenarioCount()+".html")));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	
	}
			
		
	public void getCustomerMailsValidations(WebDriver Driver){
		
		//Customer Confirmation Email
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Confirmation Email</p>");
		
		testCaseCustomerMail = 1;
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCustomerConfirmationMailLoaded() == true) {
			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCCE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
						
			for (int i = 0; i < hotelList.size(); i++) {
				
				for (int j = 0; j < confirmationDetails.getCCE_HotelsHeading().size(); j++) {
					
					
					if (confirmationDetails.getCCE_HotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))) {
																
						testCaseCustomerMail++;
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Hotel "+(i+1)+" - Name</td>");
						PrintWriter.append("<td>"+hotelList.get(i)+"</td>");
						
						if(confirmationDetails.getCCE_HotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getCCE_HotelsHeading().get(j).split(": ")[1]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getCCE_HotelsHeading().get(j).split(": ")[1]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String cceMailHeadingCreate = ""+diffDays+" Night(s) "+search.getRooms()+" Room(s) Stay at:  "+hotelList.get(i)+"";						
						String originalWeb = confirmationDetails.getCCE_HotelsHeading().get(j).split(",")[0] + confirmationDetails.getCCE_HotelsHeading().get(j).split(",")[1];
						
						testCaseCustomerMail++;
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Hotel "+(i+1)+" - Heading</td>");
						PrintWriter.append("<td>"+cceMailHeadingCreate+"</td>");
						
						if(cceMailHeadingCreate.replaceAll(" ", "").equalsIgnoreCase(originalWeb.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+originalWeb+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+originalWeb+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
						testCaseCustomerMail++;
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Hotel "+(i+1)+" - Check In</td>");
						PrintWriter.append("<td>"+reportDateFrom1+"</td>");
						
						if(reportDateFrom1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_CheckIn().get(j).replaceAll("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getCCE_CheckIn().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getCCE_CheckIn().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCustomerMail++;
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Hotel "+(i+1)+" - Check Out</td>");
						PrintWriter.append("<td>"+reportDateTo1+"</td>");
						
						if(reportDateTo1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_CheckOut().get(j).replaceAll("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getCCE_CheckOut().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getCCE_CheckOut().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCustomerMail ++;
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Hotel "+(i+1)+" - Booking Status</td>");
						PrintWriter.append("<td>Confirmed</td>");
						
						if(confirmationDetails.getCCE_HotelBookingStatus().get(j).equalsIgnoreCase("Confirmed")){
							
							PrintWriter.append("<td>"+confirmationDetails.getCCE_HotelBookingStatus().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getCCE_HotelBookingStatus().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
					
					}		
				}		
			}
			
			
			
			//Activity Details
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Acitivity Name</td>");
			PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
			
			if(search.getActivities().split(" - ")[0].replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_ActivityType())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_ActivityType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity - Booking Status</td>");
			PrintWriter.append("<td>Confirmed</td>");
			
			if(confirmationDetails.getCCE_BookingStatus().equalsIgnoreCase("Confirmed")){
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_BookingStatus()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
					
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Period /Session</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_Period().get(0)+"</td>");
						
			if(activitydetails.getResultsPage_Period().get(0).equalsIgnoreCase(confirmationDetails.getCCE_Period())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Period()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate Type</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(0)+"</td>");
						
			if(confirmationDetails.getCCE_RateType().toLowerCase().contains(activitydetails.getResultsPage_RateType().get(0).toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_RateType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			/*testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(0)+"</td>");
						
			if(confirmationDetails.getCCE_Rate().equals(activitydetails.getResultsPage_DailyRate().get(0))){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}*/
			
		
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity QTY</td>");
			PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
						
			if(Integer.toString(totalPaxCount).equals(confirmationDetails.getCCE_QTY())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_QTY()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Currency 1</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getCCE_TotalValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - First Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
						
			if(activitydetails.getPaymentPage_FName().equals(confirmationDetails.getCCE_FName())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_FName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Last Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
			if(activitydetails.getPaymentPage_LName().equals(confirmationDetails.getCCE_LName())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_LName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - TP No</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(confirmationDetails.getCCE_TP())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_TP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(confirmationDetails.getCCE_Email().contains(activitydetails.getPaymentPage_Email())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(confirmationDetails.getCCE_address())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_address()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Country</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(confirmationDetails.getCCE_country())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_country()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - City</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(confirmationDetails.getCCE_city())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_city()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			////
			
			//Activity Occupancy
			
			for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Activity Customer  - [Customer "+(i+1)+"] Title - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusTitle().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusTitle().get(i).equals(confirmationDetails.getCCE_CusTitle().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Activity Counts - [Customer "+(i+1)+"] FName- Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusFName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusFName().get(i).equals(confirmationDetails.getCCE_CusFName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCaseCustomerMail++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Activity Counts - [Customer "+(i+1)+"] LName - Customer Confirmation Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusLName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusLName().get(i).equals(confirmationDetails.getCCE_CusLName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCCE_CusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			///
			
			for (int j = 0; j < confirmationDetails.getCCE_title_Occupancy().size(); j++) {
			
				String paymentPageCusDetials = confirmationDetails.getCCE_title_Occupancy().get(j) + confirmationDetails.getCCE_firstName_Occupancy().get(j) + confirmationDetails.getCCE_lastName_Occupancy().get(j);
				
				innerloop : for (int k = 0; k < OccuPancyListForMailFromPayment.size(); k++) {
					
					if(paymentPageCusDetials.contains(OccuPancyListForMailFromPayment.get(k))){
							
						testCaseCustomerMail ++;			
						PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Hotel Occupancy Details - Customer Confirmation Email</td>");
						PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");						
						PrintWriter.append("<td>"+OccuPancyListForMailFromPayment.get(k)+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
						
						break innerloop;
					}
				}					
			}
			
			///
			
			testCaseCustomerMail++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Notes - Customer Confirmation Email]</td>");
			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
			
			if(activitydetails.getCustomerNotes().replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCCE_CustomerNote().replaceAll(" ", ""))){
				
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
			}
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CustomerNote()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
			}
			
			
			
			
			//Activity cancellation policy
			
			testCaseCustomerMail ++;
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email and Payment page Cancellation policy</td>");
			PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
						
			if(activitydetails.getCancelPolicy().size() == confirmationDetails.getCanellationPolicy().size()){
					
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			if (activitydetails.getPaymentCancelPolicy().size() == 2) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+resPolist1+"</td>");
							
				if(resPolist1.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.equals(confirmationDetails.getCanellationPolicy().get(1))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			if (activitydetails.getPaymentCancelPolicy().size() == 3) {
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+policyListOne+"</td>");
							
				if(policyListOne.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Cancellation policy 2</td>");
				PrintWriter.append("<td>"+policyListTwo+"</td>");
							
				if(policyListTwo.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCanellationPolicy().get(1).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCustomerMail ++;			
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.replaceAll(" ", "").equals(confirmationDetails.getCanellationPolicy().get(2).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			//Other details in Mail
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Tel 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCCE_Tel_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Tel_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Email 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCCE_Email_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Email_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCCE_Website().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Website()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCCE_Fax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_Fax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCustomerMail ++;			
			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Company Name</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getCCE_CompanyName().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCCE_CompanyName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
		
		
			
			
			
			
			
		}else {

			PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Email - Customer Confirmation Email</td>");
			PrintWriter.append("<td>Customer Confirmation Email should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
		}
		
		PrintWriter.append("</table>");	
		
		
		//Portal Mail
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Portal Email</p>");
		
		testCasePortaiMailCount = 1;
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCustomerPortalMailLoaded() == true) {
			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getPortalBookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalBookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalBookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			for (int i = 0; i < hotelList.size(); i++) {
				
				for (int j = 0; j < confirmationDetails.getPortalHotelsHeading().size() ; j++) {
				
					if (confirmationDetails.getPortalHotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))) {
					
						testCasePortaiMailCount++;
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Hotel "+(i+1)+" - Name</td>");
						PrintWriter.append("<td>"+hotelList.get(i)+"</td>");
						
						if(confirmationDetails.getPortalHotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getPortalHotelsHeading().get(j).split(": ")[1]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getPortalHotelsHeading().get(j).split(": ")[1]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						String cceMailHeadingCreate = ""+diffDays+" Night(s) "+search.getRooms()+" Room(s) Stay at:  "+hotelList.get(i)+"";						
						String originalWeb = confirmationDetails.getPortalHotelsHeading().get(j).split(",")[0] + confirmationDetails.getPortalHotelsHeading().get(j).split(",")[1];
						
						testCasePortaiMailCount++;
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Hotel "+(i+1)+" - Heading</td>");
						PrintWriter.append("<td>"+cceMailHeadingCreate+"</td>");
						
						if(cceMailHeadingCreate.replaceAll(" ", "").equalsIgnoreCase(originalWeb.replaceAll(" ", ""))){
							
							PrintWriter.append("<td>"+originalWeb+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+originalWeb+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCasePortaiMailCount++;
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Hotel "+(i+1)+" - Check In</td>");
						PrintWriter.append("<td>"+reportDateFrom1+"</td>");
						
						if(reportDateFrom1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getPortalCheckIn().get(j).replaceAll("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getPortalCheckIn().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getPortalCheckIn().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCasePortaiMailCount++;
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Hotel "+(i+1)+" - Check Out</td>");
						PrintWriter.append("<td>"+reportDateTo1+"</td>");
						
						if(reportDateTo1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getPortalCheckOut().get(j).replaceAll("-", ""))){
							
							PrintWriter.append("<td>"+confirmationDetails.getPortalCheckOut().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else{
							PrintWriter.append("<td>"+confirmationDetails.getPortalCheckOut().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCasePortaiMailCount ++;
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Hotel "+(i+1)+" - Booking Status</td>");
						PrintWriter.append("<td>Confirmed</td>");
						
						if(confirmationDetails.getPortalHotelBookingStatus().get(j).equalsIgnoreCase("Confirmed")){
							
							PrintWriter.append("<td>"+confirmationDetails.getPortalHotelBookingStatus().get(j)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
						}
						
						else {
							
							PrintWriter.append("<td>"+confirmationDetails.getPortalHotelBookingStatus().get(j)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
						}
						
						
						
					}
				}	
			}
			
		
			
			//Activity Details
			
			testCasePortaiMailCount++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Acitivity Name</td>");
			PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
			
			if(search.getActivities().split(" - ")[0].replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getPortalActivityType())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalActivityType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalActivityType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Activity - Booking Status</td>");
			PrintWriter.append("<td>Confirmed</td>");
			
			if(confirmationDetails.getPortalBookingStatus().equalsIgnoreCase("Confirmed")){
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalBookingStatus()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalBookingStatus()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
					
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Activity Period /Session</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_Period().get(0)+"</td>");
						
			if(activitydetails.getResultsPage_Period().get(0).equalsIgnoreCase(confirmationDetails.getPortalPeriod())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalPeriod()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalPeriod()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Activity Rate Type</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(0)+"</td>");
						
			if(confirmationDetails.getPortalRateType().toLowerCase().contains(activitydetails.getResultsPage_RateType().get(0).toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalRateType()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalRateType()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			/*testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Customer Confirmation Email - Activity Rate</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(0)+"</td>");
						
			if(confirmationDetails.getPortalRate().equals(activitydetails.getResultsPage_DailyRate().get(0))){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalRate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalRate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}*/
			
		
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Activity QTY</td>");
			PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
						
			if(Integer.toString(totalPaxCount).equals(confirmationDetails.getPortalQTY())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalQTY()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalQTY()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Currency 1</td>");
			PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
						
			if(search.getSellingCurrency().toLowerCase().equals(confirmationDetails.getPortalTotalValue_Currency().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalTotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalTotalValue_Currency()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			//////
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - First Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
						
			if(activitydetails.getPaymentPage_FName().equals(confirmationDetails.getPortalFName())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalFName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalFName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Last Name</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
						
			if(activitydetails.getPaymentPage_LName().equals(confirmationDetails.getPortalLName())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalLName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalLName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - TP No</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
						
			if(activitydetails.getPaymentPage_TP().equals(confirmationDetails.getPortalTP())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalTP()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalTP()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Email</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
						
			if(confirmationDetails.getPortalEmail().contains(activitydetails.getPaymentPage_Email())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Address</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
						
			if(activitydetails.getPaymentPage_address().equals(confirmationDetails.getPortaladdress())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortaladdress()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortaladdress()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Country</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
						
			if(activitydetails.getPaymentPage_country().equals(confirmationDetails.getPortalcountry())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalcountry()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalcountry()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - City</td>");
			PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
						
			if(activitydetails.getPaymentPage_city().equals(confirmationDetails.getPortalcity())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalcity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalcity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			////
			
			//Activity Occupancy
			
			for (int i = 0; i < activitydetails.getResultsPage_cusTitle().size(); i++) {
				
				testCasePortaiMailCount++;
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Activity Customer  - [Customer "+(i+1)+"] Title - Portal Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusTitle().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusTitle().get(i).equals(confirmationDetails.getPortalCusTitle().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusTitle().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				
				testCasePortaiMailCount++;
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Activity Counts - [Customer "+(i+1)+"] FName- Portal Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusFName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusFName().get(i).equals(confirmationDetails.getPortalCusFName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusFName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
				testCasePortaiMailCount++;
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Activity Counts - [Customer "+(i+1)+"] LName - Portal Email]</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_cusLName().get(i)+"</td>");
				
				if(activitydetails.getResultsPage_cusLName().get(i).equals(confirmationDetails.getPortalCusLName().get(i))){
					
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
				}
				else{
					PrintWriter.append("<td>"+confirmationDetails.getPortalCusLName().get(i)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
				}
				
			}
			
			////
			
			for (int j = 0; j < confirmationDetails.getPortal_title_Occupancy().size(); j++) {
				
				String paymentPageCusDetials = confirmationDetails.getPortal_title_Occupancy().get(j) + confirmationDetails.getPortal_firstName_Occupancy().get(j) + confirmationDetails.getPortal_lastName_Occupancy().get(j);
				
				innerloop : for (int k = 0; k < OccuPancyListForMailFromPayment.size(); k++) {
					
					if(paymentPageCusDetials.contains(OccuPancyListForMailFromPayment.get(k))){
							
						testCasePortaiMailCount ++;			
						PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Hotel Occupancy Details - Portal Email</td>");
						PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");						
						PrintWriter.append("<td>"+OccuPancyListForMailFromPayment.get(k)+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
						
						break innerloop;
					}
				}					
			}
			
			////
			
			testCasePortaiMailCount++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Customer Notes - Portal Email]</td>");
			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
			
			if(activitydetails.getCustomerNotes().replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getPortalCustomerNote().replaceAll(" ", ""))){
				
				PrintWriter.append("<td>"+confirmationDetails.getPortalCustomerNote()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
			}
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalCustomerNote()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
			}
			
			
			
			
			//Activity cancellation policy
			
			testCasePortaiMailCount ++;
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Customer Confirmation Email and Payment page Cancellation policy</td>");
			PrintWriter.append("<td>Payment Page :- "+activitydetails.getCancelPolicy().size()+"</td>");
						
			if(activitydetails.getCancelPolicy().size() == confirmationDetails.getCanellationPolicy().size()){
					
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>Confirmation Report :- "+confirmationDetails.getCanellationPolicy().size()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			////
			
			if (activitydetails.getPaymentCancelPolicy().size() == 2) {
				
				testCasePortaiMailCount ++;			
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+resPolist1+"</td>");
							
				if(resPolist1.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCasePortaiMailCount ++;			
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.equals(confirmationDetails.getCanellationPolicy().get(1))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			if (activitydetails.getPaymentCancelPolicy().size() == 3) {
				
				testCasePortaiMailCount ++;			
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Cancellation policy 1</td>");
				PrintWriter.append("<td>"+policyListOne+"</td>");
							
				if(policyListOne.equals(confirmationDetails.getCanellationPolicy().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(0)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCasePortaiMailCount ++;			
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Cancellation policy 2</td>");
				PrintWriter.append("<td>"+policyListTwo+"</td>");
							
				if(policyListTwo.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getCanellationPolicy().get(1).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(1)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCasePortaiMailCount ++;			
				PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - No Show Fee Apply</td>");
				PrintWriter.append("<td>"+noShowActual+"</td>");
							
				if(noShowActual.replaceAll(" ", "").equals(confirmationDetails.getCanellationPolicy().get(2).replaceAll(" ", ""))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+confirmationDetails.getCanellationPolicy().get(2)+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
			}
			
			//Other details in Mail
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getPortalTel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalTel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalTel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Tel 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getPortalTel_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalTel_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalTel_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getPortalEmail_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Email 2</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getPortalEmail_2().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail_2()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalEmail_2()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getPortalWebsite().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalWebsite()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalWebsite()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getPortalFax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalFax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalFax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCasePortaiMailCount ++;			
			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Portal Email - Company Name</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getPortalCompanyName().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getPortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getPortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
		
		
			
			
			
			
			
		}else {

			PrintWriter.append("<tr><td>"+testCasePortaiMailCount+"</td> <td>Email - Portal Email</td>");
			PrintWriter.append("<td>Portal Email should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
		}
		
		
		PrintWriter.append("</table>");	
	
		
		////////////////////////////////////////////////////
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Voucher Mail - Activity</p>");
		
		testCVEActivity = 1;
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isCVEMail_Activity() == true) {
		
			
			String leadPassengr = confirmationDetails.getCVE_LeadPassenger().replace(" ", "");
			
			String cusName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
							
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher EMail - Lead Passenger Name</td>");
			PrintWriter.append("<td>"+cusName+"</td>");
						
			if(leadPassengr.contains(cusName)){
					
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCVEActivity ++;	
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher EMail - Booking No</td>");
			PrintWriter.append("<td>"+activitydetails.getReservationNo()+"</td>");
			
			if(activitydetails.getReservationNo().equals(confirmationDetails.getCVE_BookingNo())){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_BookingNo()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			String cveIssueDate = confirmationDetails.getCVE_IssueDate().replace("-", "");
			
			testCVEActivity ++;	
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher EMail - Booking Issue Date</td>");
			PrintWriter.append("<td>"+currentDateforMatch+"</td>");
			
			if(currentDateforMatch.replace("-", "").equals(cveIssueDate)){
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_IssueDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCVEActivity++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher EMail - Acitivity Name</td>");
			PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
			
			if(search.getActivities().split(" - ")[0].replaceAll(" ", "").toLowerCase().contains(confirmationDetails.getCVE_ActivityName().replaceAll(" ", "").toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			///////
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Booking Status</td>");
			PrintWriter.append("<td>"+activitydetails.getConfirmation_BI_BookingStatus()+"</td>");
			
			if (activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains("request")) {
				
				if(activitydetails.getConfirmation_BI_BookingStatus().replace("-", "").toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().replace(" ", "").toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
			} else {

				if(activitydetails.getConfirmation_BI_BookingStatus().toLowerCase().contains(confirmationDetails.getCVE_bookingStatus().toLowerCase())){
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCVE_bookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
			}
			
			///////
			
			
			
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Program City</td>");
			PrintWriter.append("<td>"+search.getDestination()+"</td>");
						
			if(search.getDestination().toLowerCase().contains(confirmationDetails.getCVE_ProgramCity().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ProgramCity()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			////
			
			
			
//			testCVEActivity ++;
//			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Supplier Address</td>");
//			PrintWriter.append("<td>"+inventory.getSupplier_Add()+"</td>");
//						
//			if(confirmationDetails.getCVE_SupplierAdd().replaceAll(" ", "").toLowerCase().contains(inventory.getSupplier_Add().replaceAll(" ", "").toLowerCase())){
//					
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else {
//				
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierAdd()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
//			}
//			
//			testCVEActivity ++;
//			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Supplier TP</td>");
//			PrintWriter.append("<td>"+inventory.getSupplier_TP()+"</td>");
//						
//			if(inventory.getSupplier_TP().replace("-", "").toLowerCase().equals(confirmationDetails.getCVE_SupplierTP().toLowerCase())){
//					
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else {
//				
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_SupplierTP()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
//			}
			
			////
			
			testCVEActivity++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher EMail - Acitivity Name - 2</td>");
			PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
			
			if(search.getActivities().split(" - ")[0].toLowerCase().contains(confirmationDetails.getCVE_ActivityTransferDes().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ActivityTransferDes()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Activity Rate type </td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(0)+"</td>");
						
			if(confirmationDetails.getCVE_RatePlan().toLowerCase().contains(activitydetails.getResultsPage_RateType().get(0).toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_RatePlan()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Activity Session</td>");
			PrintWriter.append("<td>"+activitydetails.getResultsPage_Period().get(0)+"</td>");
						
			if(confirmationDetails.getCVE_Duration().toLowerCase().contains(activitydetails.getResultsPage_Period().get(0).toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Duration()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
		
			
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Activity QTY</td>");
			PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
						
			if(Integer.toString(totalPaxCount).equals(confirmationDetails.getCVE_Qty())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Qty()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			String CVE_ServiceDateChanegd = confirmationDetails.getCVE_ServiceDate().replace("-", "");
			
			testCVEActivity ++;
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Activity Service Date</td>");
			PrintWriter.append("<td>"+search.getActivityDate()+"</td>");
						
			if(search.getActivityDate().replace("-", "").equals(CVE_ServiceDateChanegd)){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else {
				
				PrintWriter.append("<td>"+confirmationDetails.getCVE_ServiceDate()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			
//			testCVEActivity ++;			
//			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Customer Notes</td>");
//			PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
//						
//			if(confirmationDetails.getCVE_CustomerNotes().toLowerCase().contains(activitydetails.getCustomerNotes().toLowerCase())){
//					
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
//			}
//			
//			else{
//				PrintWriter.append("<td>"+confirmationDetails.getCVE_CustomerNotes()+"</td>");
//				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
//			}
//			
			
			
			testCVEActivity ++;			
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getCVE_Tel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Tel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEActivity ++;			
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getCVE_Email_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Email_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEActivity ++;			
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getCVE_Website().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Website()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCVEActivity ++;			
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Customer Voucher Email - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getCVE_Fax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getCVE_Fax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			
			
			
		
			
		
		}else{
			
			PrintWriter.append("<tr><td>"+testCVEActivity+"</td> <td>Email - Customer Voucher Mail - Activity</td>");
			PrintWriter.append("<td>Customer Voucher Mail should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
		}
		
		PrintWriter.append("</table>");	
		
		/////////////
		
		
		ArrayList<HotelInfo> hotelList = search.getHotelInfoDetails();
		
		ArrayList<String> hotelName = new ArrayList<String>();
		ArrayList<String> hotelCity = new ArrayList<String>();
		ArrayList<String> hotelAddress = new ArrayList<String>();
		ArrayList<String> hotelTel = new ArrayList<String>();
		ArrayList<String> hotelStarCat = new ArrayList<String>();
		
		for (HotelInfo hotelInfo : hotelList) {
			
			hotelName.add(hotelInfo.getHotel_Name());
			hotelCity.add(hotelInfo.getCity());
			hotelAddress.add(hotelInfo.getAddress());
			hotelTel.add(hotelInfo.getTel());
			hotelStarCat.add(hotelInfo.getStarCate());
					
		}
		
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Voucher Mail - Hotel 1</p>");
		
		testCVEHotel1 = 1;
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isHotel1_Mail() == true) {
		
			String leadPassengr = confirmationDetails.getH1_LeadPassenger().replace(" ", "");			
			String cusName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
							
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Lead Passenger Name</td>");
			PrintWriter.append("<td>"+cusName+"</td>");
						
			if(leadPassengr.contains(cusName)){
					
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
		
			
			
				
			for (int i = 0; i < hotelName.size(); i++) {
				
				if (hotelName.get(i).replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH1_HotelName().replaceAll(" ", ""))) {
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Name</td>");
					PrintWriter.append("<td>"+hotelName.get(i)+"</td>");
					PrintWriter.append("<td>"+confirmationDetails.getH1_HotelName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");
					
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel City</td>");
					PrintWriter.append("<td>"+hotelCity.get(i)+"</td>");
								
					if(hotelCity.get(i).equalsIgnoreCase(confirmationDetails.getH1_City())){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Address</td>");
					PrintWriter.append("<td>"+hotelAddress.get(i)+"</td>");
								
					if(hotelAddress.get(i).replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH1_Address().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_Address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Telephone</td>");
					PrintWriter.append("<td>"+hotelTel.get(i)+"</td>");
								
					if(confirmationDetails.getH1_Tel().replaceAll(" ", "").contains(hotelTel.get(i).replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_Tel()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Tel()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel CheckIn</td>");
					PrintWriter.append("<td>"+reportDateFrom1+"</td>");
					
					if(reportDateFrom1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH1_CheckIn().replaceAll("-", ""))){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_CheckIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_CheckIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel CheckOut</td>");
					PrintWriter.append("<td>"+reportDateTo1+"</td>");
					
					if(reportDateTo1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH1_CheckOut().replaceAll("-", ""))){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_CheckOut()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_CheckOut()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
									
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Status</td>");
					PrintWriter.append("<td>Confirmed</td>");
					
					if(confirmationDetails.getH1_Status().equalsIgnoreCase("Confirmed")){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_Status()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Status()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - No of Nights</td>");
					PrintWriter.append("<td>"+diffDays+"</td>");
					
					if(diffDays == Integer.parseInt(confirmationDetails.getH1_Nights())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_Nights()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Nights()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - No of Rooms</td>");
					PrintWriter.append("<td>"+search.getRooms()+"</td>");
					
					if(Integer.parseInt(search.getRooms()) == Integer.parseInt(confirmationDetails.getH1_Rooms())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_Rooms()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Rooms()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - No of Adults</td>");
					PrintWriter.append("<td>"+totalAdultCount+"</td>");
					
					if(totalAdultCount == Integer.parseInt(confirmationDetails.getH1_Adult())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_Adult()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Adult()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - No of Children</td>");
					PrintWriter.append("<td>"+totalChildCount+"</td>");
					
					if(totalChildCount == Integer.parseInt(confirmationDetails.getH1_Children())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH1_Children()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_Children()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Star Cat</td>");
					PrintWriter.append("<td>"+hotelStarCat.get(i)+"</td>");
								
					if(hotelStarCat.get(i).equalsIgnoreCase(confirmationDetails.getH1_StarCat())){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_StarCat()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_StarCat()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
					/*testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Hotel Contact Details</td>");
					PrintWriter.append("<td>"+hotelTel.get(i)+"</td>");
								
					if(confirmationDetails.getH1_HotelCon_Tel().replaceAll(" ", "").contains(hotelTel.get(i).replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_HotelCon_Tel()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_HotelCon_Tel()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}*/
					
					testCVEHotel1++;
					PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Customer Notes</td>");
					PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
								
					if(activitydetails.getCustomerNotes().replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH1_CusNotes().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH1_CusNotes()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH1_CusNotes()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					for (int j = 0; j < confirmationDetails.getH1_PassengerNames().size(); j++) {
						
						String paymentPageCusDetials = confirmationDetails.getH1_PassengerNames().get(j).replaceAll(" / ", "");
								
						innerloop : for (int k = 0; k < OccuPancyListForMailFromPayment_1.size(); k++) {
							
							if(paymentPageCusDetials.contains(OccuPancyListForMailFromPayment_1.get(k))){
									
								testCVEHotel1 ++;			
								PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Passeneger Names</td>");
								PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");						
								PrintWriter.append("<td>"+OccuPancyListForMailFromPayment_1.get(k)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								break innerloop;
							}
						}					
					}
					
					
					
				}						
			}
			
			
			
			testCVEHotel1 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getH1_PortalTel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalTel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalTel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEHotel1 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getH1_PortalEmail_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEHotel1 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getH1_PortalWebsite().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalWebsite()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalWebsite()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCVEHotel1 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getH1_PortalFax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalFax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalFax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCVEHotel1 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Customer Voucher EMail (Hotel 1) - Company Name</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getH1_PortalCompanyName().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH1_PortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			
			
			
			
			
		
		
		
		}else{
			
			PrintWriter.append("<tr><td>"+testCVEHotel1+"</td> <td>Email - Customer Voucher Mail - Hotel 1</td>");
			PrintWriter.append("<td>Customer Voucher Mail (Hotel 1) should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
		}
		
		PrintWriter.append("</table>");	
		
		
		
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<p class='fontStyles'>Email - Customer Voucher Mail - Hotel 2</p>");
		
		testCVEHotel2 = 1;
		PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
		
		if (confirmationDetails.isHotel2_Mail() == true) {
		
			String leadPassengr = confirmationDetails.getH2_LeadPassenger().replace(" ", "");			
			String cusName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
							
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Lead Passenger Name</td>");
			PrintWriter.append("<td>"+cusName+"</td>");
						
			if(leadPassengr.contains(cusName)){
					
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+leadPassengr+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
		
			
			
				
			for (int i = 0; i < hotelName.size(); i++) {
				
				if (hotelName.get(i).replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH2_HotelName().replaceAll(" ", ""))) {
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Name</td>");
					PrintWriter.append("<td>"+hotelName.get(i)+"</td>");
					PrintWriter.append("<td>"+confirmationDetails.getH2_HotelName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");
					
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel City</td>");
					PrintWriter.append("<td>"+hotelCity.get(i)+"</td>");
								
					if(hotelCity.get(i).equalsIgnoreCase(confirmationDetails.getH2_City())){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Address</td>");
					PrintWriter.append("<td>"+hotelAddress.get(i)+"</td>");
								
					if(hotelAddress.get(i).replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH2_Address().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_Address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Telephone</td>");
					PrintWriter.append("<td>"+hotelTel.get(i)+"</td>");
								
					if(confirmationDetails.getH2_Tel().replaceAll(" ", "").contains(hotelTel.get(i).replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_Tel()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Tel()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
			
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel CheckIn</td>");
					PrintWriter.append("<td>"+reportDateFrom1+"</td>");
					
					if(reportDateFrom1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH2_CheckIn().replaceAll("-", ""))){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_CheckIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_CheckIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel CheckOut</td>");
					PrintWriter.append("<td>"+reportDateTo1+"</td>");
					
					if(reportDateTo1.replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH2_CheckOut().replaceAll("-", ""))){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_CheckOut()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_CheckOut()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
									
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Status</td>");
					PrintWriter.append("<td>Confirmed</td>");
					
					if(confirmationDetails.getH2_Status().equalsIgnoreCase("Confirmed")){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_Status()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Status()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - No of Nights</td>");
					PrintWriter.append("<td>"+diffDays+"</td>");
					
					if(diffDays == Integer.parseInt(confirmationDetails.getH2_Nights())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_Nights()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Nights()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - No of Rooms</td>");
					PrintWriter.append("<td>"+search.getRooms()+"</td>");
					
					if(Integer.parseInt(search.getRooms()) == Integer.parseInt(confirmationDetails.getH2_Rooms())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_Rooms()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Rooms()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - No of Adults</td>");
					PrintWriter.append("<td>"+totalAdultCount+"</td>");
					
					if(totalAdultCount == Integer.parseInt(confirmationDetails.getH2_Adult())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_Adult()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Adult()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - No of Children</td>");
					PrintWriter.append("<td>"+totalChildCount+"</td>");
					
					if(totalChildCount == Integer.parseInt(confirmationDetails.getH2_Children())){
						
						PrintWriter.append("<td>"+confirmationDetails.getH2_Children()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_Children()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Star Cat</td>");
					PrintWriter.append("<td>"+hotelStarCat.get(i)+"</td>");
								
					if(hotelStarCat.get(i).equalsIgnoreCase(confirmationDetails.getH2_StarCat())){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_StarCat()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_StarCat()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
					/*testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Hotel Contact Details</td>");
					PrintWriter.append("<td>"+hotelTel.get(i)+"</td>");
								
					if(confirmationDetails.getH2_HotelCon_Tel().replaceAll(" ", "").contains(hotelTel.get(i).replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_HotelCon_Tel()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_HotelCon_Tel()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}*/
					
					testCVEHotel2++;
					PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Customer Notes</td>");
					PrintWriter.append("<td>"+activitydetails.getCustomerNotes()+"</td>");
								
					if(activitydetails.getCustomerNotes().replaceAll(" ", "").equalsIgnoreCase(confirmationDetails.getH2_CusNotes().replaceAll(" ", ""))){
							
						PrintWriter.append("<td>"+confirmationDetails.getH2_CusNotes()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
					}
					
					else{
						PrintWriter.append("<td>"+confirmationDetails.getH2_CusNotes()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					for (int j = 0; j < confirmationDetails.getH2_PassengerNames().size(); j++) {
						
						String paymentPageCusDetials = confirmationDetails.getH2_PassengerNames().get(j).replaceAll(" / ", "");
								
						innerloop : for (int k = 0; k < OccuPancyListForMailFromPayment_1.size(); k++) {
							
							if(paymentPageCusDetials.contains(OccuPancyListForMailFromPayment_1.get(k))){
									
								testCVEHotel2 ++;			
								PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Passeneger Names</td>");
								PrintWriter.append("<td>"+paymentPageCusDetials+"</td>");						
								PrintWriter.append("<td>"+OccuPancyListForMailFromPayment_1.get(k)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
								
								break innerloop;
							}
						}					
					}
					
					
					
				}						
			}
			
			
			
			testCVEHotel2 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Tel 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Tel")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Tel").toLowerCase().equals(confirmationDetails.getH2_PortalTel_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalTel_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalTel_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEHotel2 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Email 1</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Email")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Email").toLowerCase().equals(confirmationDetails.getH2_PortalEmail_1().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalEmail_1()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			testCVEHotel2 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Website</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Website")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Website").toLowerCase().equals(confirmationDetails.getH2_PortalWebsite().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalWebsite()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalWebsite()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCVEHotel2 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Fax</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Fax")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Fax").toLowerCase().equals(confirmationDetails.getH2_PortalFax().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalFax()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalFax()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			testCVEHotel2 ++;			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Customer Voucher EMail (Hotel 2) - Company Name</td>");
			PrintWriter.append("<td>"+PG_Properties.getProperty("Portal.Name")+"</td>");
						
			if(PG_Properties.getProperty("Portal.Name").toLowerCase().equals(confirmationDetails.getH2_PortalCompanyName().toLowerCase())){
					
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
			}
			
			else{
				PrintWriter.append("<td>"+confirmationDetails.getH2_PortalCompanyName()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			
			
			
			
			
		
		
		
		}else{
			
			PrintWriter.append("<tr><td>"+testCVEHotel2+"</td> <td>Email - Customer Voucher Mail - Hotel 2</td>");
			PrintWriter.append("<td>Customer Voucher Mail (Hotel 2) should be available</td>");
			PrintWriter.append("<td>Not Available</td>");
			PrintWriter.append("<td class='Failed'>FAIL</td><tr>");			
		}
		
		PrintWriter.append("</table>");	
		
		
		
		
		
	}
	
	
	public void getQuotationMial(WebDriver Driver){
			
		if (search.getQuotationReq().equalsIgnoreCase("yes")) {
			
			PrintWriter.append("<br><br>");
			PrintWriter.append("<p class='fontStyles'>Email - Quotation Email</p>");
			
			testQuoteMail = 1;
			PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
			
			if (quoteDetails.isQuotationMailLoaded() == true) {
				
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+activitydetails.getQuote_QuotationNo()+"</td>");
							
				if(quoteDetails.getQuote_BookingNo().contains(activitydetails.getQuote_QuotationNo())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingNo()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingNo()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
							
				String referenceName = activitydetails.getPaymentPage_Title() + activitydetails.getPaymentPage_FName() + activitydetails.getPaymentPage_LName();
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Quotation Number</td>");
				PrintWriter.append("<td>"+referenceName+"</td>");
							
				if(quoteDetails.getQuote_BookingRef().contains(referenceName)){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingRef()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingRef()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				for (int i = 0; i < hotelList.size(); i++) {
					
					for (int j = 0; j < quoteDetails.getQuote_HotelsHeading().size(); j++) {
												
						if (quoteDetails.getQuote_HotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))) {
																	
							testQuoteMail++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" - Name</td>");
							PrintWriter.append("<td>"+hotelList.get(i)+"</td>");
							
							if(quoteDetails.getQuote_HotelsHeading().get(j).replaceAll(" ", "").contains(hotelList.get(i).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelsHeading().get(j).split(": ")[1]+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelsHeading().get(j).split(": ")[1]+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							
							String cceMailHeadingCreate = ""+diffDays+" Night(s) "+search.getRooms()+" Room(s) Stay at:  "+hotelList.get(i)+"";						
							String originalWeb = quoteDetails.getQuote_HotelsHeading().get(j).split(",")[0] + quoteDetails.getQuote_HotelsHeading().get(j).split(",")[1];
							
							testQuoteMail++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" - Heading</td>");
							PrintWriter.append("<td>"+cceMailHeadingCreate+"</td>");
							
							if(cceMailHeadingCreate.replaceAll(" ", "").equalsIgnoreCase(originalWeb.replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+originalWeb+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+originalWeb+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
						
							testQuoteMail++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" - Check In</td>");
							PrintWriter.append("<td>"+reportDateFrom1+"</td>");
							
							if(reportDateFrom1.replaceAll(" ", "").equalsIgnoreCase(quoteDetails.getQuote_CheckIn().get(j).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+quoteDetails.getQuote_CheckIn().get(j)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+quoteDetails.getQuote_CheckIn().get(j)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testQuoteMail++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" - Check Out</td>");
							PrintWriter.append("<td>"+reportDateTo1+"</td>");
							
							if(reportDateTo1.replaceAll(" ", "").equalsIgnoreCase(quoteDetails.getQuote_CheckOut().get(j).replaceAll(" ", ""))){
								
								PrintWriter.append("<td>"+quoteDetails.getQuote_CheckOut().get(j)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else{
								PrintWriter.append("<td>"+quoteDetails.getQuote_CheckOut().get(j)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							testQuoteMail ++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" - Booking Status</td>");
							PrintWriter.append("<td>Quote</td>");
							
							if(quoteDetails.getQuote_HotelBookingStatus().get(j).equalsIgnoreCase("Quote")){
								
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelBookingStatus().get(j)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {
								
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelBookingStatus().get(j)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							
							//Rates
							
							testQuoteMail ++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" >> Sub Total</td>");
							PrintWriter.append("<td>"+hotelSubTotal.get(i)+"</td>");
										
							if(quoteDetails.getQuote_HotelSubTotal().get(i).contains(hotelSubTotal.get(i))){
									
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelSubTotal().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelSubTotal().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testQuoteMail ++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" >> Total Tax & Service Charges</td>");
							PrintWriter.append("<td>"+hotelTotalTax.get(i)+"</td>");
										
							if(quoteDetails.getQuote_HotelTotalTax().get(i).contains(hotelTotalTax.get(i))){
									
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelTotalTax().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelTotalTax().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
							testQuoteMail ++;
							PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Hotel "+(i+1)+" >> Hotel Total Booking Value</td>");
							PrintWriter.append("<td>"+hotelTotalRate.get(i)+"</td>");
										
							if(quoteDetails.getQuote_HotelBookingValue().get(i).contains(hotelTotalRate.get(i))){
									
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelBookingValue().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
							}
							
							else {				
								PrintWriter.append("<td>"+quoteDetails.getQuote_HotelBookingValue().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
							}
							
						
						}		
					}		
				}
				
				
				
				testQuoteMail++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Acitivity Name</td>");
				PrintWriter.append("<td>"+search.getActivities().split(" - ")[0]+"</td>");
				
				if(search.getActivities().split(" - ")[0].replaceAll(" ", "").equalsIgnoreCase(quoteDetails.getQuote_ActivityType())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActivityType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActivityType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Activity - Booking Status</td>");
				PrintWriter.append("<td>Quote</td>");
				
				if(quoteDetails.getQuote_BookingStatus().equalsIgnoreCase("Quote")){
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_BookingStatus()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
						
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Activity Period /Session</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_Period().get(0)+"</td>");
							
				if(activitydetails.getResultsPage_Period().get(0).equalsIgnoreCase(quoteDetails.getQuote_Period())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_Period()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_Period()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Activity Rate Type</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_RateType().get(0)+"</td>");
							
				if(quoteDetails.getQuote_RateType().toLowerCase().contains(activitydetails.getResultsPage_RateType().get(0).toLowerCase())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_RateType()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_RateType()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				/*testCaseCustomerMail ++;
				PrintWriter.append("<tr><td>"+testCaseCustomerMail+"</td> <td>Customer Confirmation Email - Activity Rate</td>");
				PrintWriter.append("<td>"+activitydetails.getResultsPage_DailyRate().get(0)+"</td>");
							
				if(confirmationDetails.getCCE_Rate().equals(activitydetails.getResultsPage_DailyRate().get(0))){
						
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+confirmationDetails.getCCE_Rate()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}*/
				
			
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Activity QTY</td>");
				PrintWriter.append("<td>"+Integer.toString(totalPaxCount)+"</td>");
							
				if(Integer.toString(totalPaxCount).equals(quoteDetails.getQuote_QTY())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_QTY()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {
					
					PrintWriter.append("<td>"+quoteDetails.getQuote_QTY()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Currency 1</td>");
				PrintWriter.append("<td>"+search.getSellingCurrency()+"</td>");
							
				if(search.getSellingCurrency().toLowerCase().equals(quoteDetails.getQuote_TotalValue_Currency().toLowerCase())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalValue_Currency()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalValue_Currency()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Activity >> Sub Total - Quotation Mail</td>");
				PrintWriter.append("<td>"+actSubTotal+"</td>");
							
				if(quoteDetails.getQuote_ActSubTotal().contains(Integer.toString(actSubTotal))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActSubTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActSubTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Activity  >> Total Tax & Service Charges - Quotation Mail</td>");
				PrintWriter.append("<td>"+actTax+"</td>");
							
				if(quoteDetails.getQuote_ActTax().contains(Integer.toString(actTax))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActTax()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActTax()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Activity >> Total - Quotation Mail</td>");
				PrintWriter.append("<td>"+actTotalVal+"</td>");
							
				if(quoteDetails.getQuote_ActTotal().contains(Integer.toString(actTotalVal))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActTotal()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_ActTotal()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				//Total rates
				
				
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Sub Total - Quotation Mail</td>");
				PrintWriter.append("<td>"+subTotalAllProducts+"</td>");
							
				if(quoteDetails.getQuote_GrossTotalBookingValue().contains(Integer.toString(subTotalAllProducts))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_GrossTotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_GrossTotalBookingValue()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Total Taxes and Other Charges - Quotation Mail</td>");
				PrintWriter.append("<td>"+totalTaxAllProducts+"</td>");
							
				if(quoteDetails.getQuote_TotalTaxOther_2().contains(Integer.toString(totalTaxAllProducts))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalTaxOther_2()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				testQuoteMail ++;
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Total - Quotation Mail</td>");
				PrintWriter.append("<td>"+totalMybasketValue+"</td>");
							
				if(quoteDetails.getQuote_TotalBookingValue_3().contains(Integer.toString(totalMybasketValue))){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else {				
					PrintWriter.append("<td>"+quoteDetails.getQuote_TotalBookingValue_3()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - First Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_FName()+"</td>");
							
				if(activitydetails.getPaymentPage_FName().equals(quoteDetails.getQuote_FName())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_FName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_FName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Last Name</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_LName()+"</td>");
							
				if(activitydetails.getPaymentPage_LName().equals(quoteDetails.getQuote_LName())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_LName()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_LName()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - TP No</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_TP()+"</td>");
							
				if(activitydetails.getPaymentPage_TP().equals(quoteDetails.getQuote_TP())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_TP()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_TP()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Email</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_Email()+"</td>");
							
				if(quoteDetails.getQuote_Email().contains(activitydetails.getPaymentPage_Email())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_Email()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_Email()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Address</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_address()+"</td>");
							
				if(activitydetails.getPaymentPage_address().equals(quoteDetails.getQuote_address())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_address()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_address()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - Country</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_country()+"</td>");
							
				if(activitydetails.getPaymentPage_country().equals(quoteDetails.getQuote_country())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_country()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_country()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testQuoteMail ++;			
				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Quotation Email - City</td>");
				PrintWriter.append("<td>"+activitydetails.getPaymentPage_city()+"</td>");
							
				if(activitydetails.getPaymentPage_city().equals(quoteDetails.getQuote_city())){
						
					PrintWriter.append("<td>"+quoteDetails.getQuote_city()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");					
				}
				
				else{
					PrintWriter.append("<td>"+quoteDetails.getQuote_city()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			}else {

				PrintWriter.append("<tr><td>"+testQuoteMail+"</td> <td>Email - Quotation Email</td>");
				PrintWriter.append("<td>Quotation Email should be available</td>");
				PrintWriter.append("<td>Not Available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
			}
			
			PrintWriter.append("</table>");	
						
			
		}
		
	}
	
	
	
	
	public String getDateSuffix(String day) { 
		
        String dateChange = (day.substring(0, 2) + day.substring(5, 13)).replaceAll(" ", "");
		
        return dateChange;
	}
	
	
	
	public String getDayNumberSuffix(String date) {
		
		StringBuffer stringBuffer = new StringBuffer(date);		
		int day = Integer.parseInt(date.substring(0, 2));
				
	    if (day >= 11 && day <= 13) {
	        return stringBuffer.insert(2, "th").toString();
	    }
	    switch (day % 10) {
	    case 1:
	        return stringBuffer.insert(2, "st").toString();
	    case 2:
	        return stringBuffer.insert(2, "nd").toString();
	    case 3:
	        return stringBuffer.insert(2, "rd").toString();
	    default:
	        return stringBuffer.insert(2, "th ").toString();
	    }
	    	    
	}
	
}
