package com.reader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.model.ActivityDetails;
import com.model.ActivityInfo;
import com.model.HotelInfo;
import com.model.Search;

public class RatesCalculation {
	
	private Search searchInfo;
	private ActivityDetails activityDetails;
	private double netRate; 
	private double sellRate; 
	private double totalTaxRate, hotelBookingFee; 
	private int diffDays;
	private List<String> adultList;	
	private List<String> childList;
	private int totalAdultCount = 0;
	private int totalChildCount = 0;
	
	
	public RatesCalculation(Search searchfo){
		
		this.searchInfo = searchfo; 	
	}
	
	public ActivityDetails getDailyRatesCalculation(){
		
		activityDetails = new ActivityDetails();
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();
		String dateFromToCal = searchInfo.getDateFrom();
		String dateToCal = searchInfo.getDateTo();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		
		Date d1 = null;
		Date d2 = null;
		
		ArrayList<String> hotelSubTotal = new ArrayList<String>();
		ArrayList<String> hotelTotalTax = new ArrayList<String>();
		ArrayList<String> hotelTotalRate = new ArrayList<String>();
		ArrayList<String> hotelName = new ArrayList<String>();
		
		ArrayList<HotelInfo> hotelList = searchInfo.getHotelInfoDetails();
		
		for (HotelInfo hotelInfo : hotelList) {
			
			String hName = hotelInfo.getHotel_Name();
			netRate = Double.parseDouble(hotelInfo.getHotelNetRate());
			double pm = Double.parseDouble(hotelInfo.getPM());
			hotelBookingFee = Double.parseDouble(hotelInfo.getHotelBookingFee());
			
			sellRate = (netRate * ((100 + pm) / 100));	
			
			
			try {
				d1 = format.parse(dateFromToCal);
				d2 = format.parse(dateToCal);
	 
				//in milliseconds
				long diffD = d2.getTime() - d1.getTime();
				diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
						
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int rooms = Integer.parseInt(searchInfo.getRooms());
			sellRate = Math.ceil(sellRate * diffDays * rooms);						
			double totalNetRate = netRate * rooms * diffDays; 
			
			double salesTax = 0;
			double occupancyTax = 0;
			double energyTax = 0;
			double totalRate;
			
			if (hotelInfo.getSalesTax_Type().equalsIgnoreCase("percentage")) {
				double sTax = Double.parseDouble(hotelInfo.getSalesTax_Value());
				salesTax = totalNetRate * (sTax/100);
				
			} else {
				double sTax = Double.parseDouble(hotelInfo.getSalesTax_Value());
				salesTax = sTax;
			}
			
			if (hotelInfo.getOccupancy_Type().equalsIgnoreCase("percentage")) {
				double oTax = Double.parseDouble(hotelInfo.getOccupancy_Value());
				occupancyTax = totalNetRate * (oTax/100);
			} else {
				double oTax = Double.parseDouble(hotelInfo.getOccupancy_Value());
				occupancyTax = oTax;
			}
			
			if (hotelInfo.getEnergy_Type().equalsIgnoreCase("percentage")) {
				double eTax = Double.parseDouble(hotelInfo.getEnergy_Value());
				energyTax = totalNetRate * (eTax/100);
			} else {
				double eTax = Double.parseDouble(hotelInfo.getEnergy_Value());
				energyTax = eTax;
			}
			
			
			totalTaxRate = Math.ceil(salesTax + occupancyTax + energyTax + hotelBookingFee);
			totalRate = Math.ceil(sellRate + totalTaxRate);
			
			hotelSubTotal.add(Double.toString(sellRate));
			hotelTotalTax.add(Double.toString(totalTaxRate));
			hotelTotalRate.add(Double.toString(totalRate));
			hotelName.add(hName);
			
		}
		
		activityDetails.setHotelRatesName(hotelName);
		activityDetails.setHotelSubTotal(hotelSubTotal);
		activityDetails.setHotelTotalTax(hotelTotalTax);
		activityDetails.setHotelTotalRate(hotelTotalRate);
		
		//Activity rates
		
		if (searchInfo.getAdults().contains("#")) {
			String adu = searchInfo.getAdults();
			String[] partsAdults = adu.split("#");
			for (String valueAdu : partsAdults) {
				adultList.add(valueAdu);
			}
		} else {
			String adu = searchInfo.getAdults();
			adultList.add(adu);
		}
		
		if (searchInfo.getChildren().contains("#")) {
			String chi = searchInfo.getChildren();
			String[] partschilds = chi.split("#");
			for (String valuechi : partschilds) {
				childList.add(valuechi);
			}
		} else {
			String chi = searchInfo.getChildren();
			childList.add(chi);
		}
		
		for (int y = 0; y < Integer.parseInt(searchInfo.getRooms()); y++) {
			
			totalAdultCount = totalAdultCount + Integer.parseInt(adultList.get(y));
			totalChildCount = totalChildCount + Integer.parseInt(childList.get(y));																						
		}
		
		
		
		ArrayList<ActivityInfo> activityList = searchInfo.getActivityInfoDetails();
			
		for (ActivityInfo activityInfo : activityList) {
			
			double actSellRate;
			int paxCnt = totalAdultCount + totalChildCount;
			double actNetRate = Double.parseDouble(activityInfo.getActivityNetRate());
			double pm = Double.parseDouble(activityInfo.getProfir_Markup());
			
			actSellRate = (actNetRate* ((100 + pm) / 100)) * paxCnt;	
			double totalNetRate = (actNetRate * paxCnt);
			
			double totalNetRate_SalesTax = 0;
			double totalNetRate_MisFees = 0;
					
			if (activityInfo.getSalexTax_Type().equalsIgnoreCase("percentage")) {
				
				double salesTax = Double.parseDouble(activityInfo.getSalexTax_Value());
				totalNetRate_SalesTax = (totalNetRate* ((salesTax) / 100));				
			}
			
			if (activityInfo.getSalexTax_Type().equalsIgnoreCase("value")) {			
				double salesTax = Double.parseDouble(activityInfo.getSalexTax_Value());
				totalNetRate_SalesTax = salesTax;
			}
			
			if (activityInfo.getMisFee_Type().equalsIgnoreCase("percentage")) {
				
				double misFees = Double.parseDouble(activityInfo.getMisFee_Value());
				totalNetRate_MisFees = (totalNetRate* ((misFees) / 100));				
			}
			
			if (activityInfo.getMisFee_Type().equalsIgnoreCase("value")) {			
				double misFees = Double.parseDouble(activityInfo.getMisFee_Value());
				totalNetRate_MisFees = misFees;
			}
			
			int subTotal = (int) Math.ceil(actSellRate);
			int totalTax = (int) Math.ceil(totalNetRate_SalesTax + totalNetRate_MisFees);
			int actTotal = subTotal + totalTax;
			
			activityDetails.setCcFee(activityInfo.getCC_Fee());
			activityDetails.setActSubTotal(Double.toString(subTotal));
			activityDetails.setActTotalTax(Double.toString(totalTax));
			activityDetails.setActTotalRate(Double.toString(actTotal));
			
			
		}
		
			
		return activityDetails;
		
	}

}
