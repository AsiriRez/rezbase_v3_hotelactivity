package com.reader;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.ActivityDetails;
import com.model.PaymentDetails;
import com.model.Search;

public class ReservationFlow {
	
	private WebDriver driver         = null;
	private Search searchInfo;
	private int childCountFromexcel;
	private int divCount;
	private List<String> valueList, adultList, childList;
	private List<String> hotelList, activityList;
	private String currentDate, currentDateforImages, hotel1_ID, hotel2_ID;
	private String idOnly, IdStatus, idNo2, idForRemove, idNoFor2, selectedDateExcel, idOnlyForPickup;
	private ActivityDetails activityDetails ;
	private String cusFirstName, cusLastName;
		
	public ReservationFlow(ActivityDetails actDetails, WebDriver driverD, Search search){
		
		this.driver = driverD;
		this.searchInfo = search;	
		this.activityDetails = actDetails;
	}
	
	public WebDriver searchEngine(WebDriver webdriver) throws InterruptedException, ParseException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("yyyy_MM_dd");
		Calendar cal = Calendar.getInstance();
		currentDateforImages = dateFormatcurrentDate.format(cal.getTime());
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		valueList = new ArrayList<String>();
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();	
		hotelList = new ArrayList<String>();
		activityList = new ArrayList<String>();	
	
		driver.get(PG_Properties.getProperty("Baseurl") + "/admin/common/LoginPage.do");
		Thread.sleep(1500);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("user_id")));

		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/table/tbody/tr[1]/td/table/tbody/tr/td[5]/img")));
		driver.findElement(By.xpath(".//*[@id='main_mnu_0']")).click();
		driver.findElement(By.xpath(".//*[@id='mnu_8']")).click();
			
		driver.switchTo().defaultContent();
		driver.switchTo().frame("bec_container_frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hLi")));
		 		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine.png"));
        
        driver.findElement(By.xpath(".//*[@id='hLi']/a/span")).click();
		Thread.sleep(2500);
       	
		if (driver.findElements(By.id("H_Loc")).size() == 0) {
			activityDetails.setBookingEngineLoaded(false);
		}
		
		/////
		
		try {
			if (driver.findElement(By.id("H_Country")).isDisplayed() == false) {
				activityDetails.setCountryLoaded(false);
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		if (driver.findElement(By.id("H_Loc")).isDisplayed() == false) {
			activityDetails.setCityLoaded(false);
		}
		if (driver.findElement(By.id("ho_departure_temp")).isDisplayed() == false) {
			activityDetails.setDateFromLoaded(false);
		}
		if (driver.findElement(By.id("ho_arrival_temp")).isDisplayed() == false) {
			activityDetails.setDateToLoaded(false);
		}
		if (driver.findElement(By.id("R1occAdults_H")).isDisplayed() == false) {
			activityDetails.setAdultsLoaded(false);
		}
		if (driver.findElement(By.id("R1occChildren_H")).isDisplayed() == false) {
			activityDetails.setChildLoaded(false);
		}
		
		driver.findElement(By.id("H_nights")).click();
		File scrFile2d2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile2d2, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_Nights.png"));
		
			
		try {
			if (!(driver.findElement(By.xpath(".//*[@id='AC_Country']/option[3]")).getText().equalsIgnoreCase("Albania"))) {
				activityDetails.setCountryListLoaded(false);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		driver.findElement(By.id("ho_departure_temp")).click();
		File scrFile22 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile22, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_CalenderAvailable.png"));
		if (driver.findElement(By.id("ui-datepicker-div")).isDisplayed() == false) {
			activityDetails.setCalenderAvailble(false);
		}
		
		//".//*[@id='hotelDisplay']/div/ul/li[5]/div/a"
		String showAdd = driver.findElement(By.xpath(".//*[@id='hotelDisplay']/div/ul/li[5]/div/a")).getText();
		activityDetails.setShowAdd(showAdd);
		
		driver.findElement(By.xpath(".//*[@id='hotelDisplay']/div/ul/li[5]/div/a")).click();
		Thread.sleep(500);
		
		File scrFile23 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile23, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_AdditionalSearchOptions.png"));
        Thread.sleep(500);
		
		if (driver.findElement(By.id("hType_h")).isDisplayed() == false) {
			activityDetails.setHotelType(false);
		}
		
		try {
			if (!(driver.findElement(By.xpath(".//*[@id='hType_h']/option[2]")).getText().equalsIgnoreCase("Apartment"))) {
				activityDetails.setHotelTypeListLoaded(false);
			}
		} catch (Exception e1) {
			activityDetails.setHotelTypeListLoaded(false);
		}
		
		if (!(driver.findElement(By.xpath(".//*[@id='H_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setCurrencyListLoaded(false);
		}
		
		if (driver.findElement(By.xpath(".//*[@id='H_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED")) {
			driver.findElement(By.xpath(".//*[@id='H_consumerCurrencyCode']")).click();
			File scrFile232 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        FileUtils.copyFile(scrFile232, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_BookingEngine_CurrencyDropDown.png"));
	        Thread.sleep(500);		
		}
		
		if (!(driver.findElement(By.xpath(".//*[@id='star_rating_H']/option[2]")).getText().equalsIgnoreCase("1 Star"))) {
			activityDetails.setStarRating(false);
		}		
		if (driver.findElement(By.id("H_name")).isDisplayed() == false) {
			activityDetails.setHotelName(false);
		}		
		if (driver.findElement(By.id("discountCoupon_No_H")).isDisplayed() == false) {
			activityDetails.setPromoCode(false);
		}
			
		
		String hideAdd = driver.findElement(By.xpath(".//*[@id='hotelDisplay']/div/ul/li[5]/div/a")).getText();
		activityDetails.setHideAdd(hideAdd);
		
		driver.findElement(By.xpath(".//*[@id='hotelDisplay']/div/ul/li[5]/div/a")).click();
		Thread.sleep(500);
		
//		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
//		Thread.sleep(1500);
		
		////
		
		try {
			new Select(driver.findElement(By.id("H_Country"))).selectByVisibleText(searchInfo.getCountry());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		String hiddenDest = searchInfo.getDestination();		
		driver.findElement(By.id("H_Loc")).sendKeys(hiddenDest);
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText(hiddenDest)).click();
		
		//((JavascriptExecutor)driver).executeScript("document.getElementsByName('hid_H_Loc')[0].setAttribute('value','"+hiddenDest+"');");
		//((JavascriptExecutor)driver).executeScript("$('#hid_H_Loc').val('"+hiddenDest+"');");
		Thread.sleep(4000);
		//driver.findElement(By.partialLinkText(hiddenDest)).click();
		//driver.findElement(By.className("ui-corner-all")).click();
			
		((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('"+searchInfo.getDateFrom()+"');");
     	((JavascriptExecutor)driver).executeScript("$('#ho_arrival').val('"+searchInfo.getDateTo()+"');");
		
     	int rooms = Integer.parseInt(searchInfo.getRooms());
     	new Select(driver.findElement(By.id("norooms_H"))).selectByValue(Integer.toString(rooms));
     	
     	if (searchInfo.getAdults().contains("#")) {
			
			String ages = searchInfo.getAdults();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				adultList.add(value);
			}
		}
		
		else{
			String ages = searchInfo.getAdults();
			adultList.add(ages);
		}
	     	
     	if (searchInfo.getChildren().contains("#")) {
	
			String ages = searchInfo.getChildren();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				childList.add(value);
			}
     	}

		else{
			String ages = searchInfo.getChildren();
			childList.add(ages);
		}

     	for (int i = 1; i <= rooms; i++) {
			
     		new Select(driver.findElement(By.id("R"+i+"occAdults_H"))).selectByVisibleText(adultList.get(i-1));
    		new Select(driver.findElement(By.id("R"+i+"occChildren_H"))).selectByVisibleText(childList.get(i-1));
    		    		
		}
		
		/*childCountFromexcel = Integer.parseInt(searchInfo.getChildren());
		
		if (searchInfo.getAgeOfChildren().contains("#")) {
			
			String ages = searchInfo.getAgeOfChildren();
			String[] partsEA = ages.split("#");
			for (String value : partsEA) {
				valueList.add(value);
			}
		}
		
		else{
			String ages = searchInfo.getAgeOfChildren();
			valueList.add(ages);
		}
		
		
		for (int i = 0; i < childCountFromexcel; i++) {
			
			new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("A_R1childage_"+(i+1)+""))).selectByVisibleText(valueList.get(i));			
		}*/
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		/*WebElement element = driver.findElement(By.id("search_btns_h"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);*/
		
		//((JavascriptExecutor)driver).executeScript("return validate('formA');");
		driver.findElement(By.id("search_btns_h")).click();
		Thread.sleep(4000);
		
		//((JavascriptExecutor)driver).executeScript("$('#search_btns_h').click();");
				
		return driver;
	}
	
	
	public ActivityDetails searchResults(WebDriver webDriver) throws InterruptedException, IOException, ParseException{
		
		this.driver = webDriver;
				
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		WebDriverWait wait = new WebDriverWait(driver, 200);	
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ResPkgSearchForm")));
	
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
		currentDate = format1.format(cal.getTime());
		
		SimpleDateFormat formatToDate = new SimpleDateFormat("dd-MMM-yyyy");
		String cDate = formatToDate.format(cal1.getTime());
		activityDetails.setCurrentDate(cDate);
		
		driver.switchTo().defaultContent();
				
		Thread.sleep(5500);
		String showResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setShowResultsAdd(showResAdd);
		
		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);
		
		File scrFile231 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile231, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ResultsPage_HotelOneAdditionalFilters.png"));
        Thread.sleep(500);
		
		
		if (driver.findElement(By.id("star_rating_H")).isDisplayed() == false) {
			activityDetails.setResultsHotelStarRating(false);
		}
		if (driver.findElement(By.id("hType_h")).isDisplayed() == false) {
			activityDetails.setResultsHotelType(false);
		}
		if (driver.findElement(By.id("H_name")).isDisplayed() == false) {
			activityDetails.setResultsHotelName(false);
		}
		if (driver.findElement(By.id("discountCoupon_No_H")).isDisplayed() == false) {
			activityDetails.setResultsHotelPCode(false);
		}
		if (!(driver.findElement(By.xpath(".//*[@id='H_consumerCurrencyCode']/option[2]")).getText().equalsIgnoreCase("AED"))) {
			activityDetails.setResultsHotelPrefCurrency(false);
		}
		if (driver.findElement(By.id("priceLevelFrom_H")).isDisplayed() == false) {
			activityDetails.setResultsHotelPRF(false);
		}
		if (driver.findElement(By.id("priceLevelTo_H")).isDisplayed() == false) {
			activityDetails.setResultsHotelPRT(false);
		}	
			
		Thread.sleep(3500);		
		String hideResAdd = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).getText();
		activityDetails.setHideResultsAdd(hideResAdd);
		
		driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
		Thread.sleep(500);
		
		if (driver.findElement(By.id("price_slider_WJ_5")).isDisplayed() == false) {
			activityDetails.setPriceRangeFilter(false);
		}
		
		if (driver.findElement(By.id("booktype_text_WJ_9")).isDisplayed() == false) {
			activityDetails.setProgramAvailablity(false);
		}
		
		if (driver.findElement(By.id("name_text_WJ_6")).isDisplayed() == false) {
			activityDetails.setHotelNameFilter(false);
		}
		
				
		//////////
		
		boolean resultsElement;
		
		if (driver.findElements(By.id("hotel_1")).size() != 0) {
			resultsElement = true;
			activityDetails.setResultsAvailable(resultsElement);
		}else{
			resultsElement = false;
			activityDetails.setResultsAvailable(resultsElement);
		}
		
		
		File scrFile_2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile_2, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelOneResults.png"));
	
		if (resultsElement == true) {
			
			int hotelCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='content-m']/div[4]/div/div[1]/div/div/p[2]/span")).getText());
			System.out.println("Hotel count - " + hotelCount);
			activityDetails.setActivityCount(hotelCount);	
			selectedDateExcel = searchInfo.getActivityDate();
			
			int wholepages = (hotelCount/10);
			
			if(((hotelCount)%10) == 0){				
				System.out.println("Page count - " + wholepages);
			}else{
				wholepages ++;
				System.out.println("Page count - " + wholepages);
			}
			
			/*
			//Activity filter checking
			//Slider
			
			String minValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			String maxValue = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[6];
			activityDetails.setMinPrize(minValue);
			activityDetails.setMaxPrize(maxValue);
			
			//1
			String pageLoadedPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPageLoadedResults(pageLoadedPrize);
						
			//2
			
			WebElement slider = driver.findElement(By.xpath(".//*[@id='price_slider_WJ_5']/a[1]"));
			Actions move = new Actions(driver);
			Action action = move.dragAndDropBy(slider, 10, 0).build();
			action.perform();
			Thread.sleep(1000);	
			
			String prizeChanged = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_1(prizeChanged);
			String resultsPrize = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_1(resultsPrize);
					
			//3
			Actions move2 = new Actions(driver);
			Action action_2 = move2.dragAndDropBy(slider, 200, 0).build();
			action_2.perform();		
			
			String prizeChanged_100 = driver.findElement(By.xpath("/html/body/section/div[1]/div[2]/section/aside/div[2]/div[1]/div/div[1]")).getText().split(" ")[2];
			activityDetails.setPrizeChanged_2(prizeChanged_100);
			String resultsPrize_100 = driver.findElement(By.xpath(".//*[@id='activity_results_container_0']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
			activityDetails.setPrizeResults_2(resultsPrize_100);
			
			Thread.sleep(1500);	
			driver.findElement(By.xpath(".//*[@id='content-l']/div[2]/div[5]/a")).click();
			Thread.sleep(2500);	
			
			
			//On req
			
			Thread.sleep(2500);	
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("O");
			Thread.sleep(2500);	
			
			ArrayList<String> onReqLab = new ArrayList<String>();
			if (driver.findElements(By.className("result-block")).size() != 0) {
									
				List<WebElement> resultsblock = driver.findElements(By.className("result-block"));
				
				activityDetails.setResulteBlockSizeOnReq(resultsblock.size());
				
				for (int i = 0; i < resultsblock.size(); i++) {
					
					String labeltext = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/ul/li/p")).getText();
					onReqLab.add(labeltext);
				}
				
				activityDetails.setOnRequestLabels(onReqLab);
				
			}else{
				
				activityDetails.setResulteBlockSizeOnReq(1);
				onReqLab.add("On Request Activities Not Available");
				activityDetails.setOnRequestLabels(onReqLab);
				
			}
			
			
			//////
			
			Random ran = new Random();
			int x = ran.nextInt(100) + 5;
			
			if(x % 2 == 0 ){
				activityDetails.setAddAndContinue("true");
			}else{
				activityDetails.setAddAndContinue("false");
			}
			
			
			
			// select All activities	
			Thread.sleep(3500);	
			new Select(driver.findElement(By.xpath(".//*[@id='booktype_text_WJ_8']"))).selectByValue("B");
			Thread.sleep(2500);	
				
			///
			
			ArrayList<Integer> prizeList = new ArrayList<Integer>();			
			int prizeForList;
			
			if (10 <= activityCount) {
				
				ArrayList<Integer> prizeListEqual = new ArrayList<Integer>();
				
				for (int j = 0; j < 10; j++) {
					
					Thread.sleep(1500);	
					prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);
					
					if (j==0) {
						
						if (activityDetails.getAddAndContinue().equals("true")) {
							
							Thread.sleep(1500);	
							WebElement mainElement = driver.findElement(By.id("activity_results_container_"+j+""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");
							Thread.sleep(1500);	
													
							new Select(driver.findElement(By.xpath(".//*[@id='"+idForRemove+"_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(1500);	
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[2]/a")).click();	
							Thread.sleep(3500);	
							
							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
							
							//checkbox
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idForRemove+"_"+idNoFor2+"']")).click();
										
							WebElement elementChangeID = driver.findElement(By.id("activity_content_WJ_1_"+idForRemove+""));
							List<WebElement> classElements = elementChangeID.findElements(By.className("activity-list-body"));
														
							//Add and Continue
											
							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div["+(classElements.size()+2)+"]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AddToCart.png"));											        
									
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}
					        
							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);
																								
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}
					        
							driver.findElement(By.xpath("html/body/div[12]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);
							
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_RemoveCart.png"));							
					        							
							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);
														
						}
					}						
				}
				
				prizeListEqual.addAll(prizeList);
				activityDetails.setBeforeSorting(prizeListEqual);						
				
				
			}else{
				ArrayList<Integer> prizeListEqual = new ArrayList<Integer>();
				
				for (int j = 0; j < activityCount; j++) {
					
					prizeForList = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeList.add(prizeForList);
					
					if (j==0) {
						
						if (activityDetails.getAddAndContinue().equals("true")) {
							
							WebElement mainElement = driver.findElement(By.id("activity_results_container_"+j+""));
							WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
							idForRemove = idElement.getAttribute("code");
							
							
							new Select(driver.findElement(By.xpath(".//*[@id='"+idForRemove+"_date_select']"))).selectByVisibleText(selectedDateExcel);
							Thread.sleep(1500);	
							
							driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[2]/a")).click();	
							Thread.sleep(3500);	
							
							WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div[2]/div[4]"));
							idNoFor2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
							
							//checkbox
							driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idForRemove+"_"+idNoFor2+"']")).click();
													
							WebElement elementChangeID = driver.findElement(By.id("activity_content_WJ_1_"+idForRemove+""));
							List<WebElement> classElements = elementChangeID.findElements(By.className("activity-list-body"));
														
							//Add and Continue
											
							driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idForRemove+"']/div["+(classElements.size()+2)+"]/div/a[2]")).click();
							Thread.sleep(5500);
							driver.findElement(By.xpath("html/body/div[7]/span/i")).click();
							Thread.sleep(3000);
							
							File scrFile_6 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_6, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AddToCart.png"));											        
							
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setAddToCart(false);
							}
					        
							driver.findElement(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).click();
							Thread.sleep(3000);
																								
					        if (driver.findElements(By.xpath(".//*[@id='cart_display_WJ_11']/h4/div/span[2]/i")).size() == 0) {
								activityDetails.setActivityRemoved(false);
							}
					        
							driver.findElement(By.xpath("html/body/div[12]/div[11]/div/button[1]")).click();
							Thread.sleep(3000);
							
							File scrFile_7 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					        FileUtils.copyFile(scrFile_7, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_RemoveCart.png"));							
					        							
							driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[5]/input")).click();
							Thread.sleep(3000);
														
						}
					}						
				}
				
				prizeListEqual.addAll(prizeList);
				activityDetails.setBeforeSorting(prizeListEqual);						
				
			}
						
			Collections.sort(prizeList);
			activityDetails.setAfterSorting(prizeList);
			
			////////
			
			//Price highest to lowest			
			driver.findElement(By.xpath(".//*[@id='content-m']/div[2]/div/div[2]/div/div/ul[1]/li/a[2]/i")).click();
			Thread.sleep(1000);	
			ArrayList<Integer> prizeListDescendingOrder = new ArrayList<Integer>();
			ArrayList<Integer> prizeListDescendingOrderEQUAL = new ArrayList<Integer>();
			
			if (10 <= activityCount) {
				
				for (int j = 0; j < 10; j++) {
					
					int prizeForList2 = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList2);
					
				}
			}else{
				
				for (int j = 0; j < activityCount; j++) {
					
					int prizeForList2 = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='activity_results_container_"+j+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText());
					prizeListDescendingOrder.add(prizeForList2);
					
				}
			}
			
			prizeListDescendingOrderEQUAL.addAll(prizeListDescendingOrder);
			activityDetails.setBeforeSortingDescending(prizeListDescendingOrderEQUAL);						
			
			Collections.sort(prizeListDescendingOrder, Collections.reverseOrder());
			activityDetails.setAfterSortingDescending(prizeListDescendingOrder);
			
			driver.findElement(By.xpath(".//*[@id='content-m']/div[2]/div/div[2]/div/div/ul[1]/li/a[1]/i")).click();
			Thread.sleep(3000);	*/
			
			
			
			if (searchInfo.getHotels().contains("#")) {
				
				String ages = searchInfo.getHotels();
				String[] partsEA = ages.split("#");
				for (String value : partsEA) {
					hotelList.add(value);
				}
			}
			
			else{
				String ages = searchInfo.getHotels();
				hotelList.add(ages);
			}
		     	
	     	if (searchInfo.getActivities().contains("#")) {
		
				String ages = searchInfo.getActivities();
				String[] partsEA = ages.split("#");
				for (String value : partsEA) {
					activityList.add(value);
				}
	     	}

			else{
				String ages = searchInfo.getActivities();
				activityList.add(ages);
			}
			
	     	String hotelName_1 = hotelList.get(0);
			String hotelName_2 = hotelList.get(1);
			String activity_1 = activityList.get(0).split(" - ")[0];
			
			//Hotel 1
			
			
			
			final short HOTEL_PER_PAGE=10;
			outerloop : for (int hotelPage = 1; hotelPage <= wholepages ; hotelPage++) {
				for (int i = 1+((hotelPage-1)*10) ; i <= hotelPage*10; i++) {
					
					if (i == (hotelCount+1)) {
						
						break outerloop;
						
					} else {
						
						String webHotelName = driver.findElement(By.xpath(".//*[@id='hotelname_"+i+"']")).getText().replaceAll(" ", "");
						
						if (hotelName_1.replaceAll(" ", "").equalsIgnoreCase(webHotelName)) {
							
							//Add and Continue
							
							WebElement hotelIdElement = driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/section[1]"));
							hotel1_ID = hotelIdElement.getAttribute("class").split("-")[6];
							
							driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
							Thread.sleep(1500);
							File scrFile_h1M = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_h1M, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelOne_Moredetails.png"));	
							driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
							Thread.sleep(1500);
							
							driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();
							Thread.sleep(3500);
							File scrFile_h1C = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
							FileUtils.copyFile(scrFile_h1C, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelOne_CancellationPolicy.png"));	
							driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();
							Thread.sleep(3500);
							
						
							driver.findElement(By.xpath(".//*[@id='addncontinue_"+i+"']")).click();
							Thread.sleep(5500);
							
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='upselling_dig_WJ_34']/div/a[1]")));
							driver.findElement(By.xpath(".//*[@id='upselling_dig_WJ_34']/div/a[1]")).click();
							Thread.sleep(2000);
							
							//Activity Results
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_results_container_0']")));
							break outerloop;
								
						}					
					}					
				}
				
				
				if (2 <= hotelPage) {
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(hotelPage+1)+"]/a")).click();
				}else{
					driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(hotelPage+1)+"]/a")).click();
				}						
			}
			
			
			//Activiy 1
			
			boolean activityResultsElement;
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='activity_results_container_0']")));
			
			if (driver.findElements(By.id("activity_results_container_0")).size() != 0) {
				activityResultsElement = true;
				activityDetails.setActivityResultsAvailability(activityResultsElement);
			}else{
				activityResultsElement = false;
				activityDetails.setActivityResultsAvailability(activityResultsElement);
			}
			
			File scrFile_89 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile_89, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ActivityResults.png"));	
			
			String activityPagePax = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[4]/div")).getText();
			activityDetails.setActivityPagePax(activityPagePax);
						
			if(activityResultsElement == true){
				
				int activityCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='content']/div/div/div[1]/div/div/p[2]/span")).getText());
				
				int wholepagesAct = (activityCount/10);
				
				if(((activityCount)%10) == 0){				
					System.out.println("Page count - " + wholepagesAct);
				}else{
					wholepagesAct ++;
					System.out.println("Page count - " + wholepagesAct);
				}
				
				driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
				Thread.sleep(500);
				
				File scrFileA231 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFileA231, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ResultsPage_ActivityAdditionalFilters.png"));
		        Thread.sleep(500);
				
		        driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
				Thread.sleep(500);
				
				final short ACTIVITY_PER_PAGE=10;
				outerloop : for (int actvityPage = 1; actvityPage <= wholepagesAct ; actvityPage++) {
					for (int i = 0 ; i < ACTIVITY_PER_PAGE; i++) {
						
						if ( ((actvityPage-1)*ACTIVITY_PER_PAGE) + (i) >= activityCount) {
							
							break outerloop;
							
						} else {
							
							String webActivityName = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[1]/h1")).getText().replaceAll(" ", "");
							
							if (activity_1.replaceAll(" ", "").equalsIgnoreCase(webActivityName)) {
												
								WebElement mainElement = driver.findElement(By.id("activity_results_container_"+i+""));
								WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
								idOnly = idElement.getAttribute("code");
															
								WebElement daysElement = driver.findElement(By.id(""+idOnly+"_date_select"));
								List<WebElement> daysCountList = daysElement.findElements(By.tagName("option"));
								
								new Select(driver.findElement(By.xpath(".//*[@id='"+idOnly+"_date_select']"))).selectByVisibleText(selectedDateExcel);
								
								activityDetails.setDaysCount((daysCountList.size())-1);
								
								//Currency and Rate
								String activityCurr = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[1]/p/span[1]")).getText();
								activityDetails.setActivityCurrency(activityCurr);
								
								String mainRate = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[1]/p/span[2]")).getText();
								activityDetails.setMainRate(mainRate);							
								Thread.sleep(500);
								
								String description = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[1]/div")).getText();
								activityDetails.setActDescription(description);
								
								//more details
								driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();	
								Thread.sleep(3000);
								
								/*Object image = null;
								Object result = ((JavascriptExecutor) driver).executeScript(
										   "return arguments[0].complete && "+
										   "typeof arguments[0].naturalWidth != \"undefined\" && "+
										   "arguments[0].naturalWidth > 0", image);

								boolean loaded = false;
							    if (result instanceof Boolean) {
							      loaded = (Boolean) result;
							      System.out.println("More info images loaded");
							    }		    */
								
								activityDetails.setMoreInfo(true);
								
								File scrFile_55 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						        FileUtils.copyFile(scrFile_55, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ActivityMoreDetails.png"));							
						        
						        driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();	
								Thread.sleep(3000);
						
								//cancellation policy link
								
								driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();	
								Thread.sleep(8000);
								
								WebElement cancelElement = driver.findElement(By.className("cancellation-policy-body"));
								List<WebElement> rowsTo = cancelElement.findElements(By.tagName("li"));
								
								activityDetails.setCancelPolicy(true);
								
								File scrFile_5 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						        FileUtils.copyFile(scrFile_5, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ActivityCancellationPolicy.png"));							
						        						
								ArrayList<String> policyList = new ArrayList<String>();
								
								for (int j = 1; j <= rowsTo.size()-1; j++) {
									
									String policy = driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/section[2]/div/div/ul/li["+j+"]")).getText();
									policyList.add(policy);	
									
								}
								
								System.out.println(policyList);
								activityDetails.setCancelPolicy(policyList);
								
								driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/section[2]/a/i")).click();	
								Thread.sleep(1500);
								
								
								
								//////
								
								driver.findElement(By.xpath(".//*[@id='activity_results_container_"+i+"']/div/div[1]/div[2]/ul/li[3]/a")).click();	
								Thread.sleep(3500);	
								
								File scrFile_9 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						        FileUtils.copyFile(scrFile_9, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_AvailabilityActivityTypes.png"));							
								
								List<WebElement> activityTypesElement = driver.findElements(By.className("activity-list-body"));
								divCount = activityTypesElement.size();
								
								ArrayList<String> resultsPage_ActivityTypes = new ArrayList<String>();
								ArrayList<String> resultsPage_Period = new ArrayList<String>();
								ArrayList<String> resultsPage_RateType = new ArrayList<String>();
								ArrayList<String> resultsPage_DailyRate = new ArrayList<String>();
								
								
								for (int j = 0; j < activityList.size(); j++) {
									
									String excelActivityTypeName = activityList.get(0).split(" - ")[1].replaceAll(" ", "");
										
									for (int k = 1; k <= activityTypesElement.size(); k++) {
										
										Thread.sleep(1500);	
										String webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[1]")).getText().replaceAll(" ", "");
										Thread.sleep(3500);	
										
										if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {
											
											String rs_activityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[1]")).getText();
											resultsPage_ActivityTypes.add(rs_activityTypeName);
											
											String rs_activityPeriod = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[2]")).getText();
											resultsPage_Period.add(rs_activityPeriod);
											
											String rs_activityRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[3]")).getText();
											resultsPage_RateType.add(rs_activityRate);
											
											String rs_activityDailyRate = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[5]")).getText();
											resultsPage_DailyRate.add(rs_activityDailyRate);
											
											WebElement selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(k+1)+"]/div[4]"));
											idNo2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];
											
											int totalAdultCount = 0;
											int totalChildCount = 0;
											
											for (int y = 0; y < Integer.parseInt(searchInfo.getRooms()); y++) {
												
												totalAdultCount = totalAdultCount + Integer.parseInt(adultList.get(y));
												totalChildCount = totalChildCount + Integer.parseInt(childList.get(y));																						
											}
																		
											new Select(driver.findElement(By.xpath(".//*[@id='activity_qty_select_WJ_1_"+idOnly+"_"+idNo2+"']"))).selectByVisibleText(Integer.toString(totalAdultCount+totalChildCount));
																				
											driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_"+idOnly+"_"+idNo2+"']")).click();
																			
										}
									}						
								}
								
								
								activityDetails.setResultsPage_ActivityTypes(resultsPage_ActivityTypes);
								activityDetails.setResultsPage_Period(resultsPage_Period);
								activityDetails.setResultsPage_RateType(resultsPage_RateType);
								activityDetails.setResultsPage_DailyRate(resultsPage_DailyRate);
								
								driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_"+idOnly+"']/div["+(divCount+2)+"]/div/a[2]")).click();
								Thread.sleep(5500);
								
								wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='upselling_dig_WJ_24']/div/a[1]")));
								driver.findElement(By.xpath(".//*[@id='upselling_dig_WJ_24']/div/a[1]")).click();
								Thread.sleep(2000);
															
								break outerloop;
																			
							}
						}					
					}
					
					
					if (2 <= actvityPage) {
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(actvityPage+2)+"]/a")).click();
					}else{
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(actvityPage+1)+"]/a")).click();
					}						
				}
				
				
			
			}
			
			
			
			//Hotel 2
			
			boolean resultsElement_Hotel2;
			
			if (driver.findElements(By.id("hotel_1")).size() != 0) {
				resultsElement_Hotel2 = true;
				activityDetails.setHotelResultsTwo(resultsElement_Hotel2);
			}else{
				resultsElement_Hotel2 = false;
				activityDetails.setHotelResultsTwo(resultsElement_Hotel2);
			}
					
			File scrFile_132 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        FileUtils.copyFile(scrFile_132, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelTwoResults.png"));
		      
	        if(resultsElement_Hotel2 == true){
	        	
	        	int hotelCount_2 = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='content-m']/div[4]/div/div[1]/div/div/p[2]/span")).getText());
				System.out.println("Hotel count - " + hotelCount_2);
				
				String hotelPagePax = driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[4]/div")).getText();
				activityDetails.setHotel2PagePax(hotelPagePax);
				
				int wholepages_2 = (hotelCount_2/10);
				
				if(((hotelCount_2)%10) == 0){				
					System.out.println("Page count - " + wholepages_2);
				}else{
					wholepages_2 ++;
					System.out.println("Page count - " + wholepages_2);
				}
				
				driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
				Thread.sleep(500);
				
				File scrFileH231 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFileH231, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ResultsPage_HotelTwoyAdditionalFilters.png"));
		        Thread.sleep(500);
		        
		        driver.findElement(By.xpath(".//*[@id='ResPkgSearchForm']/div[7]/a")).click();
				Thread.sleep(500);
				
				outerloop : for (int hotelPage = 1; hotelPage <= wholepages_2 ; hotelPage++) {
					for (int i = 1+((hotelPage-1)*10) ; i <= hotelPage*10; i++) {
						
						if (i == (hotelCount_2+1)) {
							
							break outerloop;
							
						} else {
							
							String webHotelName = driver.findElement(By.xpath(".//*[@id='hotelname_"+i+"']")).getText().replaceAll(" ", "");
							
							if (hotelName_2.replaceAll(" ", "").equalsIgnoreCase(webHotelName)) {
								
								//Add and Continue
								
								WebElement hotelIdElement = driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/section[1]"));
								hotel2_ID = hotelIdElement.getAttribute("class").split("-")[6];
								
								driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
								Thread.sleep(1500);
								File scrFile_h1M = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
								FileUtils.copyFile(scrFile_h1M, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelTwo_Moredetails.png"));	
								driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[1]")).click();
								Thread.sleep(1500);
								
								driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/div[1]/div[1]/div[2]/div[2]/a[2]")).click();
								Thread.sleep(3500);
								File scrFile_h1C = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
								FileUtils.copyFile(scrFile_h1C, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_HotelTwo_CancellationPolicy.png"));	
								driver.findElement(By.xpath(".//*[@id='hotel_"+i+"']/div/section[3]/a/i")).click();
								Thread.sleep(3500);
								
								driver.findElement(By.xpath(".//*[@id='addncontinue_"+i+"']")).click();
								Thread.sleep(5500);
								
								driver.findElement(By.xpath("html/body/div[9]/span/i")).click();
								Thread.sleep(2000);
								
								String cartCurrencyandValue = driver.findElement(By.xpath(".//*[@id='basketPrice']/div")).getText();
								activityDetails.setCartVallue(cartCurrencyandValue);
								
								Thread.sleep(2000);
								driver.findElement(By.xpath(".//*[@id='basketPrice']/a")).click();
								Thread.sleep(2000);
								
								break outerloop;
									
							}					
						}					
					}
					
					
					if (2 <= hotelPage) {
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(hotelPage+2)+"]/a")).click();
						Thread.sleep(2000);
					}else{
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span["+(hotelPage+1)+"]/a")).click();
						Thread.sleep(2000);
					}						
				}
	        	
			}
	        
	        
	        
			//Payment Page
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("payment-cart-sum-wrapper")));
			
			WebElement idStatusElement = driver.findElement(By.xpath("/html/body/section/form/div[1]/header/div/div/div[6]/div/div[2]/div/div[1]/ul[2]/li[2]/div"));
			IdStatus = idStatusElement.getAttribute("id").substring(14, 18);
			
			//Shopping Basket
			
			File scrFile234= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        FileUtils.copyFile(scrFile234, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_PaymentsPage.png"));
	        			
			String shoppingCart_CartCurrency = driver.findElement(By.xpath(".//*[@id='sellingcurrency']")).getText();
			activityDetails.setShoppingCart_CartCurrency(shoppingCart_CartCurrency);			
			String shoppingCart_TotalBasket = driver.findElement(By.xpath(".//*[@id='totchargeamt-basket']")).getText();
			activityDetails.setShoppingCart_TotalBasketValue(shoppingCart_TotalBasket);
			
			String shoppingCart_subTotal = driver.findElement(By.xpath(".//*[@id='subtotal']")).getText();
			activityDetails.setShoppingCart_subTotal(shoppingCart_subTotal);
			String shoppingCart_TotalTax = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
			activityDetails.setShoppingCart_TotalTax(shoppingCart_TotalTax);
			String shoppingCart_TotalValue = driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText();
			activityDetails.setShoppingCart_TotalValue(shoppingCart_TotalValue);
			String shoppingCart_AmountNow = driver.findElement(By.xpath(".//*[@id='totalamountprocessnow']")).getText();
			activityDetails.setShoppingCart_AmountNow(shoppingCart_AmountNow);
			String shoppingCart_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='amountduetoutilazation']")).getText();
			activityDetails.setShoppingCart_AmountCheckIn(shoppingCart_AmountCheckIn);
			activityDetails.setPaymentDetails("Pay Online");
			
			
			//traveller details
			ArrayList<PaymentDetails> paymentInfoList = searchInfo.getPaymentDetailsInfo();
			
			for (PaymentDetails paymentDetails : paymentInfoList) {
				
				new Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(paymentDetails.getCustomerTitle());
				activityDetails.setPaymentPage_Title(paymentDetails.getCustomerTitle());
				
				cusFirstName = paymentDetails.getCustomerName();
				cusLastName = paymentDetails.getCustomerLastName();
				
				driver.findElement(By.id("cusFName")).sendKeys(cusFirstName);
				activityDetails.setPaymentPage_FName(cusFirstName);
				driver.findElement(By.id("cusLName")).sendKeys(cusLastName);
				activityDetails.setPaymentPage_LName(cusLastName);
				driver.findElement(By.id("cusAdd1")).sendKeys(paymentDetails.getAddress());
				activityDetails.setPaymentPage_address(paymentDetails.getAddress());
				
				driver.findElement(By.id("cusCity")).sendKeys(paymentDetails.getCity());
				activityDetails.setPaymentPage_city(paymentDetails.getCity());
				
				String onLineTp_1 = paymentDetails.getTel().split("-")[0];
				String onLineTp_2 = paymentDetails.getTel().split("-")[1];
				
				driver.findElement(By.id("cusareacodetext")).sendKeys(onLineTp_1);
				driver.findElement(By.id("cusPhonetext")).sendKeys(onLineTp_2);
				activityDetails.setPaymentPage_TP(paymentDetails.getTel());
				
				driver.findElement(By.id("cusEmail")).sendKeys(paymentDetails.getEmail());
				activityDetails.setPaymentPage_Email(paymentDetails.getEmail());
				driver.findElement(By.id("cusConfEmail")).sendKeys(paymentDetails.getEmail());
				
				new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(paymentDetails.getCountry());	
				activityDetails.setPaymentPage_country(paymentDetails.getCountry());
				Thread.sleep(1500);
				
				if (paymentDetails.getCountry().equalsIgnoreCase("USA") || paymentDetails.getCountry().equalsIgnoreCase("Canada") || paymentDetails.getCountry().equalsIgnoreCase("Australia")) {
					
					if (driver.findElement(By.id("cusState")).isDisplayed() == true) {
						
						new Select(driver.findElement(By.id("cusState"))).selectByVisibleText(paymentDetails.getState());				
						activityDetails.setPaymentPage_State(paymentDetails.getState());
						Thread.sleep(1500);
						driver.findElement(By.id("cusZip")).sendKeys(paymentDetails.getPostalCode());
						activityDetails.setPaymentPage_PostalCode(paymentDetails.getPostalCode());
						
					}else{
						activityDetails.setPaymentPage_State("Not Available");
					}
								
				}	
				
			}
			
			// QUOTATION req
			
			if (searchInfo.getQuotationReq().equalsIgnoreCase("Yes")) {
			
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div[2]/div/div[2]/div[2]/a[1]")).click();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("customername")));
				
				String Quote_QuotationNo = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[3]/p[2]/b")).getText();
				activityDetails.setQuote_QuotationNo(Quote_QuotationNo);
				/////
				
				File scrFile65= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile65, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"Quotation_ConfirmationPage.png"));
		       				
				String referenceName = driver.findElement(By.xpath(".//*[@id='customername']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_BookingRefference(referenceName);
				
				String confirmation_CusMailAddress = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[5]/p/span")).getText();
				activityDetails.setQuote_confirmation_CusMailAddress(confirmation_CusMailAddress);
				
				
				//Hotel 1 
				
				ArrayList<String> confirmationPage_HotelName = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelBookingStatus = new ArrayList<String>();				
				ArrayList<String> confirmationPage_HotelCheckInDate = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelCheckOutDate = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelNumberofRooms = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelNumberofNights = new ArrayList<String>();				
				ArrayList<String> Hotel_SubTotal = new ArrayList<String>();
				ArrayList<String> Hotel_TotalTaxServices = new ArrayList<String>();
				ArrayList<String> Hotel_Totals = new ArrayList<String>();
				
				ArrayList<WebElement> h_Sub = new ArrayList<WebElement>(driver.findElements(By.id("hotelsubtotal")));
				ArrayList<WebElement> h_Tax  = new ArrayList<WebElement>(driver.findElements(By.id("hoteltax")));
				ArrayList<WebElement> h_Total = new ArrayList<WebElement>(driver.findElements(By.id("hotelbookingvalue")));
				
				for (int i = 1; i <= hotelList.size(); i++) {
					
					String hotelName = driver.findElement(By.xpath(".//*[@id='hotelname_"+i+"']")).getText();
					confirmationPage_HotelName.add(hotelName);		
					String hotelBookingStatus = driver.findElement(By.xpath(".//*[@id='bookingstatus_"+i+"']")).getText();
					confirmationPage_HotelBookingStatus.add(hotelBookingStatus);	
					
					String checkInStatus = driver.findElement(By.xpath(".//*[@id='checkindate_"+i+"']")).getText();
					confirmationPage_HotelCheckInDate.add(checkInStatus);	
					
					ArrayList<WebElement> chekOUTArray = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='checkoutdate_"+i+"']")));
					
					confirmationPage_HotelCheckOutDate.add(chekOUTArray.get(0).getText());	
					confirmationPage_HotelNumberofRooms.add(chekOUTArray.get(1).getText());						
					confirmationPage_HotelNumberofNights.add(chekOUTArray.get(2).getText());	
					
					Hotel_SubTotal.add(h_Sub.get(i-1).getText());
					Hotel_TotalTaxServices.add(h_Tax.get(i-1).getText());
					Hotel_Totals.add(h_Total.get(i-1).getText());
					
				}
				
				activityDetails.setQuote_confirmationPage_HotelName(confirmationPage_HotelName);
				activityDetails.setQuote_confirmationPage_HotelBookingStatus(confirmationPage_HotelBookingStatus);
				activityDetails.setQuote_confirmationPage_HotelCheckInDate(confirmationPage_HotelCheckInDate);
				activityDetails.setQuote_confirmationPage_HotelCheckOutDate(confirmationPage_HotelCheckOutDate);
				activityDetails.setQuote_confirmationPage_HotelNumberofRooms(confirmationPage_HotelNumberofRooms);
				activityDetails.setQuote_confirmationPage_HotelNumberofNights(confirmationPage_HotelNumberofNights);
				activityDetails.setQuote_Hotel_SubTotal(Hotel_SubTotal);
				activityDetails.setQuote_Hotel_TotalTaxServices(Hotel_TotalTaxServices);
				activityDetails.setQuote_Hotel_Totals(Hotel_Totals);
				
				
				
				///////////////////////////////////////////////////////////////////////
				
				///Activity List
				
				String confirmation_BI_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[1]")).getText();
				activityDetails.setQuote_confirmation_BI_ActivityName(confirmation_BI_ActivityName);
				
				String confirmation_BI_City = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[2]")).getText();
				activityDetails.setQuote_confirmation_BI_City(confirmation_BI_City);
				
				String confirmation_BI_ActType = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[3]")).getText();
				activityDetails.setQuote_confirmation_BI_ActType(confirmation_BI_ActType);
				
				String confirmation_BI_BookingStatus = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[5]/div[1]/div/p[2]")).getText();
				activityDetails.setQuote_confirmation_BI_BookingStatus(confirmation_BI_BookingStatus);
				
				String confirmation_BI_SelectedDate = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[5]/div[2]/div/p[2]")).getText();
				activityDetails.setQuote_confirmation_BI_SelectedDate(confirmation_BI_SelectedDate);
				
				ArrayList<String> confirmationPage_RateType = new ArrayList<String>();
				ArrayList<String> confirmationPage_DailyRate = new ArrayList<String>();
				ArrayList<String> confirmationPage_QTY = new ArrayList<String>();
				
				WebElement confirmActTable = driver.findElement(By.className("activity-details-table-wrapper"));
				List<WebElement> confirmationActlist = confirmActTable.findElements(By.tagName("ul"));
				
				for (int i = 1; i <= (confirmationActlist.size()/2); i++) {
					
					String confirmationRates = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[1]")).getText();
					confirmationPage_RateType.add(confirmationRates);
					
					String confirmationCost = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[2]")).getText();
					confirmationPage_DailyRate.add(confirmationCost);
					
					String confirmationQTY = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[3]")).getText();
					confirmationPage_QTY.add(confirmationQTY);
					
				}
				
				activityDetails.setQuote_confirmation_RateType(confirmationPage_RateType);
				activityDetails.setQuote_confirmation_DailyRate(confirmationPage_DailyRate);
				activityDetails.setQuote_confirmation_QTY(confirmationPage_QTY);
				
				////
				
				String confirmation_Currency1 = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[7]/ul/li[1]/div")).getText();
				activityDetails.setQuote_confirmation_Currency1(confirmation_Currency1);		
				String confirmation_Currency2 = driver.findElement(By.xpath(".//*[@id='sellingcurrency_total']")).getText();
				activityDetails.setQuote_confirmation_Currency2(confirmation_Currency2);	
				
				
				//ActivityRates
				String confirmation_ActivityRate = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[7]/ul/li[2]/div[2]")).getText();
				activityDetails.setQuote_confirmation_ActivityRate(confirmation_ActivityRate);	
				
				String confirmation_SubTotal = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[2]/div[2]")).getText();
				activityDetails.setQuote_confirmation_SubTotal(confirmation_SubTotal);	
				
				String confirmation_Tax = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[3]/div[2]")).getText();
				activityDetails.setQuote_confirmation_Tax(confirmation_Tax);	
				
				String confirmation_Total = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[4]/div[2]")).getText();
				activityDetails.setQuote_confirmation_Total(confirmation_Total);	
				
				
	
				
				String confirmation_SubTotal2 = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setQuote_confirmation_SubTotal2(confirmation_SubTotal2);							
				
				String confirmation_TotalTaxOtherCharges = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setQuote_confirmation_TotalTaxOtherCharges(confirmation_TotalTaxOtherCharges);	
				
				String confirmation_Total2 = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setQuote_confirmation_Total2(confirmation_Total2);	
				
				
				//////
				//Traveler Details
							
				String confirmation_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_FName(confirmation_FName);
				
				String confirmation_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_LName(confirmation_LName);
				
				String confirmation_TP; 			
				try {
					confirmation_TP = driver.findElement(By.xpath(".//*[@id='cusemergencycontact']")).getText().split(": ")[1];
					activityDetails.setQuote_confirmation_TP(confirmation_TP);
				} catch (Exception e) {
					confirmation_TP = "Not Available";
					activityDetails.setQuote_confirmation_TP(confirmation_TP);
				}
			
				String confirmation_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_Email(confirmation_Email);
				
				String confirmation_address = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_address(confirmation_address);
				
				String confirmation_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_country(confirmation_country);
				
				String confirmation_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setQuote_confirmation_city(confirmation_city);
				
				
				if (activityDetails.getQuote_confirmation_country().equalsIgnoreCase("USA") || activityDetails.getQuote_confirmation_country().equalsIgnoreCase("Canada") || activityDetails.getQuote_confirmation_country().equalsIgnoreCase("Australia")) {

					String confirmation_State = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
					activityDetails.setQuote_confirmation_State(confirmation_State);
					
					String confirmation_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
					activityDetails.setQuote_confirmation_postalCode(confirmation_postalCode);
					
				}
				
				
				
			}else{
				
				//No Quotation
				
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[2]/div[2]/a[2]")).click();
				Thread.sleep(1000);
				
				//Occupancy Details
				
				 ArrayList<WebElement>  adultFirstlist = new ArrayList<WebElement>(driver.findElements(By.className("guest-first-name")));
				 ArrayList<WebElement>  adultLastlist  = new ArrayList<WebElement>(driver.findElements(By.className("guest-last-name")));				 
				 ArrayList<WebElement>  ChildFirstlist = new ArrayList<WebElement>(driver.findElements(By.className("child-first-name")));
				 ArrayList<WebElement>  ChildLastlist  = new ArrayList<WebElement>(driver.findElements(By.className("child-last-name")));
				 
				 HashMap<Integer, String> map = new HashMap<Integer, String>();
				 map.put(1, "one");
				 map.put(2, "two");
				 map.put(3, "three");
				 map.put(4, "four");
				 map.put(5, "five");
				 map.put(6, "six");
				 map.put(7, "seven");
				 map.put(8, "Eight");
				 map.put(9, "nine");
			     
				ArrayList<String> adulttitlePayments_List = new ArrayList<String>();
				ArrayList<String> adultFNamePayments_List = new ArrayList<String>();
				ArrayList<String> adultLNamePayments_List = new ArrayList<String>();
				ArrayList<String> childtitlePayments_List = new ArrayList<String>();
				ArrayList<String> childFNamePayments_List = new ArrayList<String>();
				ArrayList<String> childLNamePayments_List = new ArrayList<String>();
				 
				 Iterator<WebElement> adultIterator = adultFirstlist.iterator();
			     int AdultCount = 1;
			      
			     while(adultIterator.hasNext()){
			    	  adultIterator.next().sendKeys("AdFirst_"+map.get(AdultCount));
			    	  adultLastlist.get(AdultCount-1).sendKeys("AdLast_"+map.get(AdultCount));
			    	  adulttitlePayments_List.add("Mr");
			    	  if(AdultCount == 1){
			    		  adultFNamePayments_List.add(cusFirstName + "AdFirst_"+map.get(AdultCount));
				    	  adultLNamePayments_List.add(cusLastName + "AdLast_"+map.get(AdultCount));
			    	  }else{
			    		  adultFNamePayments_List.add("AdFirst_"+map.get(AdultCount));
				    	  adultLNamePayments_List.add("AdLast_"+map.get(AdultCount));					    	  
			    	  }
			    	  		    	  
			    	  AdultCount++;
			     }
				 
			     Iterator<WebElement> childIterator = ChildFirstlist.iterator();
			     int ChildCount = 1;
			     while(childIterator.hasNext()){
			    	  childIterator.next().sendKeys("ChFirst_"+map.get(ChildCount));
			    	  ChildLastlist.get(ChildCount-1).sendKeys("ChLast_"+map.get(ChildCount));
			    	  childtitlePayments_List.add("Mst");
			    	  childFNamePayments_List.add("ChFirst_"+map.get(ChildCount));
			    	  childLNamePayments_List.add("ChLast_"+map.get(ChildCount));
			    	  ChildCount++;
			     }
				
				int allPaxCount = adulttitlePayments_List.size() + childtitlePayments_List.size();
				activityDetails.setAllPaxCount(allPaxCount);
			     
				activityDetails.setAdulttitlePayments_List(adulttitlePayments_List);
				activityDetails.setAdultFNamePayments_List(adultFNamePayments_List);
				activityDetails.setAdultLNamePayments_List(adultLNamePayments_List);
				activityDetails.setChildtitlePayments_List(childtitlePayments_List);
				activityDetails.setChildFNamePayments_List(childFNamePayments_List);
				activityDetails.setChildLNamePayments_List(childLNamePayments_List);
				
				
			    ArrayList<WebElement>  activityFirstlist = new ArrayList<WebElement>(driver.findElements(By.className("act-occ-first-name")));
			    ArrayList<WebElement>  activityLastlist = new ArrayList<WebElement>(driver.findElements(By.className("act-occ-last-name")));
				
			    Iterator<WebElement> activityIterator = activityFirstlist.iterator();
			    int actPaxCount = 1; 
			    
			    ArrayList<String> activityTitle = new ArrayList<String>();
				ArrayList<String> activityFName = new ArrayList<String>();
				ArrayList<String> activityLName = new ArrayList<String>();
			    
			    while(activityIterator.hasNext()){
			    	activityIterator.next().sendKeys("TestFirst_"+map.get(actPaxCount));
			    	activityLastlist.get(actPaxCount-1).sendKeys("TestLast_"+map.get(actPaxCount));
			    	activityTitle.add("Mr");
			    	activityFName.add("TestFirst_"+map.get(actPaxCount));
			    	activityLName.add("TestLast_"+map.get(actPaxCount));
			    	actPaxCount++;    	
			    }
			    
			  
			    
			    activityDetails.setResultsPage_cusTitle(activityTitle);
			    activityDetails.setResultsPage_cusFName(activityFName);
			    activityDetails.setResultsPage_cusLName(activityLName);
			   
				//Pickup / DropOff Details
				
				String programId;
				
				try {	
					
					WebElement pickUplement = driver.findElement(By.name("programId"));
					programId = pickUplement.getAttribute("value");
					
					WebElement pickDetailsContainerElement = driver.findElement(By.xpath(".//*[@id='pickupdropoffdetailsnotadd"+programId+"']/div[2]/div"));
					String bidIdForPickUp = pickDetailsContainerElement.findElement(By.tagName("p")).getAttribute("id").split("Name-")[1];
					
					//DropOff details
					driver.findElement(By.name("pickuplocationStationPortNameTxt-"+bidIdForPickUp+"")).sendKeys("Colombo");
					driver.findElement(By.name("pickupaddionalinfo-"+bidIdForPickUp+"")).sendKeys("PickUp Additional Info");
					driver.findElement(By.name("dropofflocationNameStationPortTxt-"+bidIdForPickUp+"")).sendKeys("Hambanthota");
					driver.findElement(By.name("dropoffaddionalinfo-"+bidIdForPickUp+"")).sendKeys("DropOff Additional Info");
					
										
				} catch (Exception e) {
					e.printStackTrace();
				}
				
								
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[4]/div[2]/a[2]")).click();
				Thread.sleep(1000);
				
				//Billing Info
				
				/*new Select(driver.findElement(By.id("credit_card_type"))).selectByValue("mastercard");
		        Thread.sleep(1000);*/
				
				String paymentBilling_CardTotal = driver.findElement(By.xpath(".//*[@id='totchgpgamt']")).getText();
				activityDetails.setPaymentBilling_CardTotal(paymentBilling_CardTotal);
				String paymentBilling_CardCurrency = driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[3]/div/div[2]/ul/li[1]/div")).getText();
				activityDetails.setPaymentBilling_CardCurrency(paymentBilling_CardCurrency);
				
				String paymentBilling_subTotal = driver.findElement(By.xpath(".//*[@id='paymentdetails_subtotal']")).getText();
				activityDetails.setPaymentBilling_subTotal(paymentBilling_subTotal);
				String paymentBilling_TotalTax = driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText();
				activityDetails.setPaymentBilling_TotalTax(paymentBilling_TotalTax);
				String paymentBilling_TotalValue = driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText();
				activityDetails.setPaymentBilling_TotalValue(paymentBilling_TotalValue);
				String paymentBilling_AmountNow = driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText();
				activityDetails.setPaymentBilling_AmountNow(paymentBilling_AmountNow);
				String paymentBilling_AmountCheckIn = driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText();
				activityDetails.setPaymentBilling_AmountCheckIn(paymentBilling_AmountCheckIn);
				
				Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='ResPkgBookingDetailsForm']/div[2]/div/section/div/div/div[6]/div[4]/a[2]")).click();
				Thread.sleep(2000);
				
				//Notes
				
				String cusNotes = "I am wanting to generate a random string of 20 characters without " +
						"using apache classes. I don't really care about whether is " +
						"alphanumeric or not. Also, I am going to convert it to an array of bytes later FYI";
				driver.findElement(By.id("txt_cus_notes")).sendKeys(cusNotes);
				activityDetails.setCustomerNotes(cusNotes);
				
				String actNotes = "Activity notes displaying here.";
				driver.findElement(By.xpath(".//*[@id='activitynotesid0']/textarea")).sendKeys(actNotes);
				activityDetails.setActivityNotes(actNotes);
				Thread.sleep(1000);
				
				driver.findElement(By.xpath(".//*[@id='paynotes']/div[4]/a[2]")).click();
				Thread.sleep(1000);
				
				// Terms and condition
				
				String dateFromDate = searchInfo.getDateFrom();
				DateFormat dfTo = new SimpleDateFormat("yyyy/MMM/dd");
				SimpleDateFormat formatInTo = new SimpleDateFormat("MM/dd/yyyy");
				Date instanceFrom = formatInTo.parse(dateFromDate);  		
				String reportDateFrom = dfTo.format(instanceFrom).replaceAll("/", "-");
				
				String dateToDate = searchInfo.getDateTo();
				Date instanceTo = formatInTo.parse(dateToDate);  		
				String reportDateTo = dfTo.format(instanceTo).replaceAll("/", "-");
				
				ArrayList<String> hotelOnecancelListPay = new ArrayList<String>();
				ArrayList<String> hotelTwocancelListPay = new ArrayList<String>();
				ArrayList<String> cancelListPay = new ArrayList<String>();
				
				//Hotel 1 policy
				//rezbase_v3, rezpackage
						
				WebElement hotel1_cancelPolicyElement;
//				try {
//					hotel1_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateFrom+"@@rezbase_v3']/div/div[3]/span/ul"));
//					
//				} catch (Exception e) {
//					hotel1_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateTo+"@@rezbase_v3']/div/div[3]/span/ul"));					
//				}
				
				//rezproduction, acexperts, omanairholidays
				
				List<WebElement> liElementHotel1Policy = null;
				
				try {
					
					try {
						hotel1_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul"));
						
					} catch (Exception e) {
						hotel1_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateTo+"@@rezproduction']/div/div[3]/span/ul"));					
					}
								
					liElementHotel1Policy = hotel1_cancelPolicyElement.findElements(By.tagName("li"));
							
					for (int i = 1; i <= liElementHotel1Policy.size(); i++) {
						
						String cancelPolicies = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul/li["+i+"]")).getText();
						hotelOnecancelListPay.add(cancelPolicies);					
					}
					
				} catch (Exception e1) {
					hotelOnecancelListPay.add("Not Avialable");	
					hotelOnecancelListPay.add("Not Avialable");	
					hotelOnecancelListPay.add("Not Avialable");	
				}
				
							
				//Hotel 2 policy
				
				WebElement hotel2_cancelPolicyElement ;
//				try {
//					hotel2_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateFrom+"@@rezbase_v3']/div/div[3]/span/ul"));
//					
//				} catch (Exception e) {
//					hotel2_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateTo+"@@rezbase_v3']/div/div[3]/span/ul"));	
//				}
				
				List<WebElement> liElementHotel2Policy= null;
				
				try {
					
					try {
						hotel2_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul"));
						
					} catch (Exception e) {
						hotel2_cancelPolicyElement = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateTo+"@@rezproduction']/div/div[3]/span/ul"));	
					}
								
					liElementHotel2Policy = hotel2_cancelPolicyElement.findElements(By.tagName("li"));
					
					for (int i = 1; i <= liElementHotel2Policy.size(); i++) {
						
						String cancelPolicies = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul/li["+i+"]")).getText();
						hotelTwocancelListPay.add(cancelPolicies);					
					}
					
				} catch (Exception e1) {
					hotelTwocancelListPay.add("Not Avialable");
					hotelTwocancelListPay.add("Not Avialable");
					hotelTwocancelListPay.add("Not Avialable");
				}
				
				
				// Activity Policy
				
				WebElement cancelElementlistPayment = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]"));
				List<WebElement> liElementPay = cancelElementlistPayment.findElements(By.tagName("li"));
							
				for (int i = 1; i <= liElementPay.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]/ul/li["+i+"]")).getText();
					cancelListPay.add(cancelPolicies);
					
				}
				
				activityDetails.setPaymentCancelPolicy(cancelListPay);
				activityDetails.setHotel1_PaymentsPagePolicy(hotelOnecancelListPay);
				activityDetails.setHotel2_PaymentsPagePolicy(hotelTwocancelListPay);
				
			
				driver.findElement(By.xpath(".//*[@id='confreadPolicy']")).click();
				driver.findElement(By.xpath(".//*[@id='confTnC']")).click();
				
				driver.findElement(By.xpath(".//*[@id='confirm-payment-btn']")).click();			
				Thread.sleep(10500);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='divConfirm']/a")));
				
											
				//Popup window
				
							
				String paymentPopUp_ActivityName = driver.findElement(By.xpath(".//*[@id='header']/div[6]/div/div[2]/div/div[1]/ul[2]/li[1]/div")).getText();
				activityDetails.setPaymentPopUp_ActivityName(paymentPopUp_ActivityName);
							
				driver.findElement(By.xpath(".//*[@id='divConfirm']/a")).click();				
				Thread.sleep(10000);
				
				//Payment gateway
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paygatewayFrame")));
				driver.switchTo().frame("paygatewayFrame");
				
				File scrFile256 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile256, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_PaymentGateway.png"));
		       							        
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("document.getElementById('cardnumberpart1').value = '4000';");
				jse.executeScript("document.getElementById('cardnumberpart2').value = '0000';");
				jse.executeScript("document.getElementById('cardnumberpart3').value = '0000';");
				jse.executeScript("document.getElementById('cardnumberpart4').value = '0002';");
			
				new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText("01");
				new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText("2016");
				
				Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='cardholdername']")).sendKeys("aaa");
				jse.executeScript("document.getElementById('cv2').value = '111';");
				
				String amount = driver.findElement(By.xpath(".//*[@id='main_dev']/div[6]/div[2]")).getText();
				activityDetails.setPaymentGatewayTotal(amount);
							
				driver.findElement(By.xpath(".//*[@id='main_dev']/div[7]/div[2]/a/div")).click();	
				Thread.sleep(7000);
								
				
				/*try {
					driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");
					
				} catch (Exception e) {
					driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[2]/td[2]/b/input[1]")).sendKeys("1234");				
				}
				
				driver.findElement(By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[6]/td/form/div/table/tbody/tr/td/font/b/center/table/tbody/tr[4]/td[2]/b/input")).click();	
				Thread.sleep(5000);*/
				
				
				
				//Confirmation Page
				
				driver.switchTo().defaultContent();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("confirmation-block-wrapper")));	
				
				File scrFile235= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile235, new File(""+currentDateforImages+"/"+searchInfo.getScenarioCount()+"_ConfirmationPage.png"));
		       				
				String referenceName = driver.findElement(By.xpath(".//*[@id='customername']")).getText().split(": ")[1];
				activityDetails.setConfirmation_BookingRefference(referenceName);
				String bookingNo = driver.findElement(By.xpath(".//*[@id='packageconfirmno']")).getText().split(": ")[1];
				
				activityDetails.setReservationNo(bookingNo);
				String confirmation_CusMailAddress = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[1]/ul/li[4]/p/span")).getText();
				activityDetails.setConfirmation_CusMailAddress(confirmation_CusMailAddress);
				
				//Hotel 1 
				
				ArrayList<String> confirmationPage_HotelName = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelBookingStatus = new ArrayList<String>();				
				ArrayList<String> confirmationPage_HotelCheckInDate = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelCheckOutDate = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelNumberofRooms = new ArrayList<String>();
				ArrayList<String> confirmationPage_HotelNumberofNights = new ArrayList<String>();				
				ArrayList<String> Hotel_SubTotal = new ArrayList<String>();
				ArrayList<String> Hotel_TotalTaxServices = new ArrayList<String>();
				ArrayList<String> Hotel_Totals = new ArrayList<String>();
				
				ArrayList<WebElement> h_Sub = new ArrayList<WebElement>(driver.findElements(By.id("hotelsubtotal")));
				ArrayList<WebElement> h_Tax  = new ArrayList<WebElement>(driver.findElements(By.id("hoteltax")));
				ArrayList<WebElement> h_Total = new ArrayList<WebElement>(driver.findElements(By.id("hotelbookingvalue")));
				
				for (int i = 1; i <= hotelList.size(); i++) {
					
					String hotelName = driver.findElement(By.xpath(".//*[@id='hotelname_"+i+"']")).getText();
					confirmationPage_HotelName.add(hotelName);		
					String hotelBookingStatus = driver.findElement(By.xpath(".//*[@id='bookingstatus_"+i+"']")).getText();
					confirmationPage_HotelBookingStatus.add(hotelBookingStatus);	
					
					String checkInStatus = driver.findElement(By.xpath(".//*[@id='checkindate_"+i+"']")).getText();
					confirmationPage_HotelCheckInDate.add(checkInStatus);	
					
					ArrayList<WebElement> chekOUTArray = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='checkoutdate_"+i+"']")));
					
					confirmationPage_HotelCheckOutDate.add(chekOUTArray.get(0).getText());	
					confirmationPage_HotelNumberofRooms.add(chekOUTArray.get(1).getText());						
					confirmationPage_HotelNumberofNights.add(chekOUTArray.get(2).getText());	
					
					Hotel_SubTotal.add(h_Sub.get(i-1).getText());
					Hotel_TotalTaxServices.add(h_Tax.get(i-1).getText());
					Hotel_Totals.add(h_Total.get(i-1).getText());
					
				}
				
				activityDetails.setConfirmationPage_HotelName(confirmationPage_HotelName);
				activityDetails.setConfirmationPage_HotelBookingStatus(confirmationPage_HotelBookingStatus);
				activityDetails.setConfirmationPage_HotelCheckInDate(confirmationPage_HotelCheckInDate);
				activityDetails.setConfirmationPage_HotelCheckOutDate(confirmationPage_HotelCheckOutDate);
				activityDetails.setConfirmationPage_HotelNumberofRooms(confirmationPage_HotelNumberofRooms);
				activityDetails.setConfirmationPage_HotelNumberofNights(confirmationPage_HotelNumberofNights);
				activityDetails.setHotel_SubTotal(Hotel_SubTotal);
				activityDetails.setHotel_TotalTaxServices(Hotel_TotalTaxServices);
				activityDetails.setHotel_Totals(Hotel_Totals);
				
				
				
				///////////////////////////////////////////////////////////////////////
				
				///Activity List
				
				String confirmation_BI_ActivityName = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[1]")).getText();
				activityDetails.setConfirmation_BI_ActivityName(confirmation_BI_ActivityName);
				
				String confirmation_BI_City = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[2]")).getText();
				activityDetails.setConfirmation_BI_City(confirmation_BI_City);
				
				String confirmation_BI_ActType = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[4]/p[3]")).getText();
				activityDetails.setConfirmation_BI_ActType(confirmation_BI_ActType);
				
				String confirmation_BI_BookingStatus = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[5]/div[1]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_BookingStatus(confirmation_BI_BookingStatus);
				
				String confirmation_BI_SelectedDate = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[5]/div[2]/div/p[2]")).getText();
				activityDetails.setConfirmation_BI_SelectedDate(confirmation_BI_SelectedDate);
				
				ArrayList<String> confirmationPage_RateType = new ArrayList<String>();
				ArrayList<String> confirmationPage_DailyRate = new ArrayList<String>();
				ArrayList<String> confirmationPage_QTY = new ArrayList<String>();
				
				WebElement confirmActTable = driver.findElement(By.className("activity-details-table-wrapper"));
				List<WebElement> confirmationActlist = confirmActTable.findElements(By.tagName("ul"));
				
				for (int i = 1; i <= (confirmationActlist.size()/2); i++) {
					
					String confirmationRates = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[1]")).getText();
					confirmationPage_RateType.add(confirmationRates);
					
					String confirmationCost = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[2]")).getText();
					confirmationPage_DailyRate.add(confirmationCost);
					
					String confirmationQTY = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[6]/ul["+(i+1)+"]/li[3]")).getText();
					confirmationPage_QTY.add(confirmationQTY);
					
				}
				
				activityDetails.setConfirmation_RateType(confirmationPage_RateType);
				activityDetails.setConfirmation_DailyRate(confirmationPage_DailyRate);
				activityDetails.setConfirmation_QTY(confirmationPage_QTY);
				
				////
				
				String confirmation_Currency1 = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[1]/div")).getText();
				activityDetails.setConfirmation_Currency1(confirmation_Currency1);		
				String confirmation_Currency2 = driver.findElement(By.xpath(".//*[@id='sellingcurrency_total']")).getText();
				activityDetails.setConfirmation_Currency2(confirmation_Currency2);	
				
				
				//ActivityRates
				String confirmation_ActivityRate = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[7]/ul/li[2]/div[2]")).getText();
				activityDetails.setConfirmation_ActivityRate(confirmation_ActivityRate);	
				
				String confirmation_SubTotal = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[2]/div[2]")).getText();
				activityDetails.setConfirmation_SubTotal(confirmation_SubTotal);	
				
				String confirmation_Tax = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[3]/div[2]")).getText();
				activityDetails.setConfirmation_Tax(confirmation_Tax);	
				
				String confirmation_Total = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[2]/div[4]/div[7]/div[8]/ul/li[4]/div[2]")).getText();
				activityDetails.setConfirmation_Total(confirmation_Total);	
				
				
	
				
				String confirmation_SubTotal2 = driver.findElement(By.xpath(".//*[@id='totalbeforetax']")).getText();
				activityDetails.setConfirmation_SubTotal2(confirmation_SubTotal2);							
				
				String confirmation_TotalTaxOtherCharges = driver.findElement(By.xpath(".//*[@id='tottaxandothercharges']")).getText();
				activityDetails.setConfirmation_TotalTaxOtherCharges(confirmation_TotalTaxOtherCharges);	
				
				String confirmation_Total2 = driver.findElement(By.xpath(".//*[@id='totalpayable']")).getText();
				activityDetails.setConfirmation_Total2(confirmation_Total2);	
				
				String confirmation_AmountNow = driver.findElement(By.xpath(".//*[@id='amountbeingprocessednow']")).getText();
				activityDetails.setConfirmation_AmountNow(confirmation_AmountNow);	
				
				String confirmation_AmountDueCheckIn = driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText();
				activityDetails.setConfirmation_AmountDueCheckIn(confirmation_AmountDueCheckIn);	
				
				
				//////
				//Traveler Details
							
				String confirmation_FName = driver.findElement(By.xpath(".//*[@id='customerfname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_FName(confirmation_FName);
				
				String confirmation_LName = driver.findElement(By.xpath(".//*[@id='customerlname']")).getText().split(": ")[1];
				activityDetails.setConfirmation_LName(confirmation_LName);
				
				String confirmation_TP; 			
				try {
					confirmation_TP = driver.findElement(By.xpath(".//*[@id='cusemergencycontact']")).getText().split(": ")[1];
					activityDetails.setConfirmation_TP(confirmation_TP);
				} catch (Exception e) {
					confirmation_TP = "Not Available";
					activityDetails.setConfirmation_TP(confirmation_TP);
				}
			
				String confirmation_Email = driver.findElement(By.xpath(".//*[@id='customeremail']")).getText().split(": ")[1];
				activityDetails.setConfirmation_Email(confirmation_Email);
				
				String confirmation_address = driver.findElement(By.xpath(".//*[@id='customeraddress1']")).getText().split(": ")[1];
				activityDetails.setConfirmation_address(confirmation_address);
				
				String confirmation_country = driver.findElement(By.xpath(".//*[@id='customercountry']")).getText().split(": ")[1];
				activityDetails.setConfirmation_country(confirmation_country);
				
				String confirmation_city = driver.findElement(By.xpath(".//*[@id='customercity']")).getText().split(": ")[1];
				activityDetails.setConfirmation_city(confirmation_city);
				
				String confirmation_State = driver.findElement(By.xpath(".//*[@id='customerstate']")).getText().split(": ")[1];
				activityDetails.setConfirmation_State(confirmation_State);
				
				String confirmation_postalCode = driver.findElement(By.xpath(".//*[@id='cuspostcode']")).getText().split(": ")[1];
				activityDetails.setConfirmation_postalCode(confirmation_postalCode);
						
				
				
				//HotelOccupancy
				
				ArrayList<WebElement> adulttitle = new ArrayList<WebElement>();
				ArrayList<WebElement> adultFName = new ArrayList<WebElement>();
				ArrayList<WebElement> adultLName = new ArrayList<WebElement>();
				ArrayList<WebElement> childtitle = new ArrayList<WebElement>();
				ArrayList<WebElement> childFName = new ArrayList<WebElement>();
				ArrayList<WebElement> childLName = new ArrayList<WebElement>();
				
				outerloop : for (int i = 1; i <= Integer.parseInt(searchInfo.getRooms()); i++) {
					
					try {
						
						adulttitle.addAll(driver.findElements(By.id("adulttitle_"+i+""))); 									
						adultFName.addAll(driver.findElements(By.id("adultfname_"+i+"")));					
						adultLName.addAll(driver.findElements(By.id("adultlname_"+i+"")));					
						
					} catch (Exception e) {
						System.out.println("No Adult here");	
					}
										
					try {
						if (driver.findElement(By.id("childtitle_"+i+"")).isDisplayed() == false) {						
							break outerloop;						
						}else {
							childtitle.addAll(driver.findElements(By.id("childtitle_"+i+"")));							
							childFName.addAll(driver.findElements(By.id("childfname_"+i+"")));							
							childLName.addAll(driver.findElements(By.id("childlname_"+i+"")));
							
						}
					} catch (Exception e) {
						System.out.println("No child here");	
					}		
										
				}
				
				ArrayList<String> adulttitle_List = new ArrayList<String>();
				ArrayList<String> adultFName_List = new ArrayList<String>();
				ArrayList<String> adultLName_List = new ArrayList<String>();
				ArrayList<String> childtitle_List = new ArrayList<String>();
				ArrayList<String> childFName_List = new ArrayList<String>();
				ArrayList<String> childLName_List = new ArrayList<String>();
				
				for (int i = 0; i < adulttitle.size() ; i++) {
					
					adulttitle_List.add(adulttitle.get(i).getText());
					adultFName_List.add(adultFName.get(i).getText());
					adultLName_List.add(adultLName.get(i).getText());					
				}
				
				for (int i = 0; i < childtitle.size() ; i++) {
					
					childtitle_List.add(childtitle.get(i).getText());
					childFName_List.add(childFName.get(i).getText());
					childLName_List.add(childLName.get(i).getText());					
				}
				
				System.out.println(adulttitle_List);
				System.out.println(adultFName_List);
				System.out.println(adultLName_List);
				System.out.println(childtitle_List);
				System.out.println(childFName_List);
				System.out.println(childLName_List);
				
				activityDetails.setAdulttitleConfirmations_List(adulttitle_List);
				activityDetails.setAdultFNameConfirmations_List(adultFName_List);
				activityDetails.setAdultLNameConfirmations_List(adultLName_List);
				activityDetails.setChildtitleConfirmations_List(childtitle_List);
				activityDetails.setChildFNameConfirmations_List(childFName_List);
				activityDetails.setChildLNameConfirmations_List(childLName_List);
				
				
				
				
				////ActivityOccupancy
				
				String confirmation_AODActivityName = driver.findElement(By.xpath(".//*[@id='container']/div[1]/section/div/article[3]/h3")).getText().split(" - ")[1];
				activityDetails.setConfirmation_AODActivityName(confirmation_AODActivityName);
				
				String confirmation_BillingInfo_TotalValue = driver.findElement(By.xpath(".//*[@id='paymentamount']")).getText();
				activityDetails.setConfirmation_BillingInfo_TotalValue(confirmation_BillingInfo_TotalValue);
				
				ArrayList<String> confirmTitle = new ArrayList<String>();
				ArrayList<String> confirmFirstName = new ArrayList<String>();
				ArrayList<String> confirmLastName = new ArrayList<String>();
			
				WebElement confirmationPaxTable = driver.findElement(By.className("car-passeger-details"));
				List<WebElement> confirmationPaxlist = confirmationPaxTable.findElements(By.tagName("ul"));
				
				for (int i = 1; i <= (confirmationPaxlist.size()/2); i++) {
					
					String confirmationT = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[3]/div[4]/div/ul["+(i+1)+"]/li[2]")).getText();
					confirmTitle.add(confirmationT);
					
					String confirmationFN = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[3]/div[4]/div/ul["+(i+1)+"]/li[3]")).getText();
					confirmFirstName.add(confirmationFN);
					
					String confirmationLN = driver.findElement(By.xpath(".//*[@id='container']/div/section/div/article[3]/div[4]/div/ul["+(i+1)+"]/li[4]")).getText();
					confirmLastName.add(confirmationLN);
					
				}
				
				activityDetails.setConfirmationPage_cusTitle(confirmTitle);
				activityDetails.setConfirmationPage_cusFName(confirmFirstName);
				activityDetails.setConfirmationPage_cusLName(confirmLastName);
				
				
				///confirmation Policy
				
				ArrayList<String> confirm_HotelPolicyOne = new ArrayList<String>();
				ArrayList<String> confirm_HotelPolicyTwo = new ArrayList<String>();
				ArrayList<String> confirm_ActivityPolicy = new ArrayList<String>();
				
				
				
				for (int i = 1; i <= liElementHotel1Policy.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel1_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul/li["+i+"]")).getText();
					confirm_HotelPolicyOne.add(cancelPolicies);					
				}
				
				for (int i = 1; i <= liElementHotel2Policy.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='hotelcanpolicy_"+hotel2_ID+"@@"+reportDateFrom+"@@rezproduction']/div/div[3]/span/ul/li["+i+"]")).getText();
					confirm_HotelPolicyTwo.add(cancelPolicies);					
				}
				
							
				WebElement cancelElementlist = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]"));
				List<WebElement> liElement = cancelElementlist.findElements(By.tagName("li"));
				
				for (int i = 1; i <= liElement.size(); i++) {
					
					String cancelPolicies = driver.findElement(By.xpath(".//*[@id='activity_cxlpolicy_"+idOnly+"']/div[3]/ul/li["+i+"]")).getText();
					confirm_ActivityPolicy.add(cancelPolicies);
					
				}
				
				activityDetails.setConfirmCancelPolicy(confirm_ActivityPolicy);				
				activityDetails.setHotel1_ConfirmationPagePolicy(confirm_HotelPolicyOne);
				activityDetails.setHotel2_ConfirmationPagePolicy(confirm_HotelPolicyTwo);
		
								
			}
			
			
		}
		
		return activityDetails;
	}

	
	
}
